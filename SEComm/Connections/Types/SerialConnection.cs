using SEComm.Exceptions;
using System;
using System.IO.Ports;

namespace SEComm.Connections.Types
{
	public class SerialConnection : Connection
	{
		private const int PORT_BAUD = 115200;

		private const int PORT_PARITY = 0;

		private const int PORT_DATA_BITS = 8;

		private SerialPort _serialPort;

		private string _param_basePort;

		private int _param_baudRate;

		private Parity _param_parity;

		private int _param_dataBitCount;

		public override bool IsConnected
		{
			get
			{
				bool result;
				lock (this._isConnectionSynchObj)
				{
					result = (this._serialPort != null && this._serialPort.IsOpen);
				}
				return result;
			}
		}

		public SerialConnection(string port, uint baudRate, uint parity, uint dataBitCount)
		{
			this._param_basePort = port;
			this._param_baudRate = (int)baudRate;
			this._param_parity = (Parity)parity;
			this._param_dataBitCount = (int)dataBitCount;
		}

		public override void OpenConnection()
		{
			if (this._serialPort != null && this._serialPort.PortName.ToLower() != this._param_basePort.ToLower())
			{
				throw new SEExceptionConnectionAlreadyOpen("TCP Connection is already open");
			}
			if (this._serialPort == null)
			{
				this.SetPort();
			}
			if (this._serialPort != null && !this._serialPort.IsOpen)
			{
				this._serialPort.Open();
				this._stream = this._serialPort.BaseStream;
			}
		}

		protected override void CloseConnection(bool isUserInited)
		{
			if (this._serialPort != null)
			{
				this._serialPort.Close();
			}
			if (this._serialPort != null)
			{
				this._serialPort.Dispose();
			}
			this._serialPort = null;
			base.CloseConnection(isUserInited);
		}

		private void SetPort()
		{
			this._serialPort = new SerialPort(this._param_basePort, this._param_baudRate, this._param_parity, this._param_dataBitCount);
			this._serialPort.Handshake = Handshake.None;
			this._serialPort.RtsEnable = true;
			this._serialPort.DtrEnable = true;
			this._serialPort.ReadTimeout = 0;
		}
	}
}
