using SEComm.Exceptions;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace SEComm.Connections.Types
{
	public class TCPConnection : Connection
	{
		protected const int TCP_CONNECTION_TIMEOUT = 3000;

		private ManualResetEvent _notifyConnectionTimeout = null;

		private IPAddress _param_ip;

		private int _param_port;

		private TcpClient _tcpClient;

		public override bool IsConnected
		{
			get
			{
				bool result;
				lock (this._isConnectionSynchObj)
				{
					result = (this._tcpClient != null && this._tcpClient.Connected);
				}
				return result;
			}
		}

		public TCPConnection(string ip, uint port)
		{
			try
			{
				this._param_ip = Dns.GetHostAddresses(ip)[0];
				this._param_port = (int)port;
			}
			catch (Exception var_0_28)
			{
				throw new SEConnectionAccessException("TCP Connection cannot be established!");
			}
		}

		public override void OpenConnection()
		{
			IPEndPoint obj = new IPEndPoint(this._param_ip, this._param_port);
			if (this._tcpClient != null && !this._tcpClient.Client.LocalEndPoint.Equals(obj))
			{
				throw new SEExceptionConnectionAlreadyOpen("TCP Connection is already open");
			}
			if (this._tcpClient == null)
			{
				this.LogToClient();
			}
			if (this._tcpClient != null && this._tcpClient.Connected)
			{
				this._stream = this._tcpClient.GetStream();
			}
		}

		protected override void CloseConnection(bool isUserInited)
		{
			if (this._tcpClient != null)
			{
				this._tcpClient.Client.Disconnect(false);
				this._tcpClient.Close();
			}
			this._tcpClient = null;
			base.CloseConnection(isUserInited);
		}

		private void LogToClient()
		{
			this._tcpClient = new TcpClient();
			this._tcpClient.Client.Blocking = true;
			Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			try
			{
				if (this._notifyConnectionTimeout == null)
				{
					this._notifyConnectionTimeout = new ManualResetEvent(false);
				}
				this._notifyConnectionTimeout.Reset();
				this._tcpClient.BeginConnect(this._param_ip, this._param_port, new AsyncCallback(this.LogToClientCallback), socket);
				this._notifyConnectionTimeout.WaitOne(3000);
			}
			catch (TimeoutException var_1_89)
			{
				socket.Close();
				socket = null;
				if (this._tcpClient != null)
				{
					this._tcpClient.Close();
					this._tcpClient = null;
				}
			}
			finally
			{
				this._notifyConnectionTimeout.Close();
				this._notifyConnectionTimeout = null;
			}
		}

		private void LogToClientCallback(IAsyncResult result)
		{
			try
			{
				if (this._notifyConnectionTimeout != null)
				{
					this._notifyConnectionTimeout.Set();
				}
			}
			catch (Exception var_0_1E)
			{
			}
		}
	}
}
