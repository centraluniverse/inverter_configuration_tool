using System;

namespace SESecurity
{
	public class LoginCredentials
	{
		private string _userName;

		private string _deviceID;

		private DateTime _time;

		public string UserName
		{
			get
			{
				return this._userName;
			}
			set
			{
				this._userName = value;
			}
		}

		public string DeviceID
		{
			get
			{
				return this._deviceID;
			}
			set
			{
				this._deviceID = value;
			}
		}

		public DateTime Time
		{
			get
			{
				return this._time;
			}
			set
			{
				this._time = value;
			}
		}

		public LoginCredentials(string userName, string deviceID, DateTime time)
		{
			this._userName = userName;
			this._deviceID = deviceID;
			this._time = time;
		}
	}
}
