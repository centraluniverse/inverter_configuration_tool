using SEUI.Containers;
using SEUtils.Common;
using SEUtils.Graphic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace SEUI.Common
{
	public class SEProgressBar : SEForm
	{
		public delegate void ProgressFormCallBack();

		private delegate void BarDisplayDelegate();

		private delegate void BarHideDelegate();

		private const int VAL_EMPTY = -1;

		private const double BAR_AMOUNT_TO_SHOW_LIT_PANEL = 0.9;

		private const int THREAD_SLEEP_DURATION_MS = 10;

		private IContainer components = null;

		private SEPictureBox pbProgressImageEnd;

		private SEPictureBox pbProgressBar;

		private SEPictureBox pbProgressImageStart;

		private SEForm _uiOwner = null;

		private SEThread _thNotifyCallback = null;

		private SEProgressBar.ProgressFormCallBack _callBack = null;

		private Image[] _progressEndList = null;

		private int _startVal = -1;

		private float _currentVal = -1f;

		private int _endVal = -1;

		private string _text = "";

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(SEProgressBar));
			this.pbProgressImageEnd = new SEPictureBox();
			this.pbProgressBar = new SEPictureBox();
			this.pbProgressImageStart = new SEPictureBox();
			((ISupportInitialize)this.pbProgressImageEnd).BeginInit();
			((ISupportInitialize)this.pbProgressBar).BeginInit();
			((ISupportInitialize)this.pbProgressImageStart).BeginInit();
			base.SuspendLayout();
			this.pbProgressImageEnd.BackColor = Color.Transparent;
			this.pbProgressImageEnd.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbProgressImageEnd.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbProgressImageEnd.ImageListBack");
			this.pbProgressImageEnd.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbProgressImageEnd.ImageListFore");
			this.pbProgressImageEnd.Location = new Point(288, 6);
			this.pbProgressImageEnd.Name = "pbProgressImageEnd";
			this.pbProgressImageEnd.SelectedImageBack = -1;
			this.pbProgressImageEnd.SelectedImageFore = -1;
			this.pbProgressImageEnd.Size = new Size(48, 42);
			this.pbProgressImageEnd.TabIndex = 3;
			this.pbProgressImageEnd.TabStop = false;
			this.pbProgressBar.BackColor = Color.Transparent;
			this.pbProgressBar.BackgroundImageLayout = ImageLayout.None;
			this.pbProgressBar.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbProgressBar.ImageListBack");
			this.pbProgressBar.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbProgressBar.ImageListFore");
			this.pbProgressBar.Location = new Point(62, 5);
			this.pbProgressBar.Name = "pbProgressBar";
			this.pbProgressBar.SelectedImageBack = -1;
			this.pbProgressBar.SelectedImageFore = -1;
			this.pbProgressBar.Size = new Size(220, 43);
			this.pbProgressBar.TabIndex = 4;
			this.pbProgressBar.TabStop = false;
			this.pbProgressImageStart.BackColor = Color.Transparent;
			this.pbProgressImageStart.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbProgressImageStart.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbProgressImageStart.ImageListBack");
			this.pbProgressImageStart.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbProgressImageStart.ImageListFore");
			this.pbProgressImageStart.Location = new Point(8, 5);
			this.pbProgressImageStart.Name = "pbProgressImageStart";
			this.pbProgressImageStart.SelectedImageBack = -1;
			this.pbProgressImageStart.SelectedImageFore = -1;
			this.pbProgressImageStart.Size = new Size(48, 42);
			this.pbProgressImageStart.TabIndex = 5;
			this.pbProgressImageStart.TabStop = false;
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.DimGray;
			this.BackgroundImageLayout = ImageLayout.Zoom;
			base.ClientSize = new Size(346, 55);
			base.ControlBox = false;
			base.Controls.Add(this.pbProgressImageStart);
			base.Controls.Add(this.pbProgressImageEnd);
			base.Controls.Add(this.pbProgressBar);
			this.Cursor = Cursors.WaitCursor;
			this.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.FormBorderStyle = FormBorderStyle.None;
			base.Margin = new Padding(4);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "SEProgressBar";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			base.TransparencyKey = Color.DimGray;
			base.Load += new EventHandler(this.SEProgressBar_Load);
			((ISupportInitialize)this.pbProgressImageEnd).EndInit();
			((ISupportInitialize)this.pbProgressBar).EndInit();
			((ISupportInitialize)this.pbProgressImageStart).EndInit();
			base.ResumeLayout(false);
		}

		public SEProgressBar(SEForm uiOwner, Image start, ImageLayout layoutStart, Image bar, ImageLayout layoutBar, Image[] endList, ImageLayout layoutEnd, Image back, ImageLayout layoutBack)
		{
			this.InitializeComponent();
			this.SetupUI(uiOwner);
			this.UpdateImageData(start, layoutStart, bar, layoutBar, endList, layoutEnd, back, layoutBack);
			this.StartThreadControl();
		}

		private void SetupUI(SEForm uiOwner)
		{
			base.StartPosition = FormStartPosition.Manual;
			this.pbProgressImageStart.Visible = true;
			this.pbProgressBar.Visible = true;
			this.pbProgressImageEnd.Visible = true;
			this._uiOwner = uiOwner;
		}

		private void StartThreadControl()
		{
			this._thNotifyCallback = new SEThread("Progress Form Update Thread", new SEThread.ThreadFunctionCallBack(this.UpdateListenerThreadFunc), 200, 0u, 1u, true, true, true);
		}

		private void StopThreadControl()
		{
			try
			{
				this._thNotifyCallback.Stop = true;
				this._thNotifyCallback.Dispose();
			}
			catch (Exception var_0_1E)
			{
			}
			finally
			{
				this._thNotifyCallback = null;
			}
		}

		private void UpdateUIData()
		{
			Point location = this._uiOwner.Location;
			Size size = this._uiOwner.Size;
			Size size2 = base.Size;
			base.Location = new Point(location.X + (size.Width - size2.Width) / 2, location.Y + (size.Height - size2.Height) / 2);
		}

		private void UpdateListenerThreadFunc()
		{
			try
			{
				if (this._callBack != null)
				{
					this._callBack();
				}
			}
			catch (Exception var_0_1E)
			{
			}
		}

		public void Update(string text, float progressValue)
		{
			this._text = text;
			this._currentVal = progressValue;
			this.pbProgressImageEnd.TSAccess.BackgroundImage = (this.ShouldLightPanel((int)this._currentVal) ? this._progressEndList[1] : this._progressEndList[0]);
			if (this._currentVal >= (float)this._endVal)
			{
				Thread.Sleep(200);
				this.BarHide();
			}
			else
			{
				base.TSAccess.Refresh();
			}
		}

		private void UpdateProgressData(int startVal, int endVal)
		{
			this._startVal = startVal;
			this._endVal = endVal;
		}

		private void UpdateImageData(Image start, ImageLayout layoutStart, Image bar, ImageLayout layoutBar, Image[] endList, ImageLayout layoutEnd, Image back, ImageLayout layoutBack)
		{
			this.BackgroundImage = back;
			this.BackgroundImageLayout = layoutBack;
			this.pbProgressImageStart.BackgroundImage = start;
			this.pbProgressImageStart.BackgroundImageLayout = layoutStart;
			this.pbProgressBar.BackgroundImage = bar;
			this.pbProgressBar.BackgroundImageLayout = layoutBar;
			this._progressEndList = endList;
			this.pbProgressImageEnd.BackgroundImage = endList[0];
			this.pbProgressImageEnd.BackgroundImageLayout = layoutEnd;
		}

		private void SetToStart()
		{
			this._currentVal = (float)this._startVal;
		}

		public void ResetBarValues(int startVal, int endVal)
		{
			this._startVal = startVal;
			this._endVal = endVal;
			this._currentVal = 0f;
		}

		private bool IsDone()
		{
			return this._currentVal >= (float)this._endVal;
		}

		private bool ShouldLightPanel(int currentValue)
		{
			return currentValue > (int)((double)this._endVal * 0.9);
		}

		public void ShowProgressBar(int startValue, int endValue, SEProgressBar.ProgressFormCallBack callBack)
		{
			this.UpdateUIData();
			this.UpdateProgressData(startValue, endValue);
			this.Update("", (float)startValue);
			this._callBack = callBack;
			this.BarDisplay();
		}

		private void BarDisplay()
		{
			if (this._uiOwner.InvokeRequired)
			{
				this._uiOwner.BeginInvoke(new SEProgressBar.BarDisplayDelegate(this.BarDisplay));
			}
			else
			{
				try
				{
					if (!base.Visible)
					{
						base.ShowDialog(this._uiOwner);
					}
				}
				catch (Exception var_0_4B)
				{
				}
			}
		}

		public void BarHide()
		{
			if (this._uiOwner.InvokeRequired)
			{
				this._uiOwner.BeginInvoke(new SEProgressBar.BarHideDelegate(this.BarHide));
			}
			else
			{
				base.Close();
			}
		}

		private void DrawProgressBar()
		{
			Image backgroundImage = this.pbProgressBar.BackgroundImage;
			Graphics graphics = Graphics.FromImage(backgroundImage);
			int width = backgroundImage.Size.Width;
			int height = backgroundImage.Size.Height;
			float num = (!this.IsDone()) ? (this._currentVal / (float)this._endVal) : 1f;
			float num2 = (float)backgroundImage.Size.Width * num;
			Rectangle destRect = new Rectangle(0, 0, (int)num2, height);
			graphics.DrawImage(backgroundImage, destRect, 0f, 0f, num2, (float)height, GraphicsUnit.Pixel, GUtils.GetTransparentAttributes((Bitmap)backgroundImage));
		}

		private void DrawText(Graphics g)
		{
			Bitmap bitmap = (Bitmap)this.pbProgressBar.BackgroundImage;
			SolidBrush brush = new SolidBrush(GUtils.GetTransparentColor(bitmap, bitmap.Width / 2, bitmap.Height / 2));
			SizeF sizeF = g.MeasureString(this._text, this.Font);
			float x = ((float)base.Size.Width - sizeF.Width) / 2f;
			float y = ((float)base.Size.Height - sizeF.Height) / 2f - 1f * sizeF.Height;
			g.DrawString(this._text, this.Font, brush, x, y);
			string text = ((int)(this._currentVal / (float)this._endVal * 100f)).ToString() + " %";
			SolidBrush brush2 = new SolidBrush(Color.Black);
			Font font = new Font(this.Font.FontFamily, 10f, FontStyle.Bold);
			sizeF = g.MeasureString(text, font);
			x = ((float)base.Size.Width - sizeF.Width) / 2f;
			y = ((float)base.Size.Height - sizeF.Height) / 2f + 0.8f * sizeF.Height;
			g.DrawString(text, font, brush2, x, y);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			Graphics graphics = e.Graphics;
			Image backgroundImage = this.pbProgressBar.BackgroundImage;
			int width = backgroundImage.Size.Width;
			int height = backgroundImage.Size.Height;
			float num = (!this.IsDone()) ? (this._currentVal / (float)this._endVal) : 1f;
			float num2 = (float)backgroundImage.Size.Width * num;
			Rectangle rectangle = new Rectangle(0, 0, (int)num2, height);
			this.pbProgressBar.Size = new Size((int)num2, height);
			this.DrawText(graphics);
			base.OnPaint(e);
		}

		protected override void OnShown(EventArgs e)
		{
			this.SetToStart();
			this._text = "";
			base.OnShown(e);
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			this._startVal = -1;
			this._currentVal = (float)this._startVal;
			this._endVal = -1;
			base.OnClosing(e);
		}

		private void SEProgressBar_Load(object sender, EventArgs e)
		{
			this._thNotifyCallback.Pause = false;
		}
	}
}
