using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SERowCollectionThreadSafeAccess
	{
		private ThreadSafeControl _tsControl = null;

		protected ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public SERowThreadSafeAccess this[int rowIndex]
		{
			get
			{
				SERowThreadSafeAccess result = null;
				try
				{
					DataGridViewRow dgvRow = (DataGridViewRow)this.TSControl.Get("Item", new object[]
					{
						rowIndex
					});
					result = new SERowThreadSafeAccess(dgvRow);
				}
				catch (Exception var_2_38)
				{
				}
				return result;
			}
		}

		public int Count
		{
			get
			{
				return (int)this.TSControl.Get("Count");
			}
			set
			{
				this.TSControl.Set("Count", value);
			}
		}

		public int Add()
		{
			return (int)this.TSControl.InvokeMethod("Add", null);
		}

		public int Add(DataGridViewRow dataGridViewRow)
		{
			return (int)this.TSControl.InvokeMethod("Add", new object[]
			{
				dataGridViewRow
			});
		}

		public int Add(int count)
		{
			return (int)this.TSControl.InvokeMethod("Add", new object[]
			{
				count
			});
		}

		public int Add(params object[] values)
		{
			return (int)this.TSControl.InvokeMethod("Add", new object[]
			{
				values
			});
		}

		public void Clear()
		{
			this.TSControl.InvokeMethod("Clear", null);
		}

		public bool Contains(DataGridViewRow dataGridViewRow)
		{
			return (bool)this.TSControl.InvokeMethod("Contains", new object[]
			{
				dataGridViewRow
			});
		}

		public int IndexOf(DataGridViewRow dataGridViewRow)
		{
			return (int)this.TSControl.InvokeMethod("IndexOf", new object[]
			{
				dataGridViewRow
			});
		}

		public int Insert(int rowIndex, DataGridViewRow dataGridViewRow)
		{
			return (int)this.TSControl.InvokeMethod("Insert", new object[]
			{
				rowIndex,
				dataGridViewRow
			});
		}

		public int Insert(int rowIndex, int count)
		{
			return (int)this.TSControl.InvokeMethod("Insert", new object[]
			{
				rowIndex,
				count
			});
		}

		public int Insert(int rowIndex, params object[] values)
		{
			return (int)this.TSControl.InvokeMethod("Insert", new object[]
			{
				rowIndex,
				values
			});
		}

		public void Remove(DataGridViewRow dataGridViewRow)
		{
			this.TSControl.InvokeMethod("Remove", new object[]
			{
				dataGridViewRow
			});
		}

		public void RemoveAt(int index)
		{
			this.TSControl.InvokeMethod("RemoveAt", new object[]
			{
				index
			});
		}

		public SERowCollectionThreadSafeAccess(DataGridView dgv)
		{
			this.TSControl = new ThreadSafeControl(dgv.Rows, dgv);
		}
	}
}
