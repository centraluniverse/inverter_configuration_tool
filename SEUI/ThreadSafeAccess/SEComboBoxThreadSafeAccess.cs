using System;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEComboBoxThreadSafeAccess : SEControlThreadSafeAccess
	{
		private SEObjectCollectionThreadSafeAccess _tsCollection = null;

		public object DataSource
		{
			get
			{
				return base.TSControl.Get("DataSource");
			}
			set
			{
				base.TSControl.Set("DataSource", value);
			}
		}

		public string DisplayMember
		{
			get
			{
				return (string)base.TSControl.Get("DisplayMember");
			}
			set
			{
				base.TSControl.Set("DisplayMember", value);
			}
		}

		public string ValueMember
		{
			get
			{
				return (string)base.TSControl.Get("ValueMember");
			}
			set
			{
				base.TSControl.Set("ValueMember", value);
			}
		}

		public int SelectedIndex
		{
			get
			{
				return (int)base.TSControl.Get("SelectedIndex");
			}
			set
			{
				base.TSControl.Set("SelectedIndex", value);
			}
		}

		public object SelectedItem
		{
			get
			{
				return base.TSControl.Get("SelectedItem");
			}
			set
			{
				base.TSControl.Set("SelectedItem", value);
			}
		}

		public string SelectedText
		{
			get
			{
				return (string)base.TSControl.Get("SelectedText");
			}
			set
			{
				base.TSControl.Set("SelectedText", value);
			}
		}

		public object SelectedValue
		{
			get
			{
				return base.TSControl.Get("SelectedValue");
			}
			set
			{
				base.TSControl.Set("SelectedValue", value);
			}
		}

		public SEObjectCollectionThreadSafeAccess Items
		{
			get
			{
				if (this._tsCollection == null)
				{
					this._tsCollection = new SEObjectCollectionThreadSafeAccess((ComboBox)base.TSControl.Invoker);
				}
				return this._tsCollection;
			}
		}

		public SEComboBoxThreadSafeAccess(ComboBox comboBox) : base(comboBox)
		{
		}
	}
}
