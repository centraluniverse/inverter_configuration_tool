using System;
using System.Drawing;
using System.Windows.Forms;

namespace SEUI.ThreadSafeAccess
{
	public class SEPictureBoxThreadSafeAccess : SEControlThreadSafeAccess
	{
		private ThreadSafeControl _tsControl = null;

		protected new ThreadSafeControl TSControl
		{
			get
			{
				return this._tsControl;
			}
			set
			{
				this._tsControl = value;
			}
		}

		public Image Image
		{
			get
			{
				return (Image)this.TSControl.Get("Image");
			}
			set
			{
				this.TSControl.Set("Image", value);
			}
		}

		public string ImageLocation
		{
			get
			{
				return (string)this.TSControl.Get("ImageLocation");
			}
			set
			{
				this.TSControl.Set("ImageLocation", value);
			}
		}

		public PictureBoxSizeMode SizeMode
		{
			get
			{
				return (PictureBoxSizeMode)this.TSControl.Get("SizeMode");
			}
			set
			{
				this.TSControl.Set("SizeMode", value);
			}
		}

		public SEPictureBoxThreadSafeAccess(PictureBox pictureBox) : base(pictureBox)
		{
		}
	}
}
