using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace SEUI.Extra
{
	public class ExceptionForm : Form
	{
		private delegate void ShowFormDelegate();

		private const string COMMAND_MAIL_TO = "mailto";

		private const string COMMAND_MAIL_SUBJECT = "subject";

		private const string SUPPORT_URL = "support@solaredge.com";

		private const string DEFAULT_MAIL_SUBJECT = "New Support Mail";

		private IContainer components = null;

		private Button btnReport;

		private TextBox txtMessage;

		private Button btnClose;

		private static ExceptionForm _instance = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.btnReport = new Button();
			this.txtMessage = new TextBox();
			this.btnClose = new Button();
			base.SuspendLayout();
			this.btnReport.Location = new Point(254, 183);
			this.btnReport.Name = "btnReport";
			this.btnReport.Size = new Size(138, 29);
			this.btnReport.TabIndex = 0;
			this.btnReport.Text = "Report Error to SolarEdge";
			this.btnReport.UseVisualStyleBackColor = true;
			this.btnReport.Click += new EventHandler(this.btnReport_Click);
			this.txtMessage.Location = new Point(7, 8);
			this.txtMessage.Multiline = true;
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.ReadOnly = true;
			this.txtMessage.ScrollBars = ScrollBars.Vertical;
			this.txtMessage.Size = new Size(385, 168);
			this.txtMessage.TabIndex = 1;
			this.btnClose.Location = new Point(7, 183);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new Size(63, 29);
			this.btnClose.TabIndex = 2;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new EventHandler(this.btnClose_Click);
			base.AutoScaleDimensions = new SizeF(6f, 13f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(396, 216);
			base.Controls.Add(this.btnClose);
			base.Controls.Add(this.txtMessage);
			base.Controls.Add(this.btnReport);
			base.FormBorderStyle = FormBorderStyle.Fixed3D;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "ExceptionForm";
			base.ShowIcon = false;
			this.Text = "ExceptionForm";
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		private ExceptionForm()
		{
			this.InitializeComponent();
			base.WindowState = FormWindowState.Normal;
			base.StartPosition = FormStartPosition.CenterScreen;
		}

		private void ClearForm()
		{
			this.Text = "";
			this.txtMessage.Text = "";
		}

		private void SetForm(Exception e)
		{
			if (e != null)
			{
				string message = e.Message;
				string name = e.TargetSite.ReflectedType.Name;
				string name2 = e.TargetSite.Name;
				string name3 = e.GetType().Name;
				string stackTrace = e.StackTrace;
				string text = string.Concat(new string[]
				{
					name,
					name2,
					" threw a ",
					name3,
					".\r\nReason: ",
					message,
					"\r\n----------------------------------\r\n",
					stackTrace
				});
				string text2 = name3;
				this.txtMessage.Text = text;
				this.Text = text2;
			}
		}

		public static void Show(Exception e)
		{
			if (ExceptionForm._instance == null)
			{
				ExceptionForm._instance = new ExceptionForm();
			}
			ExceptionForm._instance.SetForm(e);
			ExceptionForm._instance.ShowForm();
		}

		private void ShowForm()
		{
			if (base.InvokeRequired)
			{
				base.BeginInvoke(new ExceptionForm.ShowFormDelegate(this.ShowForm));
			}
			else
			{
				base.ShowDialog();
			}
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void btnReport_Click(object sender, EventArgs e)
		{
			string fileName = "mailto:support@solaredge.com?subject:New Support Mail";
			Process.Start(fileName);
		}
	}
}
