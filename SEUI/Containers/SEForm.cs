using SEUI.Base;
using SEUI.ThreadSafeAccess;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SEUI.Containers
{
	public class SEForm : Form, ISEUIContainer, ISEUI
	{
		private IContainer components = null;

		private bool _displayWithColon = false;

		private bool _ignoreDictionaryText = false;

		private SEFormThreadSafeAccess _tsAccess = null;

		private bool _beginCapture = false;

		private Point _touchPoint = new Point(0, 0);

		private Point _endPoint = new Point(0, 0);

		public SEFormThreadSafeAccess TSAccess
		{
			get
			{
				if (this._tsAccess == null)
				{
					this._tsAccess = new SEFormThreadSafeAccess(this);
				}
				return this._tsAccess;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
			base.AutoScaleMode = AutoScaleMode.Font;
		}

		public SEForm()
		{
			this.InitializeComponent();
		}

		public bool IsUseDictionary()
		{
			return !this._ignoreDictionaryText;
		}

		public bool IsAddColon()
		{
			return this._displayWithColon;
		}

		public void SetUI(bool isAddColon, bool isUseDictionary)
		{
			this._displayWithColon = isAddColon;
			this._ignoreDictionaryText = !isUseDictionary;
		}

		protected void HandleMouseDown(int x, int y, int maxY)
		{
			this.HandleMouseDown(x, y, 0, base.Width, 0, maxY);
		}

		protected void HandleMouseDown(int x, int y, int minX, int maxX, int minY, int maxY)
		{
			if (x >= minX)
			{
				if (x <= maxX)
				{
					if (y >= minY)
					{
						if (y <= maxY)
						{
							this._beginCapture = true;
							this._touchPoint = new Point(x, y);
						}
					}
				}
			}
		}

		protected void HandleMouseMove(int x, int y)
		{
			if (this._beginCapture)
			{
				this._endPoint = new Point(x, y);
				int num = this._endPoint.X - this._touchPoint.X;
				int num2 = this._endPoint.Y - this._touchPoint.Y;
				base.Location = new Point(base.Location.X + num, base.Location.Y + num2);
			}
		}

		protected void HandleMouseUp(int x, int y)
		{
			this._beginCapture = false;
			this._touchPoint = new Point(0, 0);
			this._endPoint = new Point(0, 0);
		}
	}
}
