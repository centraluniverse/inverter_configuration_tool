using SEUI.Base;
using SEUI.ThreadSafeAccess;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SEUI.Containers
{
	public class SEGroupBox : GroupBox, ISEUIContainer, ISEUI
	{
		private IContainer components = null;

		private bool _displayWithColon = false;

		private bool _ignoreDictionaryText = false;

		private SEControlThreadSafeAccess _tsAccess = null;

		public SEControlThreadSafeAccess TSAccess
		{
			get
			{
				if (this._tsAccess == null)
				{
					this._tsAccess = new SEControlThreadSafeAccess(this);
				}
				return this._tsAccess;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
		}

		public SEGroupBox()
		{
			this.InitializeComponent();
		}

		public bool IsUseDictionary()
		{
			return !this._ignoreDictionaryText;
		}

		public bool IsAddColon()
		{
			return this._displayWithColon;
		}

		public void SetUI(bool isAddColon, bool isUseDictionary)
		{
			this._displayWithColon = isAddColon;
			this._ignoreDictionaryText = !isUseDictionary;
		}
	}
}
