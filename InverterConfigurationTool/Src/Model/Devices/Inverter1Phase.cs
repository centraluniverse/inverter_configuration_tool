using InverterConfigurationTool.Src.Engine;
using InverterConfigurationTool.Src.Model.Data;
using InverterConfigurationTool.Src.Model.Devices.Base;
using SEDevices.Data;
using SEDevices.Devices;
using SEDevices.Records.Parameter;
using SEDevices.Records.Status;
using SESecurity;
using SEUI.Common;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace InverterConfigurationTool.Src.Model.Devices
{
	public class Inverter1Phase : Inverter
	{
		private delegate void ThreadFunc();

		private SEVenus _venus;

		public SEVenus Venus
		{
			get
			{
				return this._venus;
			}
		}

		public override void StatusUpdatedSet(bool updated, UpdateDataType type)
		{
			lock (this._syncObjUpdated)
			{
				base.StatusUpdatedSet(updated, type);
				if (!updated)
				{
					this.Venus.ClearTableDataAll();
				}
			}
		}

		public override void StatusUpdatedSetAll(bool updated)
		{
			lock (this._syncObjUpdated)
			{
				base.StatusUpdatedSetAll(updated);
				if (!updated)
				{
					this.Venus.ClearTableDataAll();
				}
			}
		}

		public Inverter1Phase(SEPortia portia) : base(portia)
		{
		}

		public override void StartInverter()
		{
			if (base.Portia != null && base.Portia.CommManager.IsConnected)
			{
				this._venus = new SEVenus(base.Portia.Address + 8388608u, base.Portia.CommManager, this, null, null);
				this._plcCommandableDevice = this._venus;
			}
			base.StartInverter();
		}

		public override void ShutdownInverter()
		{
			base.ShutdownInverter();
			if (this._venus != null)
			{
				this._venus.Dispose();
				this._venus = null;
			}
		}

		public string GetParamVenus(VenusParams venusParam)
		{
			return this.GetParamVenus(venusParam, true);
		}

		public string GetParamVenus(VenusParams venusParam, bool isConvert)
		{
			string result = null;
			SEParam sEParam = null;
			if (this.Venus.IsParamExist((int)venusParam))
			{
				sEParam = this.Venus.GetParamFromTable((int)venusParam);
			}
			if (sEParam == null)
			{
				sEParam = this.Venus.Command_Params_Get(venusParam);
			}
			if (sEParam != null)
			{
				if (this.Venus.IsParamExist((int)venusParam))
				{
					this.Venus.SetParameterToTable((int)venusParam, sEParam.GetParamAsString());
				}
				else
				{
					int index = (int)sEParam.Index;
					DBParameterData instance = DBParameterData.Instance;
					object defaultVal = null;
					object parameterMinValueVenus = instance.GetParameterMinValueVenus((ushort)index);
					object parameterMaxValueVenus = instance.GetParameterMaxValueVenus((ushort)index);
					AccessLevels venusParamAccessLevelForWriting = instance.GetVenusParamAccessLevelForWriting((ushort)index);
					this.Venus.AddParameterToDataTable(index, ((VenusParams)index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValueVenus, parameterMaxValueVenus, (int)venusParamAccessLevelForWriting);
				}
				if (sEParam.ParameterType != 3)
				{
					DBParameterData instance2 = DBParameterData.Instance;
					ushort index2 = sEParam.Index;
					float? paramConversionRateVenus = instance2.GetParamConversionRateVenus(index2);
					float? paramEqAVenus = instance2.GetParamEqAVenus(index2);
					float? paramEqBVenus = instance2.GetParamEqBVenus(index2);
					float rate = paramConversionRateVenus.HasValue ? paramConversionRateVenus.Value : 1f;
					float a = paramEqAVenus.HasValue ? paramEqAVenus.Value : 1f;
					float b = paramEqBVenus.HasValue ? paramEqBVenus.Value : 0f;
					sEParam.SetConvertionRate(rate, a, b);
				}
				result = sEParam.GetParamAsString(isConvert);
			}
			return result;
		}

		public string[] GetParamVenus(VenusParams[] venusParams)
		{
			return this.GetParamVenus(venusParams, true);
		}

		public string[] GetParamVenus(VenusParams[] venusParams, bool isConvert)
		{
			List<string> list = null;
			int num = venusParams.Length;
			List<VenusParams> list2 = new List<VenusParams>(0);
			List<SEParam> list3 = new List<SEParam>(0);
			for (int i = 0; i < num; i++)
			{
				VenusParams venusParams2 = venusParams[i];
				SEParam paramFromTable = this.Venus.GetParamFromTable((int)venusParams2);
				if (paramFromTable == null)
				{
					list2.Add(venusParams2);
				}
				else
				{
					list3.Add(paramFromTable);
				}
			}
			if (list2 != null && list2.Count > 0)
			{
				SEParam[] array = this.Venus.Command_Params_Get(list2.ToArray());
				if (array != null)
				{
					int num2 = array.Length;
					for (int i = 0; i < num2; i++)
					{
						SEParam item = array[i];
						list3.Add(item);
					}
				}
			}
			int num3 = (list3 != null) ? list3.Count : 0;
			for (int i = 0; i < num3; i++)
			{
				SEParam sEParam = list3[i];
				if (this.Venus.IsParamExist((int)sEParam.Index))
				{
					this.Venus.SetParameterToTable((int)sEParam.Index, sEParam.GetParamAsString());
				}
				else
				{
					int index = (int)sEParam.Index;
					DBParameterData instance = DBParameterData.Instance;
					object defaultVal = null;
					object parameterMinValueVenus = instance.GetParameterMinValueVenus((ushort)index);
					object parameterMaxValueVenus = instance.GetParameterMaxValueVenus((ushort)index);
					AccessLevels venusParamAccessLevelForWriting = instance.GetVenusParamAccessLevelForWriting((ushort)index);
					this.Venus.AddParameterToDataTable(index, ((VenusParams)index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValueVenus, parameterMaxValueVenus, (int)venusParamAccessLevelForWriting);
				}
				if (sEParam.ParameterType != 3)
				{
					DBParameterData instance2 = DBParameterData.Instance;
					ushort index2 = sEParam.Index;
					float? paramConversionRateVenus = instance2.GetParamConversionRateVenus(index2);
					float? paramEqAVenus = instance2.GetParamEqAVenus(index2);
					float? paramEqBVenus = instance2.GetParamEqBVenus(index2);
					float rate = paramConversionRateVenus.HasValue ? paramConversionRateVenus.Value : 1f;
					float a = paramEqAVenus.HasValue ? paramEqAVenus.Value : 1f;
					float b = paramEqBVenus.HasValue ? paramEqBVenus.Value : 0f;
					sEParam.SetConvertionRate(rate, a, b);
				}
				string paramAsString = sEParam.GetParamAsString(isConvert);
				if (list == null)
				{
					list = new List<string>(0);
				}
				list.Add(paramAsString);
			}
			return list.ToArray();
		}

		public override SWVersion GetDSPVersion()
		{
			return this.Venus.DeviceSWVersion;
		}

		public override SWVersion GetDSPPwrVersion()
		{
			return this.Venus.DeviceSWVersionPower;
		}

		public void SetParamVenus(VenusParams index, object value)
		{
			this.SetParamVenus(index, value, true);
		}

		public void SetParamVenus(VenusParams index, object value, bool shouldValueBeConverted)
		{
			DBParameterData instance = DBParameterData.Instance;
			Type typeOfParamVenus = instance.GetTypeOfParamVenus((ushort)index);
			SEParam sEParam = new SEParam((ushort)index, value, typeOfParamVenus);
			if (shouldValueBeConverted && sEParam.ParameterType != 3)
			{
				sEParam.SetValueConverted(base.RevertValue((int)sEParam.Index, double.Parse(value.ToString()), DeviceType.VENUS));
			}
			if (this.Venus.IsParamExist((int)sEParam.Index))
			{
				this.Venus.SetParameterToTable((int)sEParam.Index, sEParam.GetParamAsString());
			}
			else
			{
				object defaultVal = null;
				object parameterMinValueVenus = instance.GetParameterMinValueVenus(sEParam.Index);
				object parameterMaxValueVenus = instance.GetParameterMaxValueVenus(sEParam.Index);
				AccessLevels venusParamAccessLevelForWriting = instance.GetVenusParamAccessLevelForWriting(sEParam.Index);
				this.Venus.AddParameterToDataTable((int)sEParam.Index, ((VenusParams)sEParam.Index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValueVenus, parameterMaxValueVenus, (int)venusParamAccessLevelForWriting);
			}
			this.Venus.Command_Params_Set(sEParam);
		}

		public void SetParamVenus(VenusParams[] indices, object[] values)
		{
			this.SetParamVenus(indices, values, true);
		}

		public void SetParamVenus(VenusParams[] indices, object[] values, bool shouldValuesBeConverted)
		{
			List<SEParam> list = new List<SEParam>(0);
			int num = indices.Length;
			DBParameterData instance = DBParameterData.Instance;
			for (int i = 0; i < num; i++)
			{
				ushort num2 = (ushort)indices[i];
				object obj = values[i];
				Type typeOfParamVenus = DBParameterData.Instance.GetTypeOfParamVenus(num2);
				SEParam sEParam = new SEParam(num2, obj, typeOfParamVenus);
				if (shouldValuesBeConverted && sEParam.ParameterType != 3)
				{
					sEParam.SetValueConverted(base.RevertValue((int)sEParam.Index, double.Parse(obj.ToString()), DeviceType.VENUS));
				}
				if (this.Venus.IsParamExist((int)sEParam.Index))
				{
					this.Venus.SetParameterToTable((int)sEParam.Index, sEParam.GetParamAsString());
				}
				else
				{
					DBParameterData instance2 = DBParameterData.Instance;
					object defaultVal = null;
					object parameterMinValueVenus = instance2.GetParameterMinValueVenus(sEParam.Index);
					object parameterMaxValueVenus = instance2.GetParameterMaxValueVenus(sEParam.Index);
					AccessLevels venusParamAccessLevelForWriting = instance2.GetVenusParamAccessLevelForWriting(sEParam.Index);
					this.Venus.AddParameterToDataTable((int)sEParam.Index, ((VenusParams)sEParam.Index).ToString(), (int)sEParam.ParameterType, sEParam.GetParamAsString(), defaultVal, parameterMinValueVenus, parameterMaxValueVenus, (int)venusParamAccessLevelForWriting);
				}
				list.Add(sEParam);
			}
			this.Venus.Command_Params_Set(list.ToArray());
		}

		public override void UpdateStatusSpec()
		{
			base.UpdateStatusSpec();
			try
			{
				base.DataSystem.DSP1Version = this.Venus.DeviceSWVersion;
				base.DataSystem.DSP2Version = this.Venus.DeviceSWVersionPower;
			}
			catch (Exception var_0_3A)
			{
			}
		}

		public override void UpdateStatusLCD(bool withEnergy)
		{
			try
			{
				SETelemetrySystemStatus sETelemetrySystemStatus = this.Venus.Command_Venus_GetSysStatus();
				if (sETelemetrySystemStatus != null)
				{
					base.SystemStatus = sETelemetrySystemStatus;
				}
			}
			catch (Exception var_1_21)
			{
			}
			base.UpdateStatusLCD(withEnergy);
		}

		public override void UpdateSettingsRegional()
		{
			try
			{
				DBParameterData instance = DBParameterData.Instance;
				string paramVenus = this.GetParamVenus(VenusParams.INV_COUNTRY_CODE);
				string paramPortia = base.GetParamPortia(PortiaParams.SETUP_LANGUAGE);
				int num = (!string.IsNullOrEmpty(paramVenus)) ? int.Parse(paramVenus) : -1;
				int num2 = (!string.IsNullOrEmpty(paramPortia)) ? int.Parse(paramPortia) : -1;
				DateTime currentTime = new DateTime(1970, 1, 1, 0, 0, 0);
				try
				{
					string paramPortia2 = base.GetParamPortia(PortiaParams.SYSTEM_LOCAL_TIME);
					int seconds = (!string.IsNullOrEmpty(paramPortia2)) ? int.Parse(paramPortia2) : 0;
					TimeSpan value = new TimeSpan(0, 0, seconds);
					currentTime = currentTime.Add(value);
				}
				catch (Exception var_9_8D)
				{
				}
				DateTime rTCTime = new DateTime(1970, 1, 1, 0, 0, 0);
				try
				{
					string paramPortia3 = base.GetParamPortia(PortiaParams.RTC_TIME);
					int seconds2 = (!string.IsNullOrEmpty(paramPortia3)) ? int.Parse(paramPortia3) : 0;
					TimeSpan value2 = new TimeSpan(0, 0, seconds2);
					rTCTime = rTCTime.Add(value2);
				}
				catch (Exception var_9_8D)
				{
				}
				SEParam sEParam = base.Portia.Command_Params_Get(PortiaParams.RTC_GMT_OFFSET);
				int gMTOffset = 0;
				try
				{
					gMTOffset = sEParam.ValueInt32;
				}
				catch (Exception var_16_109)
				{
				}
				base.DataRegional.GMTOffset = gMTOffset;
				if (num > -1 || num2 > -1)
				{
					RegionalData regionalData = new RegionalData(this);
					regionalData.CurrentTime = currentTime;
					regionalData.RTCTime = rTCTime;
					regionalData.GMTOffset = gMTOffset;
					regionalData.CountryIndex = num;
					regionalData.LanguageIndex = num2;
					instance.PopulateRegionalData(regionalData);
					base.DataRegional.CopyData(regionalData);
				}
			}
			catch (Exception var_16_109)
			{
			}
		}

		protected override void UpdateParameterTableCallBack()
		{
			DBParameterData instance = DBParameterData.Instance;
			try
			{
				if (base.Portia != null && this.Venus != null)
				{
					DataManager instance2 = DataManager.Instance;
					List<ushort> portiaParamListToView = instance.GetPortiaParamListToView(instance2.LoginItem.AccessLevel);
					List<ushort> venusParamListToView = instance.GetVenusParamListToView(instance2.LoginItem.AccessLevel);
					Dictionary<int, string[]> dictionary = new Dictionary<int, string[]>(0);
					Dictionary<int, string[]> dictionary2 = new Dictionary<int, string[]>(0);
					this.UpdateParams(portiaParamListToView, venusParamListToView, dictionary, dictionary2);
					this._lastUpdatedCPUData = dictionary;
					this._lastUpdatedDSPData = dictionary2;
					DataManager.Instance.UIOwner.UpdateInverterParameters(dictionary, dictionary2);
				}
			}
			catch (Exception var_6_95)
			{
			}
		}

		protected override void UpdateParams(List<ushort> cpuIndexList, List<ushort> dspIndexList, Dictionary<int, string[]> cpuDataList, Dictionary<int, string[]> dspDataList)
		{
			int num = ((cpuIndexList != null) ? cpuIndexList.Count : 0) + ((dspIndexList != null) ? dspIndexList.Count : 0);
			int num2 = 0;
			base.UpdateCPUParams(cpuIndexList, cpuDataList, ref num2);
			this.UpdateDSPParams(dspIndexList, dspDataList, ref num2);
			DataManager.Instance.UIOwner.UpdateProgressBar("", (float)num);
		}

		private void UpdateDSPParams(List<ushort> dspList, Dictionary<int, string[]> dspDataList, ref int numParamsRetreived)
		{
			DataManager instance = DataManager.Instance;
			int num = (dspList != null) ? dspList.Count : 0;
			List<VenusParams> list = new List<VenusParams>(0);
			List<int> list2 = new List<int>(0);
			for (int i = 0; i < num; i++)
			{
				int num2 = (int)dspList[i];
				int num3 = DBParameterVersion.Instance.GetParamRealIndex((ushort)num2, DeviceType.VENUS, this.Venus.DeviceSWVersion);
				if (num3 > -1)
				{
					list2.Add(num3);
					list.Add((VenusParams)num2);
					this.Venus.ClearTableData(num2);
				}
			}
			if (list != null && list.Count > 0)
			{
				string[] paramVenus = this.GetParamVenus(list.ToArray());
				int num4 = list.Count;
				num4 = Math.Min(num4, paramVenus.Length);
				for (int i = 0; i < num4; i++)
				{
					int num3 = list2[i];
					ushort num5 = dspList[i];
					string text = list[i].ToString();
					string text2 = paramVenus[i];
					string paramUnitKeyVenus = DBParameterData.Instance.GetParamUnitKeyVenus(num5);
					dspDataList.Add((int)num5, new string[]
					{
						text,
						text2,
						paramUnitKeyVenus
					});
				}
				int num6 = paramVenus.Length;
				numParamsRetreived += num6;
				instance.UIOwner.UpdateProgressBar(string.Format(DataManager.Instance.Dictionary["MsgUpdateParamDSP"], new object[]
				{
					num6
				}), (float)numParamsRetreived);
			}
		}

		public override string GetLineStatusValue()
		{
			string text = string.Format("{0:0.0}", (double)base.SystemStatus.Vout);
			string text2 = string.Format("{0:0.0}", (double)base.SystemStatus.Vin);
			string text3 = string.Format("{0:0.0}", (double)base.SystemStatus.Power);
			int length = text.Length;
			int length2 = text2.Length;
			int length3 = text3.Length;
			int num = 6 - text.Length;
			int num2 = 6 - text2.Length;
			int num3 = 6 - text3.Length;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(text);
			for (int i = 0; i < num; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append("#");
			int num4 = num2 / 2;
			for (int i = 0; i < num4; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text2);
			if (num4 > 0)
			{
				int num5 = num2 - num4;
				for (int i = 0; i < num5; i++)
				{
					stringBuilder.Append("#");
				}
			}
			stringBuilder.Append("#");
			for (int i = 0; i < num3; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text3);
			return stringBuilder.ToString();
		}

		public override string GetLinePowerOnOffSwitch()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string text = (base.SystemStatus.OnOffSwitch == 0) ? "OFF" : "ON";
			int length = text.Length;
			int num = 20 - length;
			for (int i = 0; i < num; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text);
			return stringBuilder.ToString();
		}

		public override string GetLineFacTempStatusValue()
		{
			StringBuilder stringBuilder = new StringBuilder();
			float freq = base.SystemStatus.Freq;
			float temp = base.SystemStatus.Temp;
			string text = (freq > -3.40282347E+38f && freq < 3.40282347E+38f) ? string.Format("{0:0.##}", (double)freq) : "---";
			string text2 = (temp > -3.40282347E+38f && temp < 3.40282347E+38f) ? string.Format("{0:0.##}", (double)temp) : "---";
			int length = text.Length;
			int length2 = text2.Length;
			int num = 7 - length;
			int num2 = 5 - length2;
			stringBuilder.Append(text);
			for (int i = 0; i < num; i++)
			{
				stringBuilder.Append("#");
			}
			int num3 = 20 - length - num - length2;
			for (int i = 0; i < num3; i++)
			{
				stringBuilder.Append("#");
			}
			stringBuilder.Append(text2);
			return stringBuilder.ToString();
		}

		public override void Action_UpdateParameterTable(int numOfParams, bool shouldStartNewProgressBar)
		{
			DataManager instance = DataManager.Instance;
			DBParameterData instance2 = DBParameterData.Instance;
			List<ushort> portiaParamListToView = instance2.GetPortiaParamListToView(instance.LoginItem.AccessLevel);
			List<ushort> venusParamListToView = instance2.GetVenusParamListToView(instance.LoginItem.AccessLevel);
			int endValue = ((portiaParamListToView != null) ? portiaParamListToView.Count : 0) + ((venusParamListToView != null) ? venusParamListToView.Count : 0);
			if (shouldStartNewProgressBar)
			{
				instance.UIOwner.ShowProgressBar(0, endValue, new SEProgressBar.ProgressFormCallBack(this.UpdateParameterTableCallBack), instance.UIOwner);
			}
			else
			{
				this.UpdateParameterTableCallBack();
			}
		}

		public override void Action_Set_Country(uint newCountryIndex)
		{
			this.Venus.Command_Venus_SetCountry(newCountryIndex);
			this.Venus.Command_Device_Reset(0u);
			base.SetParamPortia(PortiaParams.RESERVED1, newCountryIndex);
			string countryShortNameByIndex = DBParameterData.Instance.GetCountryShortNameByIndex((int)newCountryIndex);
			base.SetParamPortia(PortiaParams.SETUP_COUNTRY_SHORT, countryShortNameByIndex);
			base.Portia.Command_Device_Reset(0u);
			this.Action_UpdateParameterTable(0, true);
		}
	}
}
