using System;

namespace InverterConfigurationTool.Src.Ui.DataEntry
{
	public enum HWType
	{
		ZB,
		LAN,
		RS232,
		Server,
		PingTest,
		IIC,
		RS485,
		COUNT
	}
}
