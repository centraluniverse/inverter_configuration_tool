using InverterConfigurationTool.Properties;
using InverterConfigurationTool.Src.Engine;
using InverterConfigurationTool.Src.Ui.Main;
using SEComm.Connections;
using SEComm.Connections.Types;
using SESecurity;
using SEStorage;
using SEUI.Base;
using SEUI.Common;
using SEUI.Containers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO.Ports;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Ui.DataEntry
{
	public class ConnectionForm : SEForm
	{
		private delegate void ShowDelegate();

		private delegate void HideDelegate();

		private const string CONNECTION_SERIAL = "Serial Connection";

		private const string CONNECTION_TCP = "TCP Connection";

		private const string CONNECTION_PR = "Port Replicator";

		private static ConnectionForm _instance = null;

		private Dictionary<string, ConnectionType> _connections = null;

		private IContainer components = null;

		private SECheckBox chkShowOnStartup;

		private SELabel lblPort;

		private SELabel lblIP;

		private SETextBox txtPort;

		private SETextBox txtIP;

		private SELabel lblSerialPort;

		private SEComboBox cmbSerialPort;

		private SELabel lblConnectionType;

		private SEButton btnCancel;

		private SEButton btnConnect;

		private SELabel lblConnectionHeader;

		private SEComboBox cmbConnectionType;

		private SEPictureBox sePictureBox2;

		private SETextBox txtPRPort;

		private SETextBox txtPRIP;

		private SETextBox txtPRPortiaID;

		private SELabel lblPRPort;

		private SELabel lblPRIP;

		private SELabel lblPRPortiaID;

		private SETextBox txtBaud;

		private SELabel lblBaud;

		private SELabel lblParity;

		private SETextBox txtParity;

		private SELabel lblBits;

		private SETextBox txtBits;

		private SELabel lblConnectionTypeName;

		public static ConnectionForm Instance
		{
			get
			{
				if (ConnectionForm._instance == null)
				{
					ConnectionForm._instance = new ConnectionForm();
				}
				return ConnectionForm._instance;
			}
		}

		private ConnectionForm()
		{
			this.InitializeComponent();
			this.SetupLabels();
			this.UpdateUI(this);
			this.DoubleBuffered = true;
		}

		private void SetupLabels()
		{
			this.lblConnectionHeader.SetUI(false, true);
			this.lblConnectionType.SetUI(true, true);
			this.lblConnectionTypeName.SetUI(false, false);
			this.lblSerialPort.SetUI(true, true);
			this.lblBaud.SetUI(true, true);
			this.lblParity.SetUI(true, true);
			this.lblBits.SetUI(true, true);
			this.lblIP.SetUI(true, true);
			this.lblPort.SetUI(true, true);
			this.lblPRIP.SetUI(true, true);
			this.lblPRPort.SetUI(true, true);
			this.lblPRPortiaID.SetUI(true, true);
		}

		private void UpdateUI(Control control)
		{
			try
			{
				if (control != null)
				{
					control.Text = this.GetTextForControl(control);
					if (control.Controls != null && control.Controls.Count > 0)
					{
						int count = control.Controls.Count;
						for (int i = 0; i < count; i++)
						{
							Control control2 = control.Controls[i];
							this.UpdateUI(control2);
						}
					}
				}
			}
			catch (Exception var_3_7B)
			{
			}
		}

		private string GetTextForControl(Control control)
		{
			string result;
			if (control == null)
			{
				result = null;
			}
			else
			{
				string text = "";
				text = DataManager.Instance.Dictionary[control.Name];
				try
				{
					ISEUI iSEUI = (ISEUI)control;
					if (((ISEUI)control).IsAddColon())
					{
						text += ":";
					}
					else if (!((ISEUI)control).IsUseDictionary())
					{
						text = control.Text;
					}
				}
				catch (InvalidCastException var_3_7E)
				{
				}
				string text2 = text;
				result = text2;
			}
			return result;
		}

		public void BarShow()
		{
			if (MainForm.Instance.InvokeRequired)
			{
				MainForm.Instance.BeginInvoke(new ConnectionForm.ShowDelegate(this.BarShow));
			}
			else
			{
				try
				{
					if (!base.Visible)
					{
						base.ShowDialog(MainForm.Instance);
					}
				}
				catch (Exception var_0_48)
				{
				}
			}
		}

		public void BarHide()
		{
			if (base.InvokeRequired)
			{
				base.BeginInvoke(new ConnectionForm.HideDelegate(this.BarHide));
			}
			else
			{
				try
				{
					if (base.Visible)
					{
						base.Close();
					}
				}
				catch (Exception var_0_3E)
				{
				}
			}
		}

		private Dictionary<string, ConnectionType> GetAvailableConnections(AccessLevels level)
		{
			Dictionary<string, ConnectionType> dictionary = new Dictionary<string, ConnectionType>(0);
			if (level >= AccessLevels.MONITORING)
			{
				string[] portNames = SerialPort.GetPortNames();
				if (portNames.Length > 0)
				{
					dictionary.Add("Serial Connection", ConnectionType.SERIAL);
				}
			}
			if (level >= AccessLevels.ADMIN)
			{
				dictionary.Add("Port Replicator", ConnectionType.PORT_REPLICATOR);
			}
			return dictionary;
		}

		private void SetConnectionsComboBoxData(ConnectionParams lastConnectionData)
		{
			this._connections = this.GetAvailableConnections(DataManager.Instance.LoginItem.AccessLevel);
			if (this._connections != null)
			{
				this.cmbConnectionType.Items.Clear();
				int selectedIndex = 0;
				Type connection = (lastConnectionData != null) ? lastConnectionData.ConnectionType : null;
				ConnectionType connectionType = this.GetConnectionType(connection);
				Dictionary<string, ConnectionType>.KeyCollection.Enumerator enumerator = this._connections.Keys.GetEnumerator();
				int num = -1;
				while (enumerator.MoveNext())
				{
					string current = enumerator.Current;
					ConnectionType connectionType2 = this._connections[current];
					this.cmbConnectionType.Items.Add(current);
					num++;
					if (connectionType2.Equals(connectionType))
					{
						selectedIndex = num;
					}
				}
				this.cmbConnectionType.SelectedIndex = selectedIndex;
				this.lblConnectionTypeName.Text = this.cmbConnectionType.Items[0].ToString();
			}
		}

		private void SetComboBoxesSelectedData(ConnectionParams lastConnectionData)
		{
			Type type = (lastConnectionData != null) ? lastConnectionData.ConnectionType : null;
			if (type != null)
			{
				if (type.Equals(typeof(SerialConnection)))
				{
					string text = lastConnectionData.Parameters[0].ToString();
					this.LoadSerialData(text);
					this.LoadTCPData("", "");
					this.LoadPortReplicatorData("", "", "");
				}
				else if (type.Equals(typeof(TCPConnection)))
				{
					string ip = lastConnectionData.Parameters[0].ToString();
					string text = lastConnectionData.Parameters[1].ToString();
					this.LoadSerialData(null);
					this.LoadTCPData(ip, text);
					this.LoadPortReplicatorData("", "", "");
				}
				else if (type.Equals(typeof(PortReplicatorConnection)))
				{
					string ip = lastConnectionData.Parameters[0].ToString();
					string text = lastConnectionData.Parameters[1].ToString();
					uint num = (uint)lastConnectionData.Parameters[2];
					string portiaID = string.Format("{0:000000000}", Convert.ToString((long)((ulong)num), 16));
					this.LoadSerialData(null);
					this.LoadTCPData("", "");
					this.LoadPortReplicatorData(ip, text, portiaID);
				}
			}
			else
			{
				this.LoadSerialData(null);
				this.LoadTCPData("", "");
				this.LoadPortReplicatorData("", "", "");
			}
		}

		private void LoadSerialData(string selectedPort)
		{
			bool flag = !string.IsNullOrEmpty(selectedPort);
			string[] portNames = SerialPort.GetPortNames();
			int num = -1;
			this.cmbSerialPort.Items.Clear();
			int num2 = (portNames != null) ? portNames.Length : 0;
			for (int i = 0; i < num2; i++)
			{
				string text = portNames[i];
				this.cmbSerialPort.Items.Add(text);
				if (flag && selectedPort.Equals(text))
				{
					num = i;
				}
			}
			this.cmbSerialPort.SelectedIndex = ((num > -1 && num < this.cmbSerialPort.Items.Count) ? num : 0);
			this.txtBaud.TSAccess.Text = "115200";
			this.txtParity.TSAccess.Text = "0";
			this.txtBits.TSAccess.Text = "8";
		}

		private void LoadTCPData(string ip, string port)
		{
			this.txtIP.TSAccess.Text = ((!string.IsNullOrEmpty(ip)) ? ip : "");
			this.txtPort.TSAccess.Text = ((!string.IsNullOrEmpty(port)) ? port : "");
		}

		private void LoadPortReplicatorData(string ip, string port, string portiaID)
		{
			this.txtPRIP.TSAccess.Text = ((!string.IsNullOrEmpty(ip)) ? ip : "");
			this.txtPRPort.TSAccess.Text = ((!string.IsNullOrEmpty(port)) ? port : "");
			this.txtPRPortiaID.TSAccess.Text = ((!string.IsNullOrEmpty(portiaID)) ? portiaID : "");
		}

		private ConnectionType GetConnectionType(Type connection)
		{
			ConnectionType connectionType = ConnectionType.NONE;
			ConnectionType result;
			if (connection == null)
			{
				result = connectionType;
			}
			else
			{
				if (connection.Equals(typeof(SerialConnection)))
				{
					connectionType = ConnectionType.SERIAL;
				}
				if (connection.Equals(typeof(TCPConnection)))
				{
					connectionType = ConnectionType.TCP;
				}
				if (connection.Equals(typeof(PortReplicatorConnection)))
				{
					connectionType = ConnectionType.PORT_REPLICATOR;
				}
				result = connectionType;
			}
			return result;
		}

		protected override void OnShown(EventArgs e)
		{
			this.UpdateUI(this);
			SEUserSettings userSettings = DataManager.Instance.UserSettings;
			object obj = userSettings["ShowConnectionFormOnStart"];
			this.chkShowOnStartup.Checked = (obj == null || (bool)obj);
			ConnectionParams connectionParams = (userSettings["LastConnectionParams"] != null) ? ((ConnectionParams)userSettings["LastConnectionParams"]) : null;
			this.SetConnectionsComboBoxData(connectionParams);
			this.SetComboBoxesSelectedData(connectionParams);
			base.OnShown(e);
		}

		protected override void OnClosed(EventArgs e)
		{
			ConnectionForm._instance = null;
			base.OnClosed(e);
		}

		private void btnConnect_Click(object sender, EventArgs e)
		{
			object selectedItem = this.cmbConnectionType.SelectedItem;
			string text = (selectedItem != null) ? selectedItem.ToString() : null;
			if (!string.IsNullOrEmpty(text))
			{
				ConnectionType connectionType = this._connections[text];
				ConnectionParams connectionParams = null;
				switch (connectionType)
				{
				case ConnectionType.SERIAL:
				{
					string text2 = (this.cmbSerialPort.SelectedItem != null) ? this.cmbSerialPort.SelectedItem.ToString() : "";
					uint num = (!string.IsNullOrEmpty(this.txtBaud.TSAccess.Text)) ? uint.Parse(this.txtBaud.TSAccess.Text) : 0u;
					uint num2 = (!string.IsNullOrEmpty(this.txtParity.TSAccess.Text)) ? uint.Parse(this.txtParity.TSAccess.Text) : 0u;
					uint num3 = (!string.IsNullOrEmpty(this.txtBits.TSAccess.Text)) ? uint.Parse(this.txtBits.TSAccess.Text) : 0u;
					connectionParams = new ConnectionParams("Serial Connection", false, typeof(SerialConnection), new object[]
					{
						text2,
						num,
						num2,
						num3
					});
					break;
				}
				case ConnectionType.TCP:
				{
					string text3 = this.txtIP.TSAccess.Text;
					uint num4 = (!string.IsNullOrEmpty(this.txtPort.TSAccess.Text)) ? uint.Parse(this.txtPort.TSAccess.Text) : 0u;
					connectionParams = new ConnectionParams("TCP Connection", true, typeof(TCPConnection), new object[]
					{
						text3,
						num4
					});
					break;
				}
				case ConnectionType.PORT_REPLICATOR:
				{
					string text3 = this.txtPRIP.TSAccess.Text;
					uint num4 = (!string.IsNullOrEmpty(this.txtPRPort.TSAccess.Text)) ? uint.Parse(this.txtPRPort.TSAccess.Text) : 0u;
					uint num5 = 0u;
					if (!string.IsNullOrEmpty(this.txtPRPortiaID.TSAccess.Text))
					{
						string text4 = this.txtPRPortiaID.TSAccess.Text;
						num5 = uint.Parse(text4, NumberStyles.HexNumber);
					}
					connectionParams = new ConnectionParams("Port Replicator Connection", true, typeof(PortReplicatorConnection), new object[]
					{
						text3,
						num4,
						num5
					});
					break;
				}
				}
				DataManager.Instance.UserSettings["LastConnectionParams"] = connectionParams;
				DataManager.Instance.UserSettings["ShowConnectionFormOnStart"] = this.chkShowOnStartup.Checked;
				DataManager.Instance.Connect(connectionParams);
				base.Close();
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void cmbConnectionType_SelectedIndexChanged(object sender, EventArgs e)
		{
			SEComboBox sEComboBox = (SEComboBox)sender;
			AccessLevels accessLevel = DataManager.Instance.LoginItem.AccessLevel;
			bool flag = accessLevel == AccessLevels.ADMIN;
			if (sEComboBox != null)
			{
				string text = sEComboBox.SelectedItem.ToString();
				bool flag2 = text.Equals("Serial Connection");
				bool flag3 = text.Equals("TCP Connection");
				bool flag4 = text.Equals("Port Replicator");
				this.lblSerialPort.TSAccess.Visible = flag2;
				this.lblSerialPort.Location = ((!flag) ? new Point(114, 90) : new Point(14, 90));
				this.cmbSerialPort.TSAccess.Visible = flag2;
				this.cmbSerialPort.Location = ((!flag) ? new Point(114, 109) : new Point(14, 109));
				this.cmbConnectionType.Visible = flag;
				this.lblConnectionTypeName.Visible = (!flag && flag2);
				this.lblBaud.TSAccess.Visible = (flag && flag2);
				this.txtBaud.TSAccess.Visible = (flag && flag2);
				this.lblParity.TSAccess.Visible = (flag && flag2);
				this.txtParity.TSAccess.Visible = (flag && flag2);
				this.lblBits.TSAccess.Visible = (flag && flag2);
				this.txtBits.TSAccess.Visible = (flag && flag2);
				this.lblIP.TSAccess.Visible = (flag && flag3);
				this.lblPort.TSAccess.Visible = (flag && flag3);
				this.txtIP.TSAccess.Visible = (flag && flag3);
				this.txtPort.TSAccess.Visible = (flag && flag3);
				this.lblPRIP.TSAccess.Visible = (flag && flag4);
				this.lblPRPort.TSAccess.Visible = (flag && flag4);
				this.lblPRPortiaID.TSAccess.Visible = (flag && flag4);
				this.txtPRIP.TSAccess.Visible = (flag && flag4);
				this.txtPRPort.TSAccess.Visible = (flag && flag4);
				this.txtPRPortiaID.TSAccess.Visible = (flag && flag4);
			}
		}

		private void chkShowOnStartup_CheckedChanged(object sender, EventArgs e)
		{
			SECheckBox sECheckBox = (SECheckBox)sender;
			DataManager.Instance.UserSettings["ShowConnectionFormOnStart"] = sECheckBox.Checked;
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblConnectionHeader.Size.Height);
			base.OnMouseDown(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
			base.OnMouseMove(e);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
			base.OnMouseUp(e);
		}

		private void lblConnectionHeader_MouseDown(object sender, MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblConnectionHeader.Size.Height);
		}

		private void lblConnectionHeader_MouseMove(object sender, MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
		}

		private void lblConnectionHeader_MouseUp(object sender, MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(ConnectionForm));
			this.cmbConnectionType = new SEComboBox();
			this.lblConnectionHeader = new SELabel();
			this.btnConnect = new SEButton();
			this.btnCancel = new SEButton();
			this.lblConnectionType = new SELabel();
			this.cmbSerialPort = new SEComboBox();
			this.lblSerialPort = new SELabel();
			this.txtIP = new SETextBox();
			this.txtPort = new SETextBox();
			this.lblIP = new SELabel();
			this.lblPort = new SELabel();
			this.chkShowOnStartup = new SECheckBox();
			this.sePictureBox2 = new SEPictureBox();
			this.txtPRPort = new SETextBox();
			this.txtPRIP = new SETextBox();
			this.txtPRPortiaID = new SETextBox();
			this.lblPRPort = new SELabel();
			this.lblPRIP = new SELabel();
			this.lblPRPortiaID = new SELabel();
			this.txtBaud = new SETextBox();
			this.lblBaud = new SELabel();
			this.lblParity = new SELabel();
			this.txtParity = new SETextBox();
			this.lblBits = new SELabel();
			this.txtBits = new SETextBox();
			this.lblConnectionTypeName = new SELabel();
			((ISupportInitialize)this.sePictureBox2).BeginInit();
			base.SuspendLayout();
			this.cmbConnectionType.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbConnectionType.ForeColor = Color.Black;
			this.cmbConnectionType.FormattingEnabled = true;
			this.cmbConnectionType.Location = new Point(66, 60);
			this.cmbConnectionType.Name = "cmbConnectionType";
			this.cmbConnectionType.Size = new Size(161, 21);
			this.cmbConnectionType.TabIndex = 5;
			this.cmbConnectionType.SelectedIndexChanged += new EventHandler(this.cmbConnectionType_SelectedIndexChanged);
			this.lblConnectionHeader.BackColor = Color.Transparent;
			this.lblConnectionHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblConnectionHeader.Location = new Point(20, 1);
			this.lblConnectionHeader.Name = "lblConnectionHeader";
			this.lblConnectionHeader.Size = new Size(255, 28);
			this.lblConnectionHeader.TabIndex = 6;
			this.lblConnectionHeader.Text = "Connection Form";
			this.lblConnectionHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblConnectionHeader.MouseMove += new MouseEventHandler(this.lblConnectionHeader_MouseMove);
			this.lblConnectionHeader.MouseDown += new MouseEventHandler(this.lblConnectionHeader_MouseDown);
			this.lblConnectionHeader.MouseUp += new MouseEventHandler(this.lblConnectionHeader_MouseUp);
			this.btnConnect.BackColor = Color.Silver;
			this.btnConnect.Location = new Point(144, 164);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new Size(72, 25);
			this.btnConnect.TabIndex = 7;
			this.btnConnect.Text = "Connect";
			this.btnConnect.UseVisualStyleBackColor = true;
			this.btnConnect.Click += new EventHandler(this.btnConnect_Click);
			this.btnCancel.BackColor = Color.Silver;
			this.btnCancel.Location = new Point(219, 164);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new Size(72, 25);
			this.btnCancel.TabIndex = 8;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
			this.lblConnectionType.BackColor = Color.Transparent;
			this.lblConnectionType.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblConnectionType.ForeColor = Color.Black;
			this.lblConnectionType.Location = new Point(21, 38);
			this.lblConnectionType.Name = "lblConnectionType";
			this.lblConnectionType.Size = new Size(255, 16);
			this.lblConnectionType.TabIndex = 9;
			this.lblConnectionType.Text = "Select Connection Type:";
			this.lblConnectionType.TextAlign = ContentAlignment.MiddleCenter;
			this.cmbSerialPort.ForeColor = Color.Black;
			this.cmbSerialPort.FormattingEnabled = true;
			this.cmbSerialPort.Location = new Point(14, 109);
			this.cmbSerialPort.Name = "cmbSerialPort";
			this.cmbSerialPort.Size = new Size(71, 21);
			this.cmbSerialPort.TabIndex = 10;
			this.cmbSerialPort.Visible = false;
			this.lblSerialPort.BackColor = Color.Transparent;
			this.lblSerialPort.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblSerialPort.ForeColor = Color.Black;
			this.lblSerialPort.Location = new Point(14, 90);
			this.lblSerialPort.Name = "lblSerialPort";
			this.lblSerialPort.Size = new Size(71, 16);
			this.lblSerialPort.TabIndex = 11;
			this.lblSerialPort.Text = "COM Port:";
			this.lblSerialPort.TextAlign = ContentAlignment.MiddleCenter;
			this.lblSerialPort.Visible = false;
			this.txtIP.ForeColor = Color.Black;
			this.txtIP.Location = new Point(28, 110);
			this.txtIP.Name = "txtIP";
			this.txtIP.Size = new Size(131, 20);
			this.txtIP.TabIndex = 12;
			this.txtIP.Visible = false;
			this.txtPort.ForeColor = Color.Black;
			this.txtPort.Location = new Point(176, 110);
			this.txtPort.Name = "txtPort";
			this.txtPort.Size = new Size(97, 20);
			this.txtPort.TabIndex = 13;
			this.txtPort.Visible = false;
			this.lblIP.BackColor = Color.Transparent;
			this.lblIP.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblIP.ForeColor = Color.Black;
			this.lblIP.Location = new Point(28, 91);
			this.lblIP.Name = "lblIP";
			this.lblIP.Size = new Size(131, 16);
			this.lblIP.TabIndex = 14;
			this.lblIP.Text = "Select IP:";
			this.lblIP.TextAlign = ContentAlignment.MiddleCenter;
			this.lblIP.Visible = false;
			this.lblPort.BackColor = Color.Transparent;
			this.lblPort.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblPort.ForeColor = Color.Black;
			this.lblPort.Location = new Point(176, 90);
			this.lblPort.Name = "lblPort";
			this.lblPort.Size = new Size(97, 16);
			this.lblPort.TabIndex = 15;
			this.lblPort.Text = "Select Port:";
			this.lblPort.TextAlign = ContentAlignment.MiddleCenter;
			this.lblPort.Visible = false;
			this.chkShowOnStartup.BackColor = Color.Transparent;
			this.chkShowOnStartup.ForeColor = Color.Black;
			this.chkShowOnStartup.Location = new Point(99, 139);
			this.chkShowOnStartup.Name = "chkShowOnStartup";
			this.chkShowOnStartup.Size = new Size(113, 19);
			this.chkShowOnStartup.TabIndex = 16;
			this.chkShowOnStartup.Text = "Show on startup";
			this.chkShowOnStartup.UseVisualStyleBackColor = false;
			this.chkShowOnStartup.CheckedChanged += new EventHandler(this.chkShowOnStartup_CheckedChanged);
			this.sePictureBox2.BackColor = Color.Transparent;
			this.sePictureBox2.BackgroundImage = Resources.SolarEdgeLogo;
			this.sePictureBox2.BackgroundImageLayout = ImageLayout.Zoom;
			this.sePictureBox2.ImageListBack = (List<Image>)componentResourceManager.GetObject("sePictureBox2.ImageListBack");
			this.sePictureBox2.ImageListFore = (List<Image>)componentResourceManager.GetObject("sePictureBox2.ImageListFore");
			this.sePictureBox2.Location = new Point(10, 163);
			this.sePictureBox2.Name = "sePictureBox2";
			this.sePictureBox2.SelectedImageBack = -1;
			this.sePictureBox2.SelectedImageFore = -1;
			this.sePictureBox2.Size = new Size(103, 27);
			this.sePictureBox2.TabIndex = 126;
			this.sePictureBox2.TabStop = false;
			this.txtPRPort.ForeColor = Color.Black;
			this.txtPRPort.Location = new Point(135, 109);
			this.txtPRPort.Name = "txtPRPort";
			this.txtPRPort.Size = new Size(68, 20);
			this.txtPRPort.TabIndex = 128;
			this.txtPRPort.Visible = false;
			this.txtPRIP.ForeColor = Color.Black;
			this.txtPRIP.Location = new Point(14, 109);
			this.txtPRIP.Name = "txtPRIP";
			this.txtPRIP.Size = new Size(102, 20);
			this.txtPRIP.TabIndex = 127;
			this.txtPRIP.Visible = false;
			this.txtPRPortiaID.CharacterCasing = CharacterCasing.Upper;
			this.txtPRPortiaID.ForeColor = Color.Black;
			this.txtPRPortiaID.Location = new Point(219, 109);
			this.txtPRPortiaID.Name = "txtPRPortiaID";
			this.txtPRPortiaID.Size = new Size(68, 20);
			this.txtPRPortiaID.TabIndex = 129;
			this.txtPRPortiaID.Visible = false;
			this.lblPRPort.BackColor = Color.Transparent;
			this.lblPRPort.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblPRPort.ForeColor = Color.Black;
			this.lblPRPort.Location = new Point(130, 92);
			this.lblPRPort.Name = "lblPRPort";
			this.lblPRPort.Size = new Size(79, 16);
			this.lblPRPort.TabIndex = 130;
			this.lblPRPort.Text = "Select Port:";
			this.lblPRPort.TextAlign = ContentAlignment.MiddleCenter;
			this.lblPRPort.Visible = false;
			this.lblPRIP.BackColor = Color.Transparent;
			this.lblPRIP.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblPRIP.ForeColor = Color.Black;
			this.lblPRIP.Location = new Point(14, 92);
			this.lblPRIP.Name = "lblPRIP";
			this.lblPRIP.Size = new Size(99, 16);
			this.lblPRIP.TabIndex = 131;
			this.lblPRIP.Text = "Select IP:";
			this.lblPRIP.TextAlign = ContentAlignment.MiddleCenter;
			this.lblPRIP.Visible = false;
			this.lblPRPortiaID.BackColor = Color.Transparent;
			this.lblPRPortiaID.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblPRPortiaID.ForeColor = Color.Black;
			this.lblPRPortiaID.Location = new Point(221, 92);
			this.lblPRPortiaID.Name = "lblPRPortiaID";
			this.lblPRPortiaID.Size = new Size(64, 16);
			this.lblPRPortiaID.TabIndex = 132;
			this.lblPRPortiaID.Text = "Portia ID:";
			this.lblPRPortiaID.TextAlign = ContentAlignment.MiddleCenter;
			this.lblPRPortiaID.Visible = false;
			this.txtBaud.ForeColor = Color.Black;
			this.txtBaud.Location = new Point(95, 109);
			this.txtBaud.Name = "txtBaud";
			this.txtBaud.Size = new Size(64, 20);
			this.txtBaud.TabIndex = 133;
			this.txtBaud.Visible = false;
			this.lblBaud.BackColor = Color.Transparent;
			this.lblBaud.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblBaud.ForeColor = Color.Black;
			this.lblBaud.Location = new Point(96, 92);
			this.lblBaud.Name = "lblBaud";
			this.lblBaud.Size = new Size(63, 16);
			this.lblBaud.TabIndex = 134;
			this.lblBaud.Text = "Baud Rate:";
			this.lblBaud.TextAlign = ContentAlignment.MiddleCenter;
			this.lblBaud.Visible = false;
			this.lblParity.BackColor = Color.Transparent;
			this.lblParity.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblParity.ForeColor = Color.Black;
			this.lblParity.Location = new Point(168, 93);
			this.lblParity.Name = "lblParity";
			this.lblParity.Size = new Size(56, 16);
			this.lblParity.TabIndex = 136;
			this.lblParity.Text = "Parity:";
			this.lblParity.TextAlign = ContentAlignment.MiddleCenter;
			this.lblParity.Visible = false;
			this.txtParity.ForeColor = Color.Black;
			this.txtParity.Location = new Point(168, 110);
			this.txtParity.Name = "txtParity";
			this.txtParity.Size = new Size(56, 20);
			this.txtParity.TabIndex = 135;
			this.txtParity.Visible = false;
			this.lblBits.BackColor = Color.Transparent;
			this.lblBits.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblBits.ForeColor = Color.Black;
			this.lblBits.Location = new Point(230, 92);
			this.lblBits.Name = "lblBits";
			this.lblBits.Size = new Size(60, 16);
			this.lblBits.TabIndex = 138;
			this.lblBits.Text = "Data Bits:";
			this.lblBits.TextAlign = ContentAlignment.MiddleCenter;
			this.lblBits.Visible = false;
			this.txtBits.ForeColor = Color.Black;
			this.txtBits.Location = new Point(233, 109);
			this.txtBits.Name = "txtBits";
			this.txtBits.Size = new Size(56, 20);
			this.txtBits.TabIndex = 137;
			this.txtBits.Visible = false;
			this.lblConnectionTypeName.BackColor = Color.White;
			this.lblConnectionTypeName.BorderStyle = BorderStyle.FixedSingle;
			this.lblConnectionTypeName.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblConnectionTypeName.ForeColor = Color.Black;
			this.lblConnectionTypeName.Location = new Point(66, 60);
			this.lblConnectionTypeName.Name = "lblConnectionTypeName";
			this.lblConnectionTypeName.Size = new Size(161, 21);
			this.lblConnectionTypeName.TabIndex = 141;
			this.lblConnectionTypeName.TextAlign = ContentAlignment.MiddleCenter;
			this.lblConnectionTypeName.Visible = false;
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.DimGray;
			this.BackgroundImage = Resources.BackLandNew;
			this.BackgroundImageLayout = ImageLayout.Stretch;
			base.ClientSize = new Size(300, 200);
			base.ControlBox = false;
			base.Controls.Add(this.lblConnectionTypeName);
			base.Controls.Add(this.lblBits);
			base.Controls.Add(this.txtBits);
			base.Controls.Add(this.lblParity);
			base.Controls.Add(this.txtParity);
			base.Controls.Add(this.lblBaud);
			base.Controls.Add(this.txtBaud);
			base.Controls.Add(this.sePictureBox2);
			base.Controls.Add(this.chkShowOnStartup);
			base.Controls.Add(this.lblConnectionHeader);
			base.Controls.Add(this.cmbConnectionType);
			base.Controls.Add(this.btnConnect);
			base.Controls.Add(this.btnCancel);
			base.Controls.Add(this.lblSerialPort);
			base.Controls.Add(this.lblConnectionType);
			base.Controls.Add(this.cmbSerialPort);
			base.Controls.Add(this.txtPRPortiaID);
			base.Controls.Add(this.txtPRPort);
			base.Controls.Add(this.txtPRIP);
			base.Controls.Add(this.txtPort);
			base.Controls.Add(this.txtIP);
			base.Controls.Add(this.lblPRPortiaID);
			base.Controls.Add(this.lblPRIP);
			base.Controls.Add(this.lblPRPort);
			base.Controls.Add(this.lblPort);
			base.Controls.Add(this.lblIP);
			this.DoubleBuffered = true;
			base.FormBorderStyle = FormBorderStyle.None;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "ConnectionForm";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			base.TransparencyKey = Color.DimGray;
			((ISupportInitialize)this.sePictureBox2).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}
	}
}
