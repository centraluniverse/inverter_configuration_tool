using InverterConfigurationTool.Properties;
using InverterConfigurationTool.Src.Engine;
using InverterConfigurationTool.Src.Model.Devices.Base;
using SEDevices.Devices;
using SEUI.Base;
using SEUI.Common;
using SEUI.Containers;
using SEUI.Data;
using SEUI.Extra;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Ui.DataEntry
{
	public class HWSettingsATCommands : SEForm
	{
		public const int MAX_NUM_OF_ITEMS = 20;

		private IContainer components = null;

		private SELabel lblHWSettingsATCommandsHeader;

		private SEPictureBox sePictureBox1;

		private SELabel lblHWSettingsATCommandsMode;

		private SELabel lblHWSettingsATCommandsModeValue;

		private SEButton btnHWSettingsATCommandsAccept;

		private SEButton btnHWSettingsATCommandsConDisCon;

		private SEButton lblHWSettingsATCommandsRefresh;

		private DataGridViewTextBoxColumn colId;

		private DataGridViewTextBoxColumn colCmd;

		private DataGridViewTextBoxColumn colSpace;

		private DataGridViewTextBoxColumn colRsp;

		private SEDataGridView dgvConnect;

		private SEDataGridView dgvDisconnect;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;

		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;

		private SEButton btnHWSettingsATCommandsAddNewRow;

		private SEButton btnHWSettingsATCommandsRemoveSelectedRow;

		private SEPictureBox sePictureBox2;

		private SEButton btnLoadGSMATCmdsPreset;

		private Inverter _inverter = null;

		private static HWSettingsATCommands _instance = null;

		private DataTable _tblParamConnect = null;

		private DataTable _tblParamDisconnect = null;

		private bool _isConnectDataShown = true;

		private SEThread _thUpdateGSMParams = null;

		private Dictionary<int, string> _dicChangeConnectCmd;

		private Dictionary<int, string> _dicChangeConnectRsp;

		private Dictionary<int, string> _dicChangeDisconnectCmd;

		private Dictionary<int, string> _dicChangeDisconnectRsp;

		public Inverter Inverter
		{
			get
			{
				return this._inverter;
			}
			set
			{
				this._inverter = value;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(HWSettingsATCommands));
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle5 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle6 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle7 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle8 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle9 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle10 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle11 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle12 = new DataGridViewCellStyle();
			this.lblHWSettingsATCommandsHeader = new SELabel();
			this.sePictureBox1 = new SEPictureBox();
			this.lblHWSettingsATCommandsMode = new SELabel();
			this.lblHWSettingsATCommandsModeValue = new SELabel();
			this.btnHWSettingsATCommandsAccept = new SEButton();
			this.btnHWSettingsATCommandsConDisCon = new SEButton();
			this.lblHWSettingsATCommandsRefresh = new SEButton();
			this.dgvConnect = new SEDataGridView();
			this.colId = new DataGridViewTextBoxColumn();
			this.colCmd = new DataGridViewTextBoxColumn();
			this.colSpace = new DataGridViewTextBoxColumn();
			this.colRsp = new DataGridViewTextBoxColumn();
			this.dgvDisconnect = new SEDataGridView();
			this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn4 = new DataGridViewTextBoxColumn();
			this.btnHWSettingsATCommandsAddNewRow = new SEButton();
			this.btnHWSettingsATCommandsRemoveSelectedRow = new SEButton();
			this.sePictureBox2 = new SEPictureBox();
			this.btnLoadGSMATCmdsPreset = new SEButton();
			((ISupportInitialize)this.sePictureBox1).BeginInit();
			((ISupportInitialize)this.dgvConnect).BeginInit();
			((ISupportInitialize)this.dgvDisconnect).BeginInit();
			((ISupportInitialize)this.sePictureBox2).BeginInit();
			base.SuspendLayout();
			this.lblHWSettingsATCommandsHeader.BackColor = Color.Transparent;
			this.lblHWSettingsATCommandsHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsATCommandsHeader.Location = new Point(12, 1);
			this.lblHWSettingsATCommandsHeader.Name = "lblHWSettingsATCommandsHeader";
			this.lblHWSettingsATCommandsHeader.Size = new Size(537, 29);
			this.lblHWSettingsATCommandsHeader.TabIndex = 8;
			this.lblHWSettingsATCommandsHeader.Text = "AT Commands Configuration";
			this.lblHWSettingsATCommandsHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsATCommandsHeader.MouseMove += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseMove);
			this.lblHWSettingsATCommandsHeader.MouseDown += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseDown);
			this.lblHWSettingsATCommandsHeader.MouseUp += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseUp);
			this.sePictureBox1.BackColor = Color.Transparent;
			this.sePictureBox1.BackgroundImage = Resources.gsm_antenna_1;
			this.sePictureBox1.BackgroundImageLayout = ImageLayout.Zoom;
			this.sePictureBox1.ImageListBack = (List<Image>)componentResourceManager.GetObject("sePictureBox1.ImageListBack");
			this.sePictureBox1.ImageListFore = (List<Image>)componentResourceManager.GetObject("sePictureBox1.ImageListFore");
			this.sePictureBox1.Location = new Point(523, 12);
			this.sePictureBox1.Name = "sePictureBox1";
			this.sePictureBox1.SelectedImageBack = -1;
			this.sePictureBox1.SelectedImageFore = -1;
			this.sePictureBox1.Size = new Size(35, 110);
			this.sePictureBox1.TabIndex = 9;
			this.sePictureBox1.TabStop = false;
			this.lblHWSettingsATCommandsMode.BackColor = Color.Transparent;
			this.lblHWSettingsATCommandsMode.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsATCommandsMode.Location = new Point(130, 37);
			this.lblHWSettingsATCommandsMode.Name = "lblHWSettingsATCommandsMode";
			this.lblHWSettingsATCommandsMode.Size = new Size(83, 24);
			this.lblHWSettingsATCommandsMode.TabIndex = 114;
			this.lblHWSettingsATCommandsMode.Text = "Set Mode:";
			this.lblHWSettingsATCommandsMode.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsATCommandsModeValue.BackColor = Color.Transparent;
			this.lblHWSettingsATCommandsModeValue.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsATCommandsModeValue.Location = new Point(213, 37);
			this.lblHWSettingsATCommandsModeValue.Name = "lblHWSettingsATCommandsModeValue";
			this.lblHWSettingsATCommandsModeValue.Size = new Size(185, 24);
			this.lblHWSettingsATCommandsModeValue.TabIndex = 115;
			this.lblHWSettingsATCommandsModeValue.Text = "Connect Commands";
			this.lblHWSettingsATCommandsModeValue.TextAlign = ContentAlignment.MiddleLeft;
			this.btnHWSettingsATCommandsAccept.BackColor = Color.Silver;
			this.btnHWSettingsATCommandsAccept.Location = new Point(226, 358);
			this.btnHWSettingsATCommandsAccept.Name = "btnHWSettingsATCommandsAccept";
			this.btnHWSettingsATCommandsAccept.Size = new Size(100, 27);
			this.btnHWSettingsATCommandsAccept.TabIndex = 118;
			this.btnHWSettingsATCommandsAccept.Text = "Accept";
			this.btnHWSettingsATCommandsAccept.UseVisualStyleBackColor = true;
			this.btnHWSettingsATCommandsAccept.Click += new EventHandler(this.btnHWSettingsATCommandsAccept_Click);
			this.btnHWSettingsATCommandsConDisCon.BackColor = Color.Silver;
			this.btnHWSettingsATCommandsConDisCon.Location = new Point(393, 35);
			this.btnHWSettingsATCommandsConDisCon.Name = "btnHWSettingsATCommandsConDisCon";
			this.btnHWSettingsATCommandsConDisCon.Size = new Size(131, 27);
			this.btnHWSettingsATCommandsConDisCon.TabIndex = 119;
			this.btnHWSettingsATCommandsConDisCon.Text = "Show Disconnect";
			this.btnHWSettingsATCommandsConDisCon.UseVisualStyleBackColor = true;
			this.btnHWSettingsATCommandsConDisCon.Click += new EventHandler(this.btnHWSettingsATCommandsConDisCon_Click);
			this.lblHWSettingsATCommandsRefresh.BackColor = Color.Silver;
			this.lblHWSettingsATCommandsRefresh.Location = new Point(434, 358);
			this.lblHWSettingsATCommandsRefresh.Name = "lblHWSettingsATCommandsRefresh";
			this.lblHWSettingsATCommandsRefresh.Size = new Size(83, 27);
			this.lblHWSettingsATCommandsRefresh.TabIndex = 120;
			this.lblHWSettingsATCommandsRefresh.Text = "Refresh";
			this.lblHWSettingsATCommandsRefresh.UseVisualStyleBackColor = true;
			this.lblHWSettingsATCommandsRefresh.Click += new EventHandler(this.lblHWSettingsATCommandsRefresh_Click);
			this.dgvConnect.AllowUserToAddRows = false;
			this.dgvConnect.AllowUserToDeleteRows = false;
			this.dgvConnect.AllowUserToResizeColumns = false;
			this.dgvConnect.AllowUserToResizeRows = false;
			this.dgvConnect.BorderStyle = BorderStyle.Fixed3D;
			dataGridViewCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle.BackColor = SystemColors.Control;
			dataGridViewCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle.WrapMode = DataGridViewTriState.True;
			this.dgvConnect.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle;
			this.dgvConnect.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvConnect.Columns.AddRange(new DataGridViewColumn[]
			{
				this.colId,
				this.colCmd,
				this.colSpace,
				this.colRsp
			});
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle2.BackColor = Color.White;
			dataGridViewCellStyle2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle2.ForeColor = Color.Black;
			dataGridViewCellStyle2.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle2.SelectionForeColor = Color.White;
			dataGridViewCellStyle2.WrapMode = DataGridViewTriState.True;
			this.dgvConnect.DefaultCellStyle = dataGridViewCellStyle2;
			this.dgvConnect.Location = new Point(44, 70);
			this.dgvConnect.MultiSelect = false;
			this.dgvConnect.Name = "dgvConnect";
			this.dgvConnect.RowHeadersVisible = false;
			dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle3.BackColor = Color.White;
			dataGridViewCellStyle3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle3.ForeColor = Color.Black;
			dataGridViewCellStyle3.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle3.SelectionForeColor = Color.White;
			this.dgvConnect.RowsDefaultCellStyle = dataGridViewCellStyle3;
			this.dgvConnect.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvConnect.RowTemplate.DefaultCellStyle.BackColor = Color.White;
			this.dgvConnect.RowTemplate.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.dgvConnect.RowTemplate.DefaultCellStyle.ForeColor = Color.Black;
			this.dgvConnect.RowTemplate.DefaultCellStyle.SelectionBackColor = Color.Gray;
			this.dgvConnect.RowTemplate.DefaultCellStyle.SelectionForeColor = Color.White;
			this.dgvConnect.RowTemplate.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
			this.dgvConnect.ScrollBars = ScrollBars.Vertical;
			this.dgvConnect.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			this.dgvConnect.Size = new Size(473, 277);
			this.dgvConnect.TabIndex = 121;
			this.dgvConnect.CellEndEdit += new DataGridViewCellEventHandler(this.dgvConnect_CellEndEdit);
			this.colId.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
			dataGridViewCellStyle4.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle4.BackColor = Color.White;
			dataGridViewCellStyle4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle4.ForeColor = Color.Black;
			dataGridViewCellStyle4.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle4.SelectionForeColor = Color.White;
			this.colId.DefaultCellStyle = dataGridViewCellStyle4;
			this.colId.Frozen = true;
			this.colId.HeaderText = "ID";
			this.colId.Name = "colId";
			this.colId.ReadOnly = true;
			this.colId.Resizable = DataGridViewTriState.False;
			this.colId.Width = 30;
			this.colCmd.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
			dataGridViewCellStyle5.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle5.BackColor = Color.White;
			dataGridViewCellStyle5.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle5.ForeColor = Color.Black;
			dataGridViewCellStyle5.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle5.SelectionForeColor = Color.White;
			this.colCmd.DefaultCellStyle = dataGridViewCellStyle5;
			this.colCmd.Frozen = true;
			this.colCmd.HeaderText = "AT Command";
			this.colCmd.Name = "colCmd";
			this.colCmd.Resizable = DataGridViewTriState.False;
			this.colCmd.SortMode = DataGridViewColumnSortMode.NotSortable;
			this.colCmd.Width = 200;
			this.colSpace.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
			this.colSpace.HeaderText = "";
			this.colSpace.Name = "colSpace";
			this.colSpace.ReadOnly = true;
			this.colSpace.Resizable = DataGridViewTriState.False;
			this.colSpace.SortMode = DataGridViewColumnSortMode.NotSortable;
			this.colSpace.Width = 20;
			this.colRsp.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
			dataGridViewCellStyle6.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle6.BackColor = Color.White;
			dataGridViewCellStyle6.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle6.ForeColor = Color.Black;
			dataGridViewCellStyle6.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle6.SelectionForeColor = Color.White;
			this.colRsp.DefaultCellStyle = dataGridViewCellStyle6;
			this.colRsp.HeaderText = "AT Responses";
			this.colRsp.Name = "colRsp";
			this.colRsp.Resizable = DataGridViewTriState.False;
			this.colRsp.SortMode = DataGridViewColumnSortMode.NotSortable;
			this.colRsp.Width = 200;
			this.dgvDisconnect.AllowUserToAddRows = false;
			this.dgvDisconnect.AllowUserToDeleteRows = false;
			this.dgvDisconnect.AllowUserToResizeColumns = false;
			this.dgvDisconnect.AllowUserToResizeRows = false;
			this.dgvDisconnect.BorderStyle = BorderStyle.Fixed3D;
			dataGridViewCellStyle7.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle7.BackColor = SystemColors.Control;
			dataGridViewCellStyle7.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle7.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle7.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle7.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle7.WrapMode = DataGridViewTriState.True;
			this.dgvDisconnect.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.dgvDisconnect.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvDisconnect.Columns.AddRange(new DataGridViewColumn[]
			{
				this.dataGridViewTextBoxColumn1,
				this.dataGridViewTextBoxColumn2,
				this.dataGridViewTextBoxColumn3,
				this.dataGridViewTextBoxColumn4
			});
			dataGridViewCellStyle8.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle8.BackColor = Color.White;
			dataGridViewCellStyle8.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle8.ForeColor = Color.Black;
			dataGridViewCellStyle8.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle8.SelectionForeColor = Color.White;
			dataGridViewCellStyle8.WrapMode = DataGridViewTriState.True;
			this.dgvDisconnect.DefaultCellStyle = dataGridViewCellStyle8;
			this.dgvDisconnect.Location = new Point(44, 70);
			this.dgvDisconnect.MultiSelect = false;
			this.dgvDisconnect.Name = "dgvDisconnect";
			this.dgvDisconnect.RowHeadersVisible = false;
			dataGridViewCellStyle9.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle9.BackColor = Color.White;
			dataGridViewCellStyle9.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle9.ForeColor = Color.Black;
			dataGridViewCellStyle9.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle9.SelectionForeColor = Color.White;
			this.dgvDisconnect.RowsDefaultCellStyle = dataGridViewCellStyle9;
			this.dgvDisconnect.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvDisconnect.RowTemplate.DefaultCellStyle.BackColor = Color.White;
			this.dgvDisconnect.RowTemplate.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.dgvDisconnect.RowTemplate.DefaultCellStyle.ForeColor = Color.Black;
			this.dgvDisconnect.RowTemplate.DefaultCellStyle.SelectionBackColor = Color.Gray;
			this.dgvDisconnect.RowTemplate.DefaultCellStyle.SelectionForeColor = Color.White;
			this.dgvDisconnect.RowTemplate.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
			this.dgvDisconnect.ScrollBars = ScrollBars.Vertical;
			this.dgvDisconnect.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			this.dgvDisconnect.Size = new Size(473, 277);
			this.dgvDisconnect.TabIndex = 122;
			this.dgvDisconnect.CellEndEdit += new DataGridViewCellEventHandler(this.dgvDisconnect_CellEndEdit);
			this.dataGridViewTextBoxColumn1.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
			dataGridViewCellStyle10.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle10.BackColor = Color.White;
			dataGridViewCellStyle10.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle10.ForeColor = Color.Black;
			dataGridViewCellStyle10.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle10.SelectionForeColor = Color.White;
			this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle10;
			this.dataGridViewTextBoxColumn1.Frozen = true;
			this.dataGridViewTextBoxColumn1.HeaderText = "ID";
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			this.dataGridViewTextBoxColumn1.Resizable = DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn1.Width = 30;
			this.dataGridViewTextBoxColumn2.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
			dataGridViewCellStyle11.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle11.BackColor = Color.White;
			dataGridViewCellStyle11.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle11.ForeColor = Color.Black;
			dataGridViewCellStyle11.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle11.SelectionForeColor = Color.White;
			this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle11;
			this.dataGridViewTextBoxColumn2.Frozen = true;
			this.dataGridViewTextBoxColumn2.HeaderText = "AT Command";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.Resizable = DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn2.SortMode = DataGridViewColumnSortMode.NotSortable;
			this.dataGridViewTextBoxColumn2.Width = 200;
			this.dataGridViewTextBoxColumn3.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
			this.dataGridViewTextBoxColumn3.HeaderText = "";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			this.dataGridViewTextBoxColumn3.Resizable = DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn3.SortMode = DataGridViewColumnSortMode.NotSortable;
			this.dataGridViewTextBoxColumn3.Width = 20;
			this.dataGridViewTextBoxColumn4.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
			dataGridViewCellStyle12.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle12.BackColor = Color.White;
			dataGridViewCellStyle12.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle12.ForeColor = Color.Black;
			dataGridViewCellStyle12.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle12.SelectionForeColor = Color.White;
			this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle12;
			this.dataGridViewTextBoxColumn4.HeaderText = "AT Responses";
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			this.dataGridViewTextBoxColumn4.Resizable = DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn4.SortMode = DataGridViewColumnSortMode.NotSortable;
			this.dataGridViewTextBoxColumn4.Width = 200;
			this.btnHWSettingsATCommandsAddNewRow.BackColor = Color.Silver;
			this.btnHWSettingsATCommandsAddNewRow.Location = new Point(10, 93);
			this.btnHWSettingsATCommandsAddNewRow.Name = "btnHWSettingsATCommandsAddNewRow";
			this.btnHWSettingsATCommandsAddNewRow.Size = new Size(29, 21);
			this.btnHWSettingsATCommandsAddNewRow.TabIndex = 123;
			this.btnHWSettingsATCommandsAddNewRow.Text = "+";
			this.btnHWSettingsATCommandsAddNewRow.UseVisualStyleBackColor = true;
			this.btnHWSettingsATCommandsAddNewRow.Click += new EventHandler(this.btnHWSettingsATCommandsAddNewRow_Click);
			this.btnHWSettingsATCommandsRemoveSelectedRow.BackColor = Color.Silver;
			this.btnHWSettingsATCommandsRemoveSelectedRow.Enabled = false;
			this.btnHWSettingsATCommandsRemoveSelectedRow.Location = new Point(10, 115);
			this.btnHWSettingsATCommandsRemoveSelectedRow.Name = "btnHWSettingsATCommandsRemoveSelectedRow";
			this.btnHWSettingsATCommandsRemoveSelectedRow.Size = new Size(29, 21);
			this.btnHWSettingsATCommandsRemoveSelectedRow.TabIndex = 124;
			this.btnHWSettingsATCommandsRemoveSelectedRow.Text = "-";
			this.btnHWSettingsATCommandsRemoveSelectedRow.UseVisualStyleBackColor = true;
			this.btnHWSettingsATCommandsRemoveSelectedRow.Click += new EventHandler(this.btnHWSettingsATCommandsRemoveSelectedRow_Click);
			this.sePictureBox2.BackColor = Color.Transparent;
			this.sePictureBox2.BackgroundImage = Resources.SolarEdgeLogo;
			this.sePictureBox2.BackgroundImageLayout = ImageLayout.Zoom;
			this.sePictureBox2.ImageListBack = (List<Image>)componentResourceManager.GetObject("sePictureBox2.ImageListBack");
			this.sePictureBox2.ImageListFore = (List<Image>)componentResourceManager.GetObject("sePictureBox2.ImageListFore");
			this.sePictureBox2.Location = new Point(19, 358);
			this.sePictureBox2.Name = "sePictureBox2";
			this.sePictureBox2.SelectedImageBack = -1;
			this.sePictureBox2.SelectedImageFore = -1;
			this.sePictureBox2.Size = new Size(122, 29);
			this.sePictureBox2.TabIndex = 125;
			this.sePictureBox2.TabStop = false;
			this.btnLoadGSMATCmdsPreset.BackColor = Color.Silver;
			this.btnLoadGSMATCmdsPreset.Enabled = false;
			this.btnLoadGSMATCmdsPreset.Location = new Point(8, 36);
			this.btnLoadGSMATCmdsPreset.Name = "btnLoadGSMATCmdsPreset";
			this.btnLoadGSMATCmdsPreset.Size = new Size(107, 27);
			this.btnLoadGSMATCmdsPreset.TabIndex = 126;
			this.btnLoadGSMATCmdsPreset.Text = "Load GSM Preset";
			this.btnLoadGSMATCmdsPreset.UseVisualStyleBackColor = true;
			this.btnLoadGSMATCmdsPreset.Visible = false;
			this.btnLoadGSMATCmdsPreset.Click += new EventHandler(this.btnLoadGSMATCmdsPreset_Click);
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.DimGray;
			this.BackgroundImage = Resources.BackLandBigNew;
			this.BackgroundImageLayout = ImageLayout.Stretch;
			base.ClientSize = new Size(565, 395);
			base.ControlBox = false;
			base.Controls.Add(this.btnLoadGSMATCmdsPreset);
			base.Controls.Add(this.sePictureBox2);
			base.Controls.Add(this.btnHWSettingsATCommandsRemoveSelectedRow);
			base.Controls.Add(this.btnHWSettingsATCommandsAddNewRow);
			base.Controls.Add(this.lblHWSettingsATCommandsRefresh);
			base.Controls.Add(this.btnHWSettingsATCommandsConDisCon);
			base.Controls.Add(this.btnHWSettingsATCommandsAccept);
			base.Controls.Add(this.lblHWSettingsATCommandsModeValue);
			base.Controls.Add(this.lblHWSettingsATCommandsMode);
			base.Controls.Add(this.sePictureBox1);
			base.Controls.Add(this.lblHWSettingsATCommandsHeader);
			base.Controls.Add(this.dgvConnect);
			base.Controls.Add(this.dgvDisconnect);
			this.DoubleBuffered = true;
			base.FormBorderStyle = FormBorderStyle.None;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "HWSettingsATCommands";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			base.TransparencyKey = Color.DimGray;
			((ISupportInitialize)this.sePictureBox1).EndInit();
			((ISupportInitialize)this.dgvConnect).EndInit();
			((ISupportInitialize)this.dgvDisconnect).EndInit();
			((ISupportInitialize)this.sePictureBox2).EndInit();
			base.ResumeLayout(false);
		}

		private HWSettingsATCommands()
		{
			this.InitializeComponent();
			this.SetupLabels();
			this.UpdateUI(this);
			this.UpdateUISpecial();
			this._dicChangeConnectCmd = new Dictionary<int, string>(0);
			this._dicChangeConnectRsp = new Dictionary<int, string>(0);
			this._dicChangeDisconnectCmd = new Dictionary<int, string>(0);
			this._dicChangeDisconnectRsp = new Dictionary<int, string>(0);
		}

		protected void SetupLabels()
		{
			this.lblHWSettingsATCommandsHeader.SetUI(false, true);
			this.lblHWSettingsATCommandsMode.SetUI(true, true);
			this.lblHWSettingsATCommandsModeValue.SetUI(false, false);
		}

		private void UpdateUI(Control control)
		{
			try
			{
				if (control != null)
				{
					control.Text = this.GetTextForControl(control);
					if (control.Controls != null && control.Controls.Count > 0)
					{
						int count = control.Controls.Count;
						for (int i = 0; i < count; i++)
						{
							Control control2 = control.Controls[i];
							this.UpdateUI(control2);
						}
					}
				}
			}
			catch (Exception e)
			{
				ExceptionForm.Show(e);
			}
		}

		protected string GetTextForControl(Control control)
		{
			string result;
			if (control == null)
			{
				result = null;
			}
			else
			{
				string text = "";
				text = DataManager.Instance.Dictionary[control.Name];
				try
				{
					ISEUI iSEUI = (ISEUI)control;
					if (((ISEUI)control).IsAddColon())
					{
						text += ":";
					}
					else if (!((ISEUI)control).IsUseDictionary())
					{
						text = control.Text;
					}
				}
				catch (InvalidCastException var_3_7E)
				{
				}
				string text2 = text;
				result = text2;
			}
			return result;
		}

		private void UpdateUISpecial()
		{
			this.lblHWSettingsATCommandsModeValue.TSAccess.Text = DataManager.Instance.Dictionary[this._isConnectDataShown ? "lblHWSettingsATCommandsModeValueConnect" : "lblHWSettingsATCommandsModeValueDisconnect"];
			this.btnHWSettingsATCommandsConDisCon.TSAccess.Text = DataManager.Instance.Dictionary[this._isConnectDataShown ? "btnHWSettingsATCommandsConDisConDisconnect" : "btnHWSettingsATCommandsConDisConConnect"];
		}

		private void ParseData(string rawData)
		{
		}

		private void BuildTables()
		{
			if (this._tblParamConnect == null)
			{
				this._tblParamConnect = new DataTable("tableParamConnect");
				this._tblParamConnect.Columns.Add(new DataColumn("colId", typeof(int)));
				this._tblParamConnect.Columns.Add(new DataColumn("colCmd", typeof(string)));
				this._tblParamConnect.Columns.Add(new DataColumn("colSpace", typeof(string)));
				this._tblParamConnect.Columns.Add(new DataColumn("colRsp", typeof(string)));
			}
			else
			{
				this._tblParamConnect.Rows.Clear();
			}
			if (this._tblParamDisconnect == null)
			{
				this._tblParamDisconnect = new DataTable("tableParamDisconnect");
				this._tblParamDisconnect.Columns.Add(new DataColumn("colId", typeof(int)));
				this._tblParamDisconnect.Columns.Add(new DataColumn("colCmd", typeof(string)));
				this._tblParamDisconnect.Columns.Add(new DataColumn("colSpace", typeof(string)));
				this._tblParamDisconnect.Columns.Add(new DataColumn("colRsp", typeof(string)));
			}
			else
			{
				this._tblParamDisconnect.Rows.Clear();
			}
		}

		private void AddRow(DataTable table, int id, string cmd, string rsp)
		{
			if (table != null)
			{
				table.Rows.Add(new object[]
				{
					id,
					cmd,
					"-",
					rsp
				});
			}
		}

		private void ClearDGV(SEDataGridView dgv)
		{
			dgv.TSAccess.Rows.Clear();
		}

		private void AddDGVRow(SEDataGridView dgv, int row, string cmd, string rsp)
		{
			int count = dgv.TSAccess.Rows.Count;
			if (row >= count)
			{
				dgv.TSAccess.Rows.Add(new object[]
				{
					count + 1,
					cmd,
					"-",
					rsp
				});
			}
			else
			{
				dgv.TSAccess.Rows.Insert(row, new object[]
				{
					count + 1,
					cmd,
					"-",
					rsp
				});
			}
			this.SetCounters(dgv);
			this.CheckRowsForButtons(dgv);
			this.CheckAllCells(dgv, this._isConnectDataShown ? this._tblParamConnect : this._tblParamDisconnect);
		}

		private void RemoveDGVRow(SEDataGridView dgv, int row)
		{
			if (row >= 0 && row < dgv.TSAccess.Rows.Count)
			{
				dgv.TSAccess.Rows.RemoveAt(row);
				this.SetCounters(dgv);
				this.CheckRowsForButtons(dgv);
				this.CheckAllCells(dgv, this._isConnectDataShown ? this._tblParamConnect : this._tblParamDisconnect);
			}
		}

		private void SetCounters(SEDataGridView dgv)
		{
			int count = dgv.TSAccess.Rows.Count;
			for (int i = 0; i < count; i++)
			{
				dgv.TSAccess.Rows[i].Cells[0].Value = (i + 1).ToString();
			}
		}

		private void CheckRowsForButtons(SEDataGridView dgv)
		{
			if ((this._isConnectDataShown && dgv.Name.Equals(this.dgvConnect.Name) && dgv.Rows.Count < 20) || (!this._isConnectDataShown && dgv.Name.Equals(this.dgvDisconnect.Name) && dgv.Rows.Count < 10))
			{
				this.btnHWSettingsATCommandsAddNewRow.TSAccess.Enabled = true;
			}
			else
			{
				this.btnHWSettingsATCommandsAddNewRow.TSAccess.Enabled = false;
			}
			if (dgv.Rows.Count > 0)
			{
				this.btnHWSettingsATCommandsRemoveSelectedRow.TSAccess.Enabled = true;
			}
			else
			{
				this.btnHWSettingsATCommandsRemoveSelectedRow.TSAccess.Enabled = false;
			}
		}

		private void CheckCell(SEDataGridView dgv, DataTable matchingTable, int row)
		{
			Color khaki = Color.Khaki;
			Color white = Color.White;
			Color lightPink = Color.LightPink;
			if (row < dgv.TSAccess.Rows.Count)
			{
				string text = matchingTable.Rows[row].ItemArray[1].ToString();
				string text2 = matchingTable.Rows[row].ItemArray[3].ToString();
				if (dgv.TSAccess.Rows[row].Cells[1] != null && dgv.TSAccess.Rows[row].Cells[1].Value != null)
				{
					dgv.TSAccess.Rows[row].Cells[1].Style.BackColor = (dgv.TSAccess.Rows[row].Cells[1].Value.ToString().Equals(text.ToString()) ? white : khaki);
				}
				if (dgv.TSAccess.Rows[row].Cells[3] != null && dgv.TSAccess.Rows[row].Cells[3].Value != null)
				{
					dgv.TSAccess.Rows[row].Cells[3].Style.BackColor = (dgv.TSAccess.Rows[row].Cells[3].Value.ToString().Equals(text2.ToString()) ? white : khaki);
				}
				if (row < dgv.Rows.Count - 1 && dgv.TSAccess.Rows[row].Cells[1] != null && (dgv.TSAccess.Rows[row].Cells[1].Value == null || dgv.TSAccess.Rows[row].Cells[1].Value.ToString() == "") && dgv.TSAccess.Rows[row].Cells[3] != null && (dgv.TSAccess.Rows[row].Cells[3].Value != null || dgv.TSAccess.Rows[row].Cells[3].Value.ToString() == ""))
				{
					dgv.TSAccess.Rows[row].Cells[1].Style.BackColor = lightPink;
					dgv.TSAccess.Rows[row].Cells[3].Style.BackColor = lightPink;
				}
			}
		}

		private void CheckAllCells(SEDataGridView dgv, DataTable matchingTable)
		{
			int count = dgv.TSAccess.Rows.Count;
			for (int i = 0; i < count; i++)
			{
				this.CheckCell(dgv, matchingTable, i);
			}
		}

		private void RevertToParameters(SEDataGridView dgv, DataTable table)
		{
			int count = table.Rows.Count;
			dgv.TSAccess.Rows.Clear();
			for (int i = 0; i < count; i++)
			{
				string text = table.Rows[i].ItemArray[1].ToString();
				string text2 = table.Rows[i].ItemArray[3].ToString();
				if (!string.IsNullOrEmpty(text) || !string.IsNullOrEmpty(text2))
				{
					this.AddDGVRow(dgv, i, text, text2);
				}
			}
		}

		private bool CopyCellsForCommit()
		{
			bool flag = true;
			this._dicChangeConnectCmd.Clear();
			this._dicChangeConnectRsp.Clear();
			this._dicChangeDisconnectCmd.Clear();
			this._dicChangeDisconnectRsp.Clear();
			int count = this.dgvConnect.TSAccess.Rows.Count;
			bool result;
			for (int i = 0; i < count; i++)
			{
				try
				{
					string value = this.dgvConnect.TSAccess.Rows[i].Cells[1].Value.ToString();
					string value2 = this.dgvConnect.TSAccess.Rows[i].Cells[3].Value.ToString();
					if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(value2))
					{
						result = false;
						return result;
					}
					this._dicChangeConnectCmd.Add(i, value);
					this._dicChangeConnectRsp.Add(i, value2);
				}
				catch (Exception var_5_F0)
				{
					result = false;
					return result;
				}
			}
			count = this.dgvDisconnect.TSAccess.Rows.Count;
			for (int i = 0; i < count; i++)
			{
				try
				{
					string value = this.dgvDisconnect.Rows[i].Cells[1].Value.ToString();
					string value2 = this.dgvDisconnect.Rows[i].Cells[3].Value.ToString();
					if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(value2))
					{
						result = false;
						return result;
					}
					this._dicChangeDisconnectCmd.Add(i, value);
					this._dicChangeDisconnectRsp.Add(i, value2);
				}
				catch (Exception var_5_F0)
				{
					result = false;
					return result;
				}
			}
			result = flag;
			return result;
		}

		public static void ShowHWSettingsATCommands(Form owner, bool refreshData)
		{
			if (HWSettingsATCommands._instance == null)
			{
				HWSettingsATCommands._instance = new HWSettingsATCommands();
			}
			HWSettingsATCommands._instance.Inverter = DataManager.Instance.GetSelectedInverter();
			HWSettingsATCommands._instance.Action_GetParams();
			HWSettingsATCommands._instance._isConnectDataShown = true;
			HWSettingsATCommands._instance.DisplayCommands();
			HWSettingsATCommands._instance.UpdateUISpecial();
			HWSettingsATCommands._instance.ShowDialog(owner);
		}

		public static Dictionary<int, string> GetChangeList(bool isConnect, bool isCommand)
		{
			Dictionary<int, string> result = null;
			if (HWSettingsATCommands._instance != null)
			{
				if (isConnect)
				{
					result = (isCommand ? HWSettingsATCommands._instance._dicChangeConnectCmd : HWSettingsATCommands._instance._dicChangeConnectRsp);
				}
				else
				{
					result = (isCommand ? HWSettingsATCommands._instance._dicChangeDisconnectCmd : HWSettingsATCommands._instance._dicChangeDisconnectRsp);
				}
			}
			return result;
		}

		private void DisplayCommands()
		{
			this.dgvConnect.TSAccess.Visible = this._isConnectDataShown;
			this.dgvDisconnect.TSAccess.Visible = !this._isConnectDataShown;
		}

		private void GSMParamUpdate()
		{
			DataManager instance = DataManager.Instance;
			this.BuildTables();
			int num = 20;
			int num2 = 10;
			instance.UIOwner.UpdateProgressBar(DataManager.Instance.Dictionary["msgGettingATCommands"], 0f);
			List<PortiaParams> list = new List<PortiaParams>(0);
			for (int i = 0; i < num; i++)
			{
				string value = string.Format("GSM_CONN_CMD_{0}", i + 1);
				string value2 = string.Format("GSM_CONN_RESP_{0}", i + 1);
				PortiaParams item = (PortiaParams)Enum.Parse(typeof(PortiaParams), value);
				PortiaParams item2 = (PortiaParams)Enum.Parse(typeof(PortiaParams), value2);
				list.Add(item);
				list.Add(item2);
			}
			for (int i = 0; i < num2; i++)
			{
				string value = string.Format("GSM_DISCONN_CMD_{0}", i + 1);
				string value2 = string.Format("GSM_DISCONN_RESP_{0}", i + 1);
				PortiaParams item3 = (PortiaParams)Enum.Parse(typeof(PortiaParams), value);
				PortiaParams item4 = (PortiaParams)Enum.Parse(typeof(PortiaParams), value2);
				list.Add(item3);
				list.Add(item4);
			}
			string[] paramPortia = this.Inverter.GetParamPortia(list.ToArray());
			instance.UIOwner.UpdateProgressBar(DataManager.Instance.Dictionary["msgGettingATCommandsParamsRetrieved"], 50f);
			if (paramPortia != null)
			{
				int num3 = paramPortia.Length;
				int num4 = 0;
				int num5 = num * 2;
				for (int j = num4; j < num5; j += 2)
				{
					string cmd = paramPortia[j];
					string rsp = paramPortia[j + 1];
					this.AddRow(this._tblParamConnect, j, cmd, rsp);
				}
				num4 = num5;
				num5 += num2 * 2;
				for (int j = num4; j < num5; j += 2)
				{
					string cmd2 = paramPortia[j];
					string rsp2 = paramPortia[j + 1];
					this.AddRow(this._tblParamDisconnect, j, cmd2, rsp2);
				}
				instance.UIOwner.UpdateProgressBar(DataManager.Instance.Dictionary["msgGettingATCommandsParamsLoaded"], 100f);
				if (this._dicChangeConnectCmd.Count == 0 && this._dicChangeConnectRsp.Count == 0 && this._dicChangeDisconnectCmd.Count == 0 && this._dicChangeDisconnectRsp.Count == 0)
				{
					this.RevertToParameters(this.dgvConnect, this._tblParamConnect);
					this.RevertToParameters(this.dgvDisconnect, this._tblParamDisconnect);
				}
				this.CheckAllCells(this.dgvConnect, this._tblParamConnect);
				this.CheckAllCells(this.dgvDisconnect, this._tblParamDisconnect);
			}
		}

		public void Action_GetParams()
		{
			DataManager.Instance.UIOwner.ShowProgressBar(0, 100, new SEProgressBar.ProgressFormCallBack(this.GSMParamUpdate), this);
		}

		protected override void OnShown(EventArgs e)
		{
			this.UpdateUI(this);
			base.OnShown(e);
		}

		private void btnHWSettingsATCommandsConDisCon_Click(object sender, EventArgs e)
		{
			this._isConnectDataShown = !this._isConnectDataShown;
			this.DisplayCommands();
			this.UpdateUISpecial();
			this.CheckRowsForButtons(this._isConnectDataShown ? this.dgvConnect : this.dgvDisconnect);
		}

		private void btnHWSettingsATCommandsAccept_Click(object sender, EventArgs e)
		{
			bool flag = this.CopyCellsForCommit();
			if (flag)
			{
				base.Close();
			}
			else
			{
				string text = DataManager.Instance.Dictionary["MsgATCommandsErrorMessage"];
				text.Replace("@#", "\r\n");
				string caption = DataManager.Instance.Dictionary["MsgATCommandsErrorCaption"];
				MessageBoxIcon icon = MessageBoxIcon.Hand;
				MessageBoxButtons buttons = MessageBoxButtons.YesNo;
				MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1;
				switch (MessageBox.Show(text, caption, buttons, icon, defaultButton))
				{
				case DialogResult.No:
					base.Close();
					this.RevertToParameters(this.dgvConnect, this._tblParamConnect);
					this.RevertToParameters(this.dgvDisconnect, this._tblParamDisconnect);
					break;
				}
			}
		}

		private void lblHWSettingsATCommandsRefresh_Click(object sender, EventArgs e)
		{
			this.Action_GetParams();
		}

		private void btnHWSettingsATCommandsAddNewRow_Click(object sender, EventArgs e)
		{
			SEDataGridView sEDataGridView = this._isConnectDataShown ? this.dgvConnect : this.dgvDisconnect;
			int row = (sEDataGridView.SelectedRows != null && sEDataGridView.SelectedRows.Count > 0) ? sEDataGridView.SelectedRows[0].Index : sEDataGridView.Rows.Count;
			this.AddDGVRow(this._isConnectDataShown ? this.dgvConnect : this.dgvDisconnect, row, "", "");
		}

		private void btnHWSettingsATCommandsRemoveSelectedRow_Click(object sender, EventArgs e)
		{
			SEDataGridView sEDataGridView = this._isConnectDataShown ? this.dgvConnect : this.dgvDisconnect;
			int num = (sEDataGridView.SelectedRows != null && sEDataGridView.SelectedRows.Count > 0) ? sEDataGridView.SelectedRows[0].Index : -1;
			if (num > -1)
			{
				this.RemoveDGVRow(this._isConnectDataShown ? this.dgvConnect : this.dgvDisconnect, num);
			}
		}

		private void btnLoadGSMATCmdsPreset_Click(object sender, EventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.DefaultExt = ".atcc";
			openFileDialog.AddExtension = true;
			openFileDialog.AutoUpgradeEnabled = true;
			openFileDialog.Title = DataManager.Instance.Dictionary["MsgATCommandsOpenPresetTitle"];
			openFileDialog.ValidateNames = true;
			openFileDialog.Filter = DataManager.Instance.Dictionary["MsgATCommandsOpenPresetFilter"] + "|*.atcc";
			DialogResult dialogResult = openFileDialog.ShowDialog();
			if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes)
			{
				string fileName = openFileDialog.FileName;
				try
				{
					FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
					StreamReader streamReader = new StreamReader(stream);
					string rawData = streamReader.ReadToEnd();
					this.ParseData(rawData);
				}
				catch (Exception var_6_B7)
				{
					string text = DataManager.Instance.Dictionary["MsgATCommandsOpenPresetFilterErrorMessage"];
					string caption = DataManager.Instance.Dictionary["MsgATCommandsOpenPresetFilterErrorCaption"];
					MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void dgvConnect_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			int rowIndex = e.RowIndex;
			int columnIndex = e.ColumnIndex;
			if (columnIndex == 1 || columnIndex == 3)
			{
				SEDataGridView dgv = (SEDataGridView)sender;
				this.CheckCell(dgv, this._tblParamConnect, rowIndex);
			}
		}

		private void dgvDisconnect_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			int rowIndex = e.RowIndex;
			int columnIndex = e.ColumnIndex;
			if (columnIndex == 1 || columnIndex == 3)
			{
				SEDataGridView dgv = (SEDataGridView)sender;
				this.CheckCell(dgv, this._tblParamDisconnect, rowIndex);
			}
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblHWSettingsATCommandsHeader.Size.Height);
			base.OnMouseDown(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
			base.OnMouseMove(e);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
			base.OnMouseUp(e);
		}

		private void lblHWSettingsATCommandsHeader_MouseDown(object sender, MouseEventArgs e)
		{
			base.HandleMouseDown(e.X, e.Y, this.lblHWSettingsATCommandsHeader.Size.Height);
		}

		private void lblHWSettingsATCommandsHeader_MouseMove(object sender, MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
		}

		private void lblHWSettingsATCommandsHeader_MouseUp(object sender, MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
		}
	}
}
