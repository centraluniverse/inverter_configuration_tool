using InverterConfigurationTool.Properties;
using InverterConfigurationTool.Src.Engine;
using InverterConfigurationTool.Src.Model.Data;
using InverterConfigurationTool.Src.Model.Devices;
using InverterConfigurationTool.Src.Model.Devices.Base;
using InverterConfigurationTool.Src.Ui.Main;
using SEDevices.Data;
using SEDevices.Devices;
using SEStorage;
using SEUI.Base;
using SEUI.Common;
using SEUI.Containers;
using SEUI.Extra;
using SEUI.ThreadSafeAccess;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace InverterConfigurationTool.Src.Ui.DataEntry
{
	public class HWSettings : SEForm
	{
		private IContainer components = null;

		private SELabel lblHWSettingsZBHeader;

		private SEPanel pnlHWSettingsZB;

		private SETextBox txtHWSettingsZBPanID;

		private SELabel lblHWSettingsZBPanID;

		private SECheckBox chkBxHWSettingsZBScanChannel1;

		private SELabel lblHWSettingsZBScanChannel1;

		private SELabel lblHWSettingsZBScanChannel0;

		private SECheckBox chkBxHWSettingsZBScanChannel0;

		private SECheckBox chkBxHWSettingsZBScanChannel15;

		private SECheckBox chkBxHWSettingsZBScanChannel14;

		private SECheckBox chkBxHWSettingsZBScanChannel13;

		private SECheckBox chkBxHWSettingsZBScanChannel12;

		private SECheckBox chkBxHWSettingsZBScanChannel11;

		private SECheckBox chkBxHWSettingsZBScanChannel10;

		private SECheckBox chkBxHWSettingsZBScanChannel9;

		private SECheckBox chkBxHWSettingsZBScanChannel8;

		private SECheckBox chkBxHWSettingsZBScanChannel7;

		private SECheckBox chkBxHWSettingsZBScanChannel6;

		private SECheckBox chkBxHWSettingsZBScanChannel5;

		private SECheckBox chkBxHWSettingsZBScanChannel4;

		private SECheckBox chkBxHWSettingsZBScanChannel3;

		private SECheckBox chkBxHWSettingsZBScanChannel2;

		private SELabel lblHWSettingsZBScanChannel5;

		private SELabel lblHWSettingsZBScanChannel4;

		private SELabel lblHWSettingsZBScanChannel3;

		private SELabel lblHWSettingsZBScanChannel2;

		private SELabel lblHWSettingsZBConfig;

		private SEButton btnSettingsApply;

		private SEButton btnSettingsCancel;

		private SEPanel pnlHWSettingsLAN;

		private SELabel lblHWSettingsLANHeader;

		private SETextBox txtHWSettingsLANCurrentIP;

		private SELabel lbHWlSettingsLANCurrentIP;

		private SEPictureBox pbHWSettingsZB;

		private SEPictureBox pbHWSettingsLAN;

		private SELabel lblHWSettingsLANSubnetMask;

		private SELabel lblHWSettingsLANHeaderCurrent;

		private SETextBox txtHWSettingsLANDNS;

		private SELabel lblHWSettingsLANDNS;

		private SETextBox txtHWSettingsLANGateway;

		private SELabel lblHWSettingsLANGateway;

		private SETextBox txtHWSettingsLANSubnetMask;

		private SECheckBox chkBxHWSettingsLANDHCP;

		private SELabel lblHWSettingsLANHeaderDetected;

		private SEPanel pnlHWSettingsPingTest;

		private SEPictureBox pbHWSettingsPingTest;

		private SELabel lblHWSettingsLANPingTestHeader;

		private SELabel lblHWSettingsPingTestAddress;

		private SEButton btnHWSettingsPingTestClear;

		private SETextBox txtHWSettingsPingResult;

		private SEButton btnHWSettingsPingTestPing;

		private SETextBox txtHWSettingsPingTestURL;

		private SEPanel pnlHWSettingsRS232;

		private SEPictureBox pbHWSettingsRS232;

		private SEComboBox cmbHWSettingsRS232CommMode;

		private SELabel lblHWSettingsRS232CommMode;

		private SELabel lblHWSettingsRS232Header;

		private SETextBox txtHWSettingsRS232GSMPassword;

		private SELabel lblHWSettingsRS232GSMPassword;

		private SETextBox txtHWSettingsRS232GSMUsername;

		private SELabel lblHWSettingsRS232GSMUsername;

		private SETextBox txtHWSettingsRS232GSMAPN;

		private SELabel lblHWSettingsRS232GSMAPN;

		private SEPictureBox pbHWSettingsRS232GSM;

		private SEPanel pnlHWSettingsServer;

		private SETextBox txtHWSettingsServerPort;

		private SELabel lblHWSettingsServerPort;

		private SETextBox txtHWSettingsServerAddress;

		private SELabel lblHWSettingsServerAddress;

		private SEPictureBox pbHWSettingsServer;

		private SEComboBox cmbHWSettingsServerVia;

		private SELabel lblHWSettingsServerVia;

		private SELabel lblHWSettingsServerHeader;

		private SEPanel pnlHWSettingsIIC;

		private SEComboBox cmbHWSettingsIICCommType;

		private SELabel lblHWSettingsIICCommType;

		private SEPictureBox pbHWSettingsIIC;

		private SELabel lblHWSettingsIICHeader;

		private SEPanel pnlHWSettingsRS485;

		private SEComboBox cmbHWSettingsRS485Configuration;

		private SELabel lblHWSettingsRS485Configuration;

		private SEPictureBox pbHWSettingsRS485;

		private SELabel lblHWSettingsRS485Header;

		private SELabel lblHWSettingsZBScanChannelAll;

		private SECheckBox chkBxHWSettingsZBScanChannelAll;

		private SELabel lblHWSettingsRS232GSMModemType;

		private SEComboBox cmbHWSettingsRS232GSMModemType;

		private SEComboBox cmbHWSettingsIICCommMode;

		private SELabel lblHWSettingsIICCommMode;

		private SEGroupBox grbHWSettingsZBScanChannel;

		private SEButton btnHWSettingsRS232ShowATCommands;

		private SELabel lblHWSettingsStringSizeProblem;

		private SEPictureBox sePictureBox1;

		private SELabel lblHeader;

		private SELabel lblHWSettingsLANCurrentIPDetected;

		private SELabel lblHWSettingsLANDNSDetected;

		private SELabel lblHWSettingsLANGatewayDetected;

		private SELabel lblHWSettingsLANSubnetMaskDetected;

		private SELabel lblHWSettingsZBConfiguration;

		private SECheckBox chkHWSettingsZBConfigurationEnabled;

		private SELabel lblHWSettingsZBScanChannel6;

		private SELabel lblHWSettingsZBScanChannel8;

		private SELabel lblHWSettingsZBScanChannel7;

		private SELabel lblHWSettingsZBScanChannel12;

		private SELabel lblHWSettingsZBScanChannel11;

		private SELabel lblHWSettingsZBScanChannel10;

		private SELabel lblHWSettingsZBScanChannel9;

		private SELabel lblHWSettingsZBScanChannel13;

		private SELabel lblHWSettingsZBScanChannel14;

		private SELabel lblHWSettingsZBScanChannel15;

		private static Color COLOR_GOOD = Color.White;

		private static Color COLOR_BAD = Color.FromArgb(255, 192, 192);

		private int[] _scanChannelMask = new int[]
		{
			11,
			12,
			13,
			14,
			15,
			16,
			17,
			18,
			19,
			20,
			21,
			22,
			23,
			24,
			25,
			26
		};

		private bool[] _scanChannelMaskUse;

		private Inverter _inverter;

		private static HWSettings _instance = null;

		private bool _wasRS232GSMStringErrorShown;

		private bool _shouldCheckApplyBeActive;

		private bool _shouldChkBxEventRun;

		public Inverter Inverter
		{
			get
			{
				return this._inverter;
			}
			set
			{
				this._inverter = value;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(HWSettings));
			this.lblHWSettingsZBHeader = new SELabel();
			this.pnlHWSettingsZB = new SEPanel();
			this.chkHWSettingsZBConfigurationEnabled = new SECheckBox();
			this.lblHWSettingsZBConfiguration = new SELabel();
			this.grbHWSettingsZBScanChannel = new SEGroupBox();
			this.lblHWSettingsZBScanChannel15 = new SELabel();
			this.lblHWSettingsZBScanChannel14 = new SELabel();
			this.lblHWSettingsZBScanChannel13 = new SELabel();
			this.lblHWSettingsZBScanChannel12 = new SELabel();
			this.lblHWSettingsZBScanChannel11 = new SELabel();
			this.lblHWSettingsZBScanChannel10 = new SELabel();
			this.lblHWSettingsZBScanChannel9 = new SELabel();
			this.lblHWSettingsZBScanChannel8 = new SELabel();
			this.lblHWSettingsZBScanChannel7 = new SELabel();
			this.lblHWSettingsZBScanChannel6 = new SELabel();
			this.lblHWSettingsZBScanChannelAll = new SELabel();
			this.chkBxHWSettingsZBScanChannelAll = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel1 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel2 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel3 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel4 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel5 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel6 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel7 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel8 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel9 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel10 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel11 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel12 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel13 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel14 = new SECheckBox();
			this.chkBxHWSettingsZBScanChannel15 = new SECheckBox();
			this.lblHWSettingsZBScanChannel5 = new SELabel();
			this.chkBxHWSettingsZBScanChannel0 = new SECheckBox();
			this.lblHWSettingsZBScanChannel4 = new SELabel();
			this.lblHWSettingsZBScanChannel0 = new SELabel();
			this.lblHWSettingsZBScanChannel3 = new SELabel();
			this.lblHWSettingsZBScanChannel1 = new SELabel();
			this.lblHWSettingsZBScanChannel2 = new SELabel();
			this.pbHWSettingsZB = new SEPictureBox();
			this.lblHWSettingsZBConfig = new SELabel();
			this.txtHWSettingsZBPanID = new SETextBox();
			this.lblHWSettingsZBPanID = new SELabel();
			this.btnSettingsApply = new SEButton();
			this.btnSettingsCancel = new SEButton();
			this.pnlHWSettingsLAN = new SEPanel();
			this.lblHWSettingsLANDNSDetected = new SELabel();
			this.lblHWSettingsLANGatewayDetected = new SELabel();
			this.lblHWSettingsLANSubnetMaskDetected = new SELabel();
			this.lblHWSettingsLANCurrentIPDetected = new SELabel();
			this.chkBxHWSettingsLANDHCP = new SECheckBox();
			this.lblHWSettingsLANHeaderDetected = new SELabel();
			this.lblHWSettingsLANHeaderCurrent = new SELabel();
			this.txtHWSettingsLANDNS = new SETextBox();
			this.lblHWSettingsLANDNS = new SELabel();
			this.txtHWSettingsLANGateway = new SETextBox();
			this.lblHWSettingsLANGateway = new SELabel();
			this.txtHWSettingsLANSubnetMask = new SETextBox();
			this.lblHWSettingsLANSubnetMask = new SELabel();
			this.pbHWSettingsLAN = new SEPictureBox();
			this.lblHWSettingsLANHeader = new SELabel();
			this.txtHWSettingsLANCurrentIP = new SETextBox();
			this.lbHWlSettingsLANCurrentIP = new SELabel();
			this.pnlHWSettingsPingTest = new SEPanel();
			this.lblHWSettingsPingTestAddress = new SELabel();
			this.btnHWSettingsPingTestClear = new SEButton();
			this.txtHWSettingsPingResult = new SETextBox();
			this.btnHWSettingsPingTestPing = new SEButton();
			this.txtHWSettingsPingTestURL = new SETextBox();
			this.pbHWSettingsPingTest = new SEPictureBox();
			this.lblHWSettingsLANPingTestHeader = new SELabel();
			this.pnlHWSettingsRS232 = new SEPanel();
			this.lblHWSettingsStringSizeProblem = new SELabel();
			this.btnHWSettingsRS232ShowATCommands = new SEButton();
			this.cmbHWSettingsRS232GSMModemType = new SEComboBox();
			this.lblHWSettingsRS232GSMModemType = new SELabel();
			this.pbHWSettingsRS232GSM = new SEPictureBox();
			this.txtHWSettingsRS232GSMPassword = new SETextBox();
			this.lblHWSettingsRS232GSMPassword = new SELabel();
			this.txtHWSettingsRS232GSMUsername = new SETextBox();
			this.lblHWSettingsRS232GSMUsername = new SELabel();
			this.txtHWSettingsRS232GSMAPN = new SETextBox();
			this.lblHWSettingsRS232GSMAPN = new SELabel();
			this.pbHWSettingsRS232 = new SEPictureBox();
			this.cmbHWSettingsRS232CommMode = new SEComboBox();
			this.lblHWSettingsRS232CommMode = new SELabel();
			this.lblHWSettingsRS232Header = new SELabel();
			this.pnlHWSettingsServer = new SEPanel();
			this.txtHWSettingsServerPort = new SETextBox();
			this.lblHWSettingsServerPort = new SELabel();
			this.txtHWSettingsServerAddress = new SETextBox();
			this.lblHWSettingsServerAddress = new SELabel();
			this.pbHWSettingsServer = new SEPictureBox();
			this.cmbHWSettingsServerVia = new SEComboBox();
			this.lblHWSettingsServerVia = new SELabel();
			this.lblHWSettingsServerHeader = new SELabel();
			this.pnlHWSettingsIIC = new SEPanel();
			this.cmbHWSettingsIICCommMode = new SEComboBox();
			this.lblHWSettingsIICCommMode = new SELabel();
			this.cmbHWSettingsIICCommType = new SEComboBox();
			this.lblHWSettingsIICCommType = new SELabel();
			this.pbHWSettingsIIC = new SEPictureBox();
			this.lblHWSettingsIICHeader = new SELabel();
			this.pnlHWSettingsRS485 = new SEPanel();
			this.cmbHWSettingsRS485Configuration = new SEComboBox();
			this.lblHWSettingsRS485Configuration = new SELabel();
			this.pbHWSettingsRS485 = new SEPictureBox();
			this.lblHWSettingsRS485Header = new SELabel();
			this.sePictureBox1 = new SEPictureBox();
			this.lblHeader = new SELabel();
			this.pnlHWSettingsZB.SuspendLayout();
			this.grbHWSettingsZBScanChannel.SuspendLayout();
			((ISupportInitialize)this.pbHWSettingsZB).BeginInit();
			this.pnlHWSettingsLAN.SuspendLayout();
			((ISupportInitialize)this.pbHWSettingsLAN).BeginInit();
			this.pnlHWSettingsPingTest.SuspendLayout();
			((ISupportInitialize)this.pbHWSettingsPingTest).BeginInit();
			this.pnlHWSettingsRS232.SuspendLayout();
			((ISupportInitialize)this.pbHWSettingsRS232GSM).BeginInit();
			((ISupportInitialize)this.pbHWSettingsRS232).BeginInit();
			this.pnlHWSettingsServer.SuspendLayout();
			((ISupportInitialize)this.pbHWSettingsServer).BeginInit();
			this.pnlHWSettingsIIC.SuspendLayout();
			((ISupportInitialize)this.pbHWSettingsIIC).BeginInit();
			this.pnlHWSettingsRS485.SuspendLayout();
			((ISupportInitialize)this.pbHWSettingsRS485).BeginInit();
			((ISupportInitialize)this.sePictureBox1).BeginInit();
			base.SuspendLayout();
			this.lblHWSettingsZBHeader.BackColor = Color.Transparent;
			this.lblHWSettingsZBHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsZBHeader.Location = new Point(16, 3);
			this.lblHWSettingsZBHeader.Name = "lblHWSettingsZBHeader";
			this.lblHWSettingsZBHeader.Size = new Size(372, 26);
			this.lblHWSettingsZBHeader.TabIndex = 7;
			this.lblHWSettingsZBHeader.Text = "ZigBee Settings";
			this.lblHWSettingsZBHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsZBHeader.MouseDown += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseDown);
			this.lblHWSettingsZBHeader.MouseMove += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseMove);
			this.lblHWSettingsZBHeader.MouseUp += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseUp);
			this.pnlHWSettingsZB.BackColor = Color.Transparent;
			this.pnlHWSettingsZB.Controls.Add(this.chkHWSettingsZBConfigurationEnabled);
			this.pnlHWSettingsZB.Controls.Add(this.lblHWSettingsZBConfiguration);
			this.pnlHWSettingsZB.Controls.Add(this.grbHWSettingsZBScanChannel);
			this.pnlHWSettingsZB.Controls.Add(this.pbHWSettingsZB);
			this.pnlHWSettingsZB.Controls.Add(this.lblHWSettingsZBConfig);
			this.pnlHWSettingsZB.Controls.Add(this.lblHWSettingsZBHeader);
			this.pnlHWSettingsZB.Controls.Add(this.txtHWSettingsZBPanID);
			this.pnlHWSettingsZB.Controls.Add(this.lblHWSettingsZBPanID);
			this.pnlHWSettingsZB.Location = new Point(276, 234);
			this.pnlHWSettingsZB.Name = "pnlHWSettingsZB";
			this.pnlHWSettingsZB.Size = new Size(402, 191);
			this.pnlHWSettingsZB.TabIndex = 8;
			this.chkHWSettingsZBConfigurationEnabled.Cursor = Cursors.Hand;
			this.chkHWSettingsZBConfigurationEnabled.Location = new Point(252, 39);
			this.chkHWSettingsZBConfigurationEnabled.Name = "chkHWSettingsZBConfigurationEnabled";
			this.chkHWSettingsZBConfigurationEnabled.Size = new Size(76, 27);
			this.chkHWSettingsZBConfigurationEnabled.TabIndex = 105;
			this.chkHWSettingsZBConfigurationEnabled.Text = "Enable";
			this.chkHWSettingsZBConfigurationEnabled.TextAlign = ContentAlignment.MiddleCenter;
			this.chkHWSettingsZBConfigurationEnabled.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkHWSettingsZBConfigurationEnabled.UseVisualStyleBackColor = true;
			this.chkHWSettingsZBConfigurationEnabled.CheckedChanged += new EventHandler(this.chkHWSettingsZBConfigurationEnabled_CheckedChanged);
			this.lblHWSettingsZBConfiguration.BackColor = Color.White;
			this.lblHWSettingsZBConfiguration.BorderStyle = BorderStyle.FixedSingle;
			this.lblHWSettingsZBConfiguration.Location = new Point(113, 42);
			this.lblHWSettingsZBConfiguration.Name = "lblHWSettingsZBConfiguration";
			this.lblHWSettingsZBConfiguration.Size = new Size(135, 20);
			this.lblHWSettingsZBConfiguration.TabIndex = 102;
			this.lblHWSettingsZBConfiguration.TextAlign = ContentAlignment.MiddleCenter;
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel15);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel14);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel13);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel12);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel11);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel10);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel9);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel8);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel7);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel6);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannelAll);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannelAll);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel1);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel2);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel3);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel4);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel5);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel6);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel7);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel8);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel9);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel10);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel11);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel12);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel13);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel14);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel15);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel5);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.chkBxHWSettingsZBScanChannel0);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel4);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel0);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel3);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel1);
			this.grbHWSettingsZBScanChannel.Controls.Add(this.lblHWSettingsZBScanChannel2);
			this.grbHWSettingsZBScanChannel.Location = new Point(53, 105);
			this.grbHWSettingsZBScanChannel.Name = "grbHWSettingsZBScanChannel";
			this.grbHWSettingsZBScanChannel.Size = new Size(289, 65);
			this.grbHWSettingsZBScanChannel.TabIndex = 101;
			this.grbHWSettingsZBScanChannel.TabStop = false;
			this.grbHWSettingsZBScanChannel.Text = "Scan Channel";
			this.lblHWSettingsZBScanChannel15.Location = new Point(239, 38);
			this.lblHWSettingsZBScanChannel15.Name = "lblHWSettingsZBScanChannel15";
			this.lblHWSettingsZBScanChannel15.Size = new Size(23, 19);
			this.lblHWSettingsZBScanChannel15.TabIndex = 111;
			this.lblHWSettingsZBScanChannel15.Text = "1A";
			this.lblHWSettingsZBScanChannel14.Location = new Point(224, 38);
			this.lblHWSettingsZBScanChannel14.Name = "lblHWSettingsZBScanChannel14";
			this.lblHWSettingsZBScanChannel14.Size = new Size(23, 19);
			this.lblHWSettingsZBScanChannel14.TabIndex = 110;
			this.lblHWSettingsZBScanChannel14.Text = "19";
			this.lblHWSettingsZBScanChannel13.Location = new Point(209, 38);
			this.lblHWSettingsZBScanChannel13.Name = "lblHWSettingsZBScanChannel13";
			this.lblHWSettingsZBScanChannel13.Size = new Size(23, 19);
			this.lblHWSettingsZBScanChannel13.TabIndex = 109;
			this.lblHWSettingsZBScanChannel13.Text = "18";
			this.lblHWSettingsZBScanChannel12.Location = new Point(193, 38);
			this.lblHWSettingsZBScanChannel12.Name = "lblHWSettingsZBScanChannel12";
			this.lblHWSettingsZBScanChannel12.Size = new Size(23, 19);
			this.lblHWSettingsZBScanChannel12.TabIndex = 108;
			this.lblHWSettingsZBScanChannel12.Text = "17";
			this.lblHWSettingsZBScanChannel11.Location = new Point(177, 38);
			this.lblHWSettingsZBScanChannel11.Name = "lblHWSettingsZBScanChannel11";
			this.lblHWSettingsZBScanChannel11.Size = new Size(23, 19);
			this.lblHWSettingsZBScanChannel11.TabIndex = 107;
			this.lblHWSettingsZBScanChannel11.Text = "16";
			this.lblHWSettingsZBScanChannel10.Location = new Point(162, 38);
			this.lblHWSettingsZBScanChannel10.Name = "lblHWSettingsZBScanChannel10";
			this.lblHWSettingsZBScanChannel10.Size = new Size(23, 19);
			this.lblHWSettingsZBScanChannel10.TabIndex = 106;
			this.lblHWSettingsZBScanChannel10.Text = "15";
			this.lblHWSettingsZBScanChannel9.Location = new Point(146, 38);
			this.lblHWSettingsZBScanChannel9.Name = "lblHWSettingsZBScanChannel9";
			this.lblHWSettingsZBScanChannel9.Size = new Size(23, 19);
			this.lblHWSettingsZBScanChannel9.TabIndex = 105;
			this.lblHWSettingsZBScanChannel9.Text = "14";
			this.lblHWSettingsZBScanChannel8.Location = new Point(131, 38);
			this.lblHWSettingsZBScanChannel8.Name = "lblHWSettingsZBScanChannel8";
			this.lblHWSettingsZBScanChannel8.Size = new Size(23, 19);
			this.lblHWSettingsZBScanChannel8.TabIndex = 103;
			this.lblHWSettingsZBScanChannel8.Text = "13";
			this.lblHWSettingsZBScanChannel7.Location = new Point(115, 38);
			this.lblHWSettingsZBScanChannel7.Name = "lblHWSettingsZBScanChannel7";
			this.lblHWSettingsZBScanChannel7.Size = new Size(23, 19);
			this.lblHWSettingsZBScanChannel7.TabIndex = 102;
			this.lblHWSettingsZBScanChannel7.Text = "12";
			this.lblHWSettingsZBScanChannel6.Location = new Point(100, 38);
			this.lblHWSettingsZBScanChannel6.Name = "lblHWSettingsZBScanChannel6";
			this.lblHWSettingsZBScanChannel6.Size = new Size(23, 19);
			this.lblHWSettingsZBScanChannel6.TabIndex = 101;
			this.lblHWSettingsZBScanChannel6.Text = "11";
			this.lblHWSettingsZBScanChannelAll.Location = new Point(262, 38);
			this.lblHWSettingsZBScanChannelAll.Name = "lblHWSettingsZBScanChannelAll";
			this.lblHWSettingsZBScanChannelAll.Size = new Size(31, 19);
			this.lblHWSettingsZBScanChannelAll.TabIndex = 100;
			this.lblHWSettingsZBScanChannelAll.Text = "All";
			this.chkBxHWSettingsZBScanChannelAll.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannelAll.Location = new Point(265, 13);
			this.chkBxHWSettingsZBScanChannelAll.Name = "chkBxHWSettingsZBScanChannelAll";
			this.chkBxHWSettingsZBScanChannelAll.Size = new Size(17, 27);
			this.chkBxHWSettingsZBScanChannelAll.TabIndex = 99;
			this.chkBxHWSettingsZBScanChannelAll.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannelAll.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannelAll.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel1.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel1.Location = new Point(28, 13);
			this.chkBxHWSettingsZBScanChannel1.Name = "chkBxHWSettingsZBScanChannel1";
			this.chkBxHWSettingsZBScanChannel1.Size = new Size(15, 27);
			this.chkBxHWSettingsZBScanChannel1.TabIndex = 55;
			this.chkBxHWSettingsZBScanChannel1.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel1.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel1.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel2.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel2.Location = new Point(43, 13);
			this.chkBxHWSettingsZBScanChannel2.Name = "chkBxHWSettingsZBScanChannel2";
			this.chkBxHWSettingsZBScanChannel2.Size = new Size(15, 27);
			this.chkBxHWSettingsZBScanChannel2.TabIndex = 56;
			this.chkBxHWSettingsZBScanChannel2.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel2.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel2.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel3.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel3.Location = new Point(58, 13);
			this.chkBxHWSettingsZBScanChannel3.Name = "chkBxHWSettingsZBScanChannel3";
			this.chkBxHWSettingsZBScanChannel3.Size = new Size(17, 27);
			this.chkBxHWSettingsZBScanChannel3.TabIndex = 63;
			this.chkBxHWSettingsZBScanChannel3.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel3.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel3.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel4.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel4.Location = new Point(73, 13);
			this.chkBxHWSettingsZBScanChannel4.Name = "chkBxHWSettingsZBScanChannel4";
			this.chkBxHWSettingsZBScanChannel4.Size = new Size(15, 27);
			this.chkBxHWSettingsZBScanChannel4.TabIndex = 64;
			this.chkBxHWSettingsZBScanChannel4.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel4.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel4.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel5.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel5.Location = new Point(88, 13);
			this.chkBxHWSettingsZBScanChannel5.Name = "chkBxHWSettingsZBScanChannel5";
			this.chkBxHWSettingsZBScanChannel5.Size = new Size(17, 27);
			this.chkBxHWSettingsZBScanChannel5.TabIndex = 65;
			this.chkBxHWSettingsZBScanChannel5.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel5.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel5.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel6.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel6.Location = new Point(104, 13);
			this.chkBxHWSettingsZBScanChannel6.Name = "chkBxHWSettingsZBScanChannel6";
			this.chkBxHWSettingsZBScanChannel6.Size = new Size(15, 27);
			this.chkBxHWSettingsZBScanChannel6.TabIndex = 66;
			this.chkBxHWSettingsZBScanChannel6.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel6.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel6.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel7.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel7.Location = new Point(118, 13);
			this.chkBxHWSettingsZBScanChannel7.Name = "chkBxHWSettingsZBScanChannel7";
			this.chkBxHWSettingsZBScanChannel7.Size = new Size(17, 27);
			this.chkBxHWSettingsZBScanChannel7.TabIndex = 67;
			this.chkBxHWSettingsZBScanChannel7.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel7.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel7.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel8.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel8.Location = new Point(133, 13);
			this.chkBxHWSettingsZBScanChannel8.Name = "chkBxHWSettingsZBScanChannel8";
			this.chkBxHWSettingsZBScanChannel8.Size = new Size(15, 27);
			this.chkBxHWSettingsZBScanChannel8.TabIndex = 68;
			this.chkBxHWSettingsZBScanChannel8.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel8.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel8.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel9.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel9.Location = new Point(149, 13);
			this.chkBxHWSettingsZBScanChannel9.Name = "chkBxHWSettingsZBScanChannel9";
			this.chkBxHWSettingsZBScanChannel9.Size = new Size(15, 27);
			this.chkBxHWSettingsZBScanChannel9.TabIndex = 69;
			this.chkBxHWSettingsZBScanChannel9.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel9.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel9.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel10.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel10.Location = new Point(165, 13);
			this.chkBxHWSettingsZBScanChannel10.Name = "chkBxHWSettingsZBScanChannel10";
			this.chkBxHWSettingsZBScanChannel10.Size = new Size(15, 27);
			this.chkBxHWSettingsZBScanChannel10.TabIndex = 70;
			this.chkBxHWSettingsZBScanChannel10.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel10.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel10.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel11.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel11.Location = new Point(180, 13);
			this.chkBxHWSettingsZBScanChannel11.Name = "chkBxHWSettingsZBScanChannel11";
			this.chkBxHWSettingsZBScanChannel11.Size = new Size(17, 27);
			this.chkBxHWSettingsZBScanChannel11.TabIndex = 71;
			this.chkBxHWSettingsZBScanChannel11.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel11.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel11.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel12.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel12.Location = new Point(196, 13);
			this.chkBxHWSettingsZBScanChannel12.Name = "chkBxHWSettingsZBScanChannel12";
			this.chkBxHWSettingsZBScanChannel12.Size = new Size(15, 27);
			this.chkBxHWSettingsZBScanChannel12.TabIndex = 72;
			this.chkBxHWSettingsZBScanChannel12.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel12.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel12.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel13.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel13.Location = new Point(212, 13);
			this.chkBxHWSettingsZBScanChannel13.Name = "chkBxHWSettingsZBScanChannel13";
			this.chkBxHWSettingsZBScanChannel13.Size = new Size(17, 27);
			this.chkBxHWSettingsZBScanChannel13.TabIndex = 73;
			this.chkBxHWSettingsZBScanChannel13.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel13.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel13.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel14.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel14.Location = new Point(228, 13);
			this.chkBxHWSettingsZBScanChannel14.Name = "chkBxHWSettingsZBScanChannel14";
			this.chkBxHWSettingsZBScanChannel14.Size = new Size(15, 27);
			this.chkBxHWSettingsZBScanChannel14.TabIndex = 74;
			this.chkBxHWSettingsZBScanChannel14.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel14.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel14.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsZBScanChannel15.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel15.Location = new Point(244, 13);
			this.chkBxHWSettingsZBScanChannel15.Name = "chkBxHWSettingsZBScanChannel15";
			this.chkBxHWSettingsZBScanChannel15.Size = new Size(17, 27);
			this.chkBxHWSettingsZBScanChannel15.TabIndex = 75;
			this.chkBxHWSettingsZBScanChannel15.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel15.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel15.UseVisualStyleBackColor = true;
			this.lblHWSettingsZBScanChannel5.Location = new Point(84, 38);
			this.lblHWSettingsZBScanChannel5.Name = "lblHWSettingsZBScanChannel5";
			this.lblHWSettingsZBScanChannel5.Size = new Size(23, 19);
			this.lblHWSettingsZBScanChannel5.TabIndex = 82;
			this.lblHWSettingsZBScanChannel5.Text = "10";
			this.chkBxHWSettingsZBScanChannel0.Cursor = Cursors.Hand;
			this.chkBxHWSettingsZBScanChannel0.Location = new Point(13, 13);
			this.chkBxHWSettingsZBScanChannel0.Name = "chkBxHWSettingsZBScanChannel0";
			this.chkBxHWSettingsZBScanChannel0.Size = new Size(15, 27);
			this.chkBxHWSettingsZBScanChannel0.TabIndex = 76;
			this.chkBxHWSettingsZBScanChannel0.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsZBScanChannel0.TextImageRelation = TextImageRelation.ImageAboveText;
			this.chkBxHWSettingsZBScanChannel0.UseVisualStyleBackColor = true;
			this.lblHWSettingsZBScanChannel4.Location = new Point(73, 38);
			this.lblHWSettingsZBScanChannel4.Name = "lblHWSettingsZBScanChannel4";
			this.lblHWSettingsZBScanChannel4.Size = new Size(12, 19);
			this.lblHWSettingsZBScanChannel4.TabIndex = 81;
			this.lblHWSettingsZBScanChannel4.Text = "F";
			this.lblHWSettingsZBScanChannel0.Location = new Point(13, 38);
			this.lblHWSettingsZBScanChannel0.Name = "lblHWSettingsZBScanChannel0";
			this.lblHWSettingsZBScanChannel0.Size = new Size(12, 19);
			this.lblHWSettingsZBScanChannel0.TabIndex = 77;
			this.lblHWSettingsZBScanChannel0.Text = "B";
			this.lblHWSettingsZBScanChannel3.Location = new Point(58, 38);
			this.lblHWSettingsZBScanChannel3.Name = "lblHWSettingsZBScanChannel3";
			this.lblHWSettingsZBScanChannel3.Size = new Size(12, 19);
			this.lblHWSettingsZBScanChannel3.TabIndex = 80;
			this.lblHWSettingsZBScanChannel3.Text = "E";
			this.lblHWSettingsZBScanChannel1.Location = new Point(28, 38);
			this.lblHWSettingsZBScanChannel1.Name = "lblHWSettingsZBScanChannel1";
			this.lblHWSettingsZBScanChannel1.Size = new Size(12, 19);
			this.lblHWSettingsZBScanChannel1.TabIndex = 78;
			this.lblHWSettingsZBScanChannel1.Text = "C";
			this.lblHWSettingsZBScanChannel2.Location = new Point(42, 38);
			this.lblHWSettingsZBScanChannel2.Name = "lblHWSettingsZBScanChannel2";
			this.lblHWSettingsZBScanChannel2.Size = new Size(12, 19);
			this.lblHWSettingsZBScanChannel2.TabIndex = 79;
			this.lblHWSettingsZBScanChannel2.Text = "D";
			this.pbHWSettingsZB.BackgroundImage = (Image)componentResourceManager.GetObject("pbHWSettingsZB.BackgroundImage");
			this.pbHWSettingsZB.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbHWSettingsZB.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbHWSettingsZB.ImageListBack");
			this.pbHWSettingsZB.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbHWSettingsZB.ImageListFore");
			this.pbHWSettingsZB.Location = new Point(334, 2);
			this.pbHWSettingsZB.Name = "pbHWSettingsZB";
			this.pbHWSettingsZB.SelectedImageBack = -1;
			this.pbHWSettingsZB.SelectedImageFore = -1;
			this.pbHWSettingsZB.Size = new Size(65, 61);
			this.pbHWSettingsZB.TabIndex = 98;
			this.pbHWSettingsZB.TabStop = false;
			this.lblHWSettingsZBConfig.Location = new Point(6, 40);
			this.lblHWSettingsZBConfig.Name = "lblHWSettingsZBConfig";
			this.lblHWSettingsZBConfig.Size = new Size(101, 22);
			this.lblHWSettingsZBConfig.TabIndex = 95;
			this.lblHWSettingsZBConfig.Tag = "";
			this.lblHWSettingsZBConfig.Text = "Configuration:";
			this.lblHWSettingsZBConfig.TextAlign = ContentAlignment.MiddleRight;
			this.txtHWSettingsZBPanID.BackColor = Color.White;
			this.txtHWSettingsZBPanID.CharacterCasing = CharacterCasing.Upper;
			this.txtHWSettingsZBPanID.Location = new Point(114, 72);
			this.txtHWSettingsZBPanID.MaxLength = 8;
			this.txtHWSettingsZBPanID.Name = "txtHWSettingsZBPanID";
			this.txtHWSettingsZBPanID.Size = new Size(271, 20);
			this.txtHWSettingsZBPanID.TabIndex = 52;
			this.txtHWSettingsZBPanID.Tag = "";
			this.txtHWSettingsZBPanID.TextAlign = HorizontalAlignment.Center;
			this.txtHWSettingsZBPanID.TextChanged += new EventHandler(this.txtHWSettingsZBPanID_TextChanged);
			this.txtHWSettingsZBPanID.KeyDown += new KeyEventHandler(this.txtHWSettingsZBPanID_KeyDown);
			this.lblHWSettingsZBPanID.Location = new Point(7, 71);
			this.lblHWSettingsZBPanID.Name = "lblHWSettingsZBPanID";
			this.lblHWSettingsZBPanID.Size = new Size(101, 22);
			this.lblHWSettingsZBPanID.TabIndex = 51;
			this.lblHWSettingsZBPanID.Tag = "";
			this.lblHWSettingsZBPanID.Text = "PAN ID:";
			this.lblHWSettingsZBPanID.TextAlign = ContentAlignment.MiddleRight;
			this.btnSettingsApply.BackColor = Color.Silver;
			this.btnSettingsApply.Location = new Point(220, 204);
			this.btnSettingsApply.Name = "btnSettingsApply";
			this.btnSettingsApply.Size = new Size(88, 29);
			this.btnSettingsApply.TabIndex = 9;
			this.btnSettingsApply.Text = "Apply";
			this.btnSettingsApply.UseVisualStyleBackColor = true;
			this.btnSettingsApply.Click += new EventHandler(this.btnSettingsApply_Click);
			this.btnSettingsCancel.BackColor = Color.Silver;
			this.btnSettingsCancel.Location = new Point(313, 204);
			this.btnSettingsCancel.Name = "btnSettingsCancel";
			this.btnSettingsCancel.Size = new Size(88, 29);
			this.btnSettingsCancel.TabIndex = 10;
			this.btnSettingsCancel.Text = "Cancel";
			this.btnSettingsCancel.UseVisualStyleBackColor = true;
			this.btnSettingsCancel.Click += new EventHandler(this.btnSettingsCancel_Click);
			this.pnlHWSettingsLAN.BackColor = Color.Transparent;
			this.pnlHWSettingsLAN.Controls.Add(this.lblHWSettingsLANDNSDetected);
			this.pnlHWSettingsLAN.Controls.Add(this.lblHWSettingsLANGatewayDetected);
			this.pnlHWSettingsLAN.Controls.Add(this.lblHWSettingsLANSubnetMaskDetected);
			this.pnlHWSettingsLAN.Controls.Add(this.lblHWSettingsLANCurrentIPDetected);
			this.pnlHWSettingsLAN.Controls.Add(this.chkBxHWSettingsLANDHCP);
			this.pnlHWSettingsLAN.Controls.Add(this.lblHWSettingsLANHeaderDetected);
			this.pnlHWSettingsLAN.Controls.Add(this.lblHWSettingsLANHeaderCurrent);
			this.pnlHWSettingsLAN.Controls.Add(this.txtHWSettingsLANDNS);
			this.pnlHWSettingsLAN.Controls.Add(this.lblHWSettingsLANDNS);
			this.pnlHWSettingsLAN.Controls.Add(this.txtHWSettingsLANGateway);
			this.pnlHWSettingsLAN.Controls.Add(this.lblHWSettingsLANGateway);
			this.pnlHWSettingsLAN.Controls.Add(this.txtHWSettingsLANSubnetMask);
			this.pnlHWSettingsLAN.Controls.Add(this.lblHWSettingsLANSubnetMask);
			this.pnlHWSettingsLAN.Controls.Add(this.pbHWSettingsLAN);
			this.pnlHWSettingsLAN.Controls.Add(this.lblHWSettingsLANHeader);
			this.pnlHWSettingsLAN.Controls.Add(this.txtHWSettingsLANCurrentIP);
			this.pnlHWSettingsLAN.Controls.Add(this.lbHWlSettingsLANCurrentIP);
			this.pnlHWSettingsLAN.Location = new Point(393, 47);
			this.pnlHWSettingsLAN.Name = "pnlHWSettingsLAN";
			this.pnlHWSettingsLAN.Size = new Size(402, 191);
			this.pnlHWSettingsLAN.TabIndex = 11;
			this.lblHWSettingsLANDNSDetected.BackColor = Color.White;
			this.lblHWSettingsLANDNSDetected.BorderStyle = BorderStyle.FixedSingle;
			this.lblHWSettingsLANDNSDetected.Location = new Point(204, 131);
			this.lblHWSettingsLANDNSDetected.Name = "lblHWSettingsLANDNSDetected";
			this.lblHWSettingsLANDNSDetected.Size = new Size(108, 21);
			this.lblHWSettingsLANDNSDetected.TabIndex = 115;
			this.lblHWSettingsLANDNSDetected.Text = "999.999.999.999";
			this.lblHWSettingsLANDNSDetected.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsLANGatewayDetected.BackColor = Color.White;
			this.lblHWSettingsLANGatewayDetected.BorderStyle = BorderStyle.FixedSingle;
			this.lblHWSettingsLANGatewayDetected.Location = new Point(204, 105);
			this.lblHWSettingsLANGatewayDetected.Name = "lblHWSettingsLANGatewayDetected";
			this.lblHWSettingsLANGatewayDetected.Size = new Size(108, 21);
			this.lblHWSettingsLANGatewayDetected.TabIndex = 114;
			this.lblHWSettingsLANGatewayDetected.Text = "999.999.999.999";
			this.lblHWSettingsLANGatewayDetected.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsLANSubnetMaskDetected.BackColor = Color.White;
			this.lblHWSettingsLANSubnetMaskDetected.BorderStyle = BorderStyle.FixedSingle;
			this.lblHWSettingsLANSubnetMaskDetected.Location = new Point(204, 80);
			this.lblHWSettingsLANSubnetMaskDetected.Name = "lblHWSettingsLANSubnetMaskDetected";
			this.lblHWSettingsLANSubnetMaskDetected.Size = new Size(108, 21);
			this.lblHWSettingsLANSubnetMaskDetected.TabIndex = 113;
			this.lblHWSettingsLANSubnetMaskDetected.Text = "999.999.999.999";
			this.lblHWSettingsLANSubnetMaskDetected.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsLANCurrentIPDetected.BackColor = Color.White;
			this.lblHWSettingsLANCurrentIPDetected.BorderStyle = BorderStyle.FixedSingle;
			this.lblHWSettingsLANCurrentIPDetected.Location = new Point(204, 54);
			this.lblHWSettingsLANCurrentIPDetected.Name = "lblHWSettingsLANCurrentIPDetected";
			this.lblHWSettingsLANCurrentIPDetected.Size = new Size(108, 21);
			this.lblHWSettingsLANCurrentIPDetected.TabIndex = 112;
			this.lblHWSettingsLANCurrentIPDetected.Text = "999.999.999.999";
			this.lblHWSettingsLANCurrentIPDetected.TextAlign = ContentAlignment.MiddleCenter;
			this.chkBxHWSettingsLANDHCP.Location = new Point(153, 159);
			this.chkBxHWSettingsLANDHCP.Name = "chkBxHWSettingsLANDHCP";
			this.chkBxHWSettingsLANDHCP.Size = new Size(90, 22);
			this.chkBxHWSettingsLANDHCP.TabIndex = 111;
			this.chkBxHWSettingsLANDHCP.Text = "Use DHCP";
			this.chkBxHWSettingsLANDHCP.UseVisualStyleBackColor = true;
			this.chkBxHWSettingsLANDHCP.CheckedChanged += new EventHandler(this.chkBxHWSettingsLANDHCP_CheckedChanged);
			this.lblHWSettingsLANHeaderDetected.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Underline, GraphicsUnit.Point, 0);
			this.lblHWSettingsLANHeaderDetected.Location = new Point(210, 31);
			this.lblHWSettingsLANHeaderDetected.Name = "lblHWSettingsLANHeaderDetected";
			this.lblHWSettingsLANHeaderDetected.Size = new Size(94, 22);
			this.lblHWSettingsLANHeaderDetected.TabIndex = 110;
			this.lblHWSettingsLANHeaderDetected.Tag = "";
			this.lblHWSettingsLANHeaderDetected.Text = "Detected:";
			this.lblHWSettingsLANHeaderDetected.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsLANHeaderCurrent.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Underline, GraphicsUnit.Point, 0);
			this.lblHWSettingsLANHeaderCurrent.Location = new Point(103, 31);
			this.lblHWSettingsLANHeaderCurrent.Name = "lblHWSettingsLANHeaderCurrent";
			this.lblHWSettingsLANHeaderCurrent.Size = new Size(94, 22);
			this.lblHWSettingsLANHeaderCurrent.TabIndex = 109;
			this.lblHWSettingsLANHeaderCurrent.Tag = "";
			this.lblHWSettingsLANHeaderCurrent.Text = "Settings:";
			this.lblHWSettingsLANHeaderCurrent.TextAlign = ContentAlignment.MiddleCenter;
			this.txtHWSettingsLANDNS.BackColor = Color.White;
			this.txtHWSettingsLANDNS.Location = new Point(93, 131);
			this.txtHWSettingsLANDNS.Name = "txtHWSettingsLANDNS";
			this.txtHWSettingsLANDNS.Size = new Size(105, 20);
			this.txtHWSettingsLANDNS.TabIndex = 107;
			this.txtHWSettingsLANDNS.Tag = "";
			this.txtHWSettingsLANDNS.Text = "999.999.999.999";
			this.txtHWSettingsLANDNS.TextAlign = HorizontalAlignment.Center;
			this.txtHWSettingsLANDNS.TextChanged += new EventHandler(this.txtHWSettingsLANDNS_TextChanged);
			this.lblHWSettingsLANDNS.Location = new Point(0, 130);
			this.lblHWSettingsLANDNS.Name = "lblHWSettingsLANDNS";
			this.lblHWSettingsLANDNS.Size = new Size(94, 22);
			this.lblHWSettingsLANDNS.TabIndex = 105;
			this.lblHWSettingsLANDNS.Tag = "";
			this.lblHWSettingsLANDNS.Text = "DNS IP:";
			this.lblHWSettingsLANDNS.TextAlign = ContentAlignment.MiddleRight;
			this.txtHWSettingsLANGateway.BackColor = Color.White;
			this.txtHWSettingsLANGateway.Location = new Point(93, 105);
			this.txtHWSettingsLANGateway.Name = "txtHWSettingsLANGateway";
			this.txtHWSettingsLANGateway.Size = new Size(105, 20);
			this.txtHWSettingsLANGateway.TabIndex = 104;
			this.txtHWSettingsLANGateway.Tag = "";
			this.txtHWSettingsLANGateway.Text = "999.999.999.999";
			this.txtHWSettingsLANGateway.TextAlign = HorizontalAlignment.Center;
			this.txtHWSettingsLANGateway.TextChanged += new EventHandler(this.txtHWSettingsLANGateway_TextChanged);
			this.lblHWSettingsLANGateway.Location = new Point(1, 103);
			this.lblHWSettingsLANGateway.Name = "lblHWSettingsLANGateway";
			this.lblHWSettingsLANGateway.Size = new Size(94, 22);
			this.lblHWSettingsLANGateway.TabIndex = 103;
			this.lblHWSettingsLANGateway.Tag = "";
			this.lblHWSettingsLANGateway.Text = "Default Gateway:";
			this.lblHWSettingsLANGateway.TextAlign = ContentAlignment.MiddleRight;
			this.txtHWSettingsLANSubnetMask.BackColor = Color.White;
			this.txtHWSettingsLANSubnetMask.Location = new Point(94, 80);
			this.txtHWSettingsLANSubnetMask.Name = "txtHWSettingsLANSubnetMask";
			this.txtHWSettingsLANSubnetMask.Size = new Size(105, 20);
			this.txtHWSettingsLANSubnetMask.TabIndex = 101;
			this.txtHWSettingsLANSubnetMask.Tag = "";
			this.txtHWSettingsLANSubnetMask.Text = "999.999.999.999";
			this.txtHWSettingsLANSubnetMask.TextAlign = HorizontalAlignment.Center;
			this.txtHWSettingsLANSubnetMask.TextChanged += new EventHandler(this.txtHWSettingsLANSubnetMask_TextChanged);
			this.lblHWSettingsLANSubnetMask.Location = new Point(1, 79);
			this.lblHWSettingsLANSubnetMask.Name = "lblHWSettingsLANSubnetMask";
			this.lblHWSettingsLANSubnetMask.Size = new Size(94, 22);
			this.lblHWSettingsLANSubnetMask.TabIndex = 98;
			this.lblHWSettingsLANSubnetMask.Tag = "";
			this.lblHWSettingsLANSubnetMask.Text = "Subnet Mask:";
			this.lblHWSettingsLANSubnetMask.TextAlign = ContentAlignment.MiddleRight;
			this.pbHWSettingsLAN.BackgroundImage = Resources.LAN2;
			this.pbHWSettingsLAN.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbHWSettingsLAN.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbHWSettingsLAN.ImageListBack");
			this.pbHWSettingsLAN.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbHWSettingsLAN.ImageListFore");
			this.pbHWSettingsLAN.Location = new Point(335, 0);
			this.pbHWSettingsLAN.Name = "pbHWSettingsLAN";
			this.pbHWSettingsLAN.SelectedImageBack = -1;
			this.pbHWSettingsLAN.SelectedImageFore = -1;
			this.pbHWSettingsLAN.Size = new Size(65, 61);
			this.pbHWSettingsLAN.TabIndex = 97;
			this.pbHWSettingsLAN.TabStop = false;
			this.lblHWSettingsLANHeader.BackColor = Color.Transparent;
			this.lblHWSettingsLANHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsLANHeader.Location = new Point(13, 2);
			this.lblHWSettingsLANHeader.Name = "lblHWSettingsLANHeader";
			this.lblHWSettingsLANHeader.Size = new Size(373, 30);
			this.lblHWSettingsLANHeader.TabIndex = 7;
			this.lblHWSettingsLANHeader.Text = "LAN Settings";
			this.lblHWSettingsLANHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsLANHeader.MouseDown += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseDown);
			this.lblHWSettingsLANHeader.MouseMove += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseMove);
			this.lblHWSettingsLANHeader.MouseUp += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseUp);
			this.txtHWSettingsLANCurrentIP.BackColor = Color.White;
			this.txtHWSettingsLANCurrentIP.Location = new Point(94, 54);
			this.txtHWSettingsLANCurrentIP.Name = "txtHWSettingsLANCurrentIP";
			this.txtHWSettingsLANCurrentIP.Size = new Size(105, 20);
			this.txtHWSettingsLANCurrentIP.TabIndex = 52;
			this.txtHWSettingsLANCurrentIP.Tag = "";
			this.txtHWSettingsLANCurrentIP.Text = "999.999.999.999";
			this.txtHWSettingsLANCurrentIP.TextAlign = HorizontalAlignment.Center;
			this.txtHWSettingsLANCurrentIP.TextChanged += new EventHandler(this.txtHWSettingsLANCurrentIP_TextChanged);
			this.lbHWlSettingsLANCurrentIP.Location = new Point(2, 52);
			this.lbHWlSettingsLANCurrentIP.Name = "lbHWlSettingsLANCurrentIP";
			this.lbHWlSettingsLANCurrentIP.Size = new Size(94, 22);
			this.lbHWlSettingsLANCurrentIP.TabIndex = 51;
			this.lbHWlSettingsLANCurrentIP.Tag = "";
			this.lbHWlSettingsLANCurrentIP.Text = "Current IP:";
			this.lbHWlSettingsLANCurrentIP.TextAlign = ContentAlignment.MiddleRight;
			this.pnlHWSettingsPingTest.BackColor = Color.Transparent;
			this.pnlHWSettingsPingTest.Controls.Add(this.lblHWSettingsPingTestAddress);
			this.pnlHWSettingsPingTest.Controls.Add(this.btnHWSettingsPingTestClear);
			this.pnlHWSettingsPingTest.Controls.Add(this.txtHWSettingsPingResult);
			this.pnlHWSettingsPingTest.Controls.Add(this.btnHWSettingsPingTestPing);
			this.pnlHWSettingsPingTest.Controls.Add(this.txtHWSettingsPingTestURL);
			this.pnlHWSettingsPingTest.Controls.Add(this.pbHWSettingsPingTest);
			this.pnlHWSettingsPingTest.Controls.Add(this.lblHWSettingsLANPingTestHeader);
			this.pnlHWSettingsPingTest.Location = new Point(381, 88);
			this.pnlHWSettingsPingTest.Name = "pnlHWSettingsPingTest";
			this.pnlHWSettingsPingTest.Size = new Size(402, 191);
			this.pnlHWSettingsPingTest.TabIndex = 12;
			this.lblHWSettingsPingTestAddress.BackColor = Color.Transparent;
			this.lblHWSettingsPingTestAddress.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsPingTestAddress.Location = new Point(9, 43);
			this.lblHWSettingsPingTestAddress.Name = "lblHWSettingsPingTestAddress";
			this.lblHWSettingsPingTestAddress.Size = new Size(70, 16);
			this.lblHWSettingsPingTestAddress.TabIndex = 117;
			this.lblHWSettingsPingTestAddress.Text = "Address:";
			this.lblHWSettingsPingTestAddress.TextAlign = ContentAlignment.MiddleRight;
			this.btnHWSettingsPingTestClear.BackColor = Color.Silver;
			this.btnHWSettingsPingTestClear.ForeColor = Color.Black;
			this.btnHWSettingsPingTestClear.Location = new Point(337, 93);
			this.btnHWSettingsPingTestClear.Name = "btnHWSettingsPingTestClear";
			this.btnHWSettingsPingTestClear.Size = new Size(62, 26);
			this.btnHWSettingsPingTestClear.TabIndex = 115;
			this.btnHWSettingsPingTestClear.Tag = "";
			this.btnHWSettingsPingTestClear.Text = "Clear";
			this.btnHWSettingsPingTestClear.UseVisualStyleBackColor = true;
			this.btnHWSettingsPingTestClear.Click += new EventHandler(this.btnHWSettingsPingTestClear_Click);
			this.txtHWSettingsPingResult.BackColor = Color.White;
			this.txtHWSettingsPingResult.Enabled = false;
			this.txtHWSettingsPingResult.ForeColor = Color.Black;
			this.txtHWSettingsPingResult.Location = new Point(5, 66);
			this.txtHWSettingsPingResult.Multiline = true;
			this.txtHWSettingsPingResult.Name = "txtHWSettingsPingResult";
			this.txtHWSettingsPingResult.ScrollBars = ScrollBars.Vertical;
			this.txtHWSettingsPingResult.Size = new Size(324, 118);
			this.txtHWSettingsPingResult.TabIndex = 116;
			this.txtHWSettingsPingResult.EnabledChanged += new EventHandler(this.txtHWSettingsPingResult_EnabledChanged);
			this.txtHWSettingsPingResult.TextChanged += new EventHandler(this.txtHWSettingsPingResult_TextChanged);
			this.btnHWSettingsPingTestPing.BackColor = Color.Silver;
			this.btnHWSettingsPingTestPing.ForeColor = Color.Black;
			this.btnHWSettingsPingTestPing.Location = new Point(337, 66);
			this.btnHWSettingsPingTestPing.Name = "btnHWSettingsPingTestPing";
			this.btnHWSettingsPingTestPing.Size = new Size(62, 26);
			this.btnHWSettingsPingTestPing.TabIndex = 114;
			this.btnHWSettingsPingTestPing.Tag = "";
			this.btnHWSettingsPingTestPing.Text = "Ping";
			this.btnHWSettingsPingTestPing.UseVisualStyleBackColor = true;
			this.btnHWSettingsPingTestPing.Click += new EventHandler(this.btnHWSettingsPingTestPing_Click);
			this.txtHWSettingsPingTestURL.BackColor = Color.White;
			this.txtHWSettingsPingTestURL.Location = new Point(85, 41);
			this.txtHWSettingsPingTestURL.Name = "txtHWSettingsPingTestURL";
			this.txtHWSettingsPingTestURL.Size = new Size(244, 20);
			this.txtHWSettingsPingTestURL.TabIndex = 113;
			this.txtHWSettingsPingTestURL.Tag = "";
			this.txtHWSettingsPingTestURL.TextAlign = HorizontalAlignment.Center;
			this.txtHWSettingsPingTestURL.TextChanged += new EventHandler(this.txtHWSettingsPingTestURL_TextChanged);
			this.pbHWSettingsPingTest.BackgroundImage = Resources.Server;
			this.pbHWSettingsPingTest.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbHWSettingsPingTest.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbHWSettingsPingTest.ImageListBack");
			this.pbHWSettingsPingTest.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbHWSettingsPingTest.ImageListFore");
			this.pbHWSettingsPingTest.Location = new Point(335, 0);
			this.pbHWSettingsPingTest.Name = "pbHWSettingsPingTest";
			this.pbHWSettingsPingTest.SelectedImageBack = -1;
			this.pbHWSettingsPingTest.SelectedImageFore = -1;
			this.pbHWSettingsPingTest.Size = new Size(65, 61);
			this.pbHWSettingsPingTest.TabIndex = 97;
			this.pbHWSettingsPingTest.TabStop = false;
			this.lblHWSettingsLANPingTestHeader.BackColor = Color.Transparent;
			this.lblHWSettingsLANPingTestHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsLANPingTestHeader.Location = new Point(5, 2);
			this.lblHWSettingsLANPingTestHeader.Name = "lblHWSettingsLANPingTestHeader";
			this.lblHWSettingsLANPingTestHeader.Size = new Size(397, 24);
			this.lblHWSettingsLANPingTestHeader.TabIndex = 7;
			this.lblHWSettingsLANPingTestHeader.Text = "Ping Test";
			this.lblHWSettingsLANPingTestHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsLANPingTestHeader.MouseDown += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseDown);
			this.lblHWSettingsLANPingTestHeader.MouseMove += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseMove);
			this.lblHWSettingsLANPingTestHeader.MouseUp += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseUp);
			this.pnlHWSettingsRS232.BackColor = Color.Transparent;
			this.pnlHWSettingsRS232.Controls.Add(this.lblHWSettingsStringSizeProblem);
			this.pnlHWSettingsRS232.Controls.Add(this.btnHWSettingsRS232ShowATCommands);
			this.pnlHWSettingsRS232.Controls.Add(this.cmbHWSettingsRS232GSMModemType);
			this.pnlHWSettingsRS232.Controls.Add(this.lblHWSettingsRS232GSMModemType);
			this.pnlHWSettingsRS232.Controls.Add(this.pbHWSettingsRS232GSM);
			this.pnlHWSettingsRS232.Controls.Add(this.txtHWSettingsRS232GSMPassword);
			this.pnlHWSettingsRS232.Controls.Add(this.lblHWSettingsRS232GSMPassword);
			this.pnlHWSettingsRS232.Controls.Add(this.txtHWSettingsRS232GSMUsername);
			this.pnlHWSettingsRS232.Controls.Add(this.lblHWSettingsRS232GSMUsername);
			this.pnlHWSettingsRS232.Controls.Add(this.txtHWSettingsRS232GSMAPN);
			this.pnlHWSettingsRS232.Controls.Add(this.lblHWSettingsRS232GSMAPN);
			this.pnlHWSettingsRS232.Controls.Add(this.pbHWSettingsRS232);
			this.pnlHWSettingsRS232.Controls.Add(this.cmbHWSettingsRS232CommMode);
			this.pnlHWSettingsRS232.Controls.Add(this.lblHWSettingsRS232CommMode);
			this.pnlHWSettingsRS232.Controls.Add(this.lblHWSettingsRS232Header);
			this.pnlHWSettingsRS232.Location = new Point(402, 17);
			this.pnlHWSettingsRS232.Name = "pnlHWSettingsRS232";
			this.pnlHWSettingsRS232.Size = new Size(402, 191);
			this.pnlHWSettingsRS232.TabIndex = 13;
			this.lblHWSettingsStringSizeProblem.Location = new Point(18, 167);
			this.lblHWSettingsStringSizeProblem.Name = "lblHWSettingsStringSizeProblem";
			this.lblHWSettingsStringSizeProblem.Size = new Size(313, 17);
			this.lblHWSettingsStringSizeProblem.TabIndex = 111;
			this.lblHWSettingsStringSizeProblem.Visible = false;
			this.btnHWSettingsRS232ShowATCommands.BackColor = SystemColors.Control;
			this.btnHWSettingsRS232ShowATCommands.Location = new Point(144, 117);
			this.btnHWSettingsRS232ShowATCommands.Name = "btnHWSettingsRS232ShowATCommands";
			this.btnHWSettingsRS232ShowATCommands.Size = new Size(116, 29);
			this.btnHWSettingsRS232ShowATCommands.TabIndex = 110;
			this.btnHWSettingsRS232ShowATCommands.Text = "AT Commands";
			this.btnHWSettingsRS232ShowATCommands.UseVisualStyleBackColor = true;
			this.btnHWSettingsRS232ShowATCommands.Click += new EventHandler(this.btnHWSettingsRS232ShowATCommands_Click);
			this.cmbHWSettingsRS232GSMModemType.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbHWSettingsRS232GSMModemType.FormattingEnabled = true;
			this.cmbHWSettingsRS232GSMModemType.Location = new Point(116, 76);
			this.cmbHWSettingsRS232GSMModemType.Name = "cmbHWSettingsRS232GSMModemType";
			this.cmbHWSettingsRS232GSMModemType.Size = new Size(215, 21);
			this.cmbHWSettingsRS232GSMModemType.TabIndex = 109;
			this.cmbHWSettingsRS232GSMModemType.SelectedIndexChanged += new EventHandler(this.cmbHWSettingsRS232GSMModemType_SelectedIndexChanged);
			this.lblHWSettingsRS232GSMModemType.Location = new Point(12, 76);
			this.lblHWSettingsRS232GSMModemType.Name = "lblHWSettingsRS232GSMModemType";
			this.lblHWSettingsRS232GSMModemType.Size = new Size(98, 22);
			this.lblHWSettingsRS232GSMModemType.TabIndex = 108;
			this.lblHWSettingsRS232GSMModemType.Tag = "";
			this.lblHWSettingsRS232GSMModemType.Text = "Modem Type:";
			this.lblHWSettingsRS232GSMModemType.TextAlign = ContentAlignment.MiddleCenter;
			this.pbHWSettingsRS232GSM.BackgroundImage = (Image)componentResourceManager.GetObject("pbHWSettingsRS232GSM.BackgroundImage");
			this.pbHWSettingsRS232GSM.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbHWSettingsRS232GSM.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbHWSettingsRS232GSM.ImageListBack");
			this.pbHWSettingsRS232GSM.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbHWSettingsRS232GSM.ImageListFore");
			this.pbHWSettingsRS232GSM.Location = new Point(345, 71);
			this.pbHWSettingsRS232GSM.Name = "pbHWSettingsRS232GSM";
			this.pbHWSettingsRS232GSM.SelectedImageBack = -1;
			this.pbHWSettingsRS232GSM.SelectedImageFore = -1;
			this.pbHWSettingsRS232GSM.Size = new Size(39, 114);
			this.pbHWSettingsRS232GSM.TabIndex = 105;
			this.pbHWSettingsRS232GSM.TabStop = false;
			this.txtHWSettingsRS232GSMPassword.BackColor = Color.White;
			this.txtHWSettingsRS232GSMPassword.Location = new Point(251, 141);
			this.txtHWSettingsRS232GSMPassword.Name = "txtHWSettingsRS232GSMPassword";
			this.txtHWSettingsRS232GSMPassword.Size = new Size(80, 20);
			this.txtHWSettingsRS232GSMPassword.TabIndex = 104;
			this.txtHWSettingsRS232GSMPassword.Tag = "";
			this.txtHWSettingsRS232GSMPassword.TextAlign = HorizontalAlignment.Center;
			this.txtHWSettingsRS232GSMPassword.TextChanged += new EventHandler(this.txtHWSettingsRS232GSMPassword_TextChanged);
			this.lblHWSettingsRS232GSMPassword.Location = new Point(191, 139);
			this.lblHWSettingsRS232GSMPassword.Name = "lblHWSettingsRS232GSMPassword";
			this.lblHWSettingsRS232GSMPassword.Size = new Size(65, 22);
			this.lblHWSettingsRS232GSMPassword.TabIndex = 103;
			this.lblHWSettingsRS232GSMPassword.Tag = "";
			this.lblHWSettingsRS232GSMPassword.Text = "Password:";
			this.lblHWSettingsRS232GSMPassword.TextAlign = ContentAlignment.MiddleCenter;
			this.txtHWSettingsRS232GSMUsername.BackColor = Color.White;
			this.txtHWSettingsRS232GSMUsername.Location = new Point(74, 141);
			this.txtHWSettingsRS232GSMUsername.Name = "txtHWSettingsRS232GSMUsername";
			this.txtHWSettingsRS232GSMUsername.Size = new Size(109, 20);
			this.txtHWSettingsRS232GSMUsername.TabIndex = 102;
			this.txtHWSettingsRS232GSMUsername.Tag = "";
			this.txtHWSettingsRS232GSMUsername.TextAlign = HorizontalAlignment.Center;
			this.txtHWSettingsRS232GSMUsername.TextChanged += new EventHandler(this.txtHWSettingsRS232GSMUsername_TextChanged);
			this.lblHWSettingsRS232GSMUsername.Location = new Point(9, 140);
			this.lblHWSettingsRS232GSMUsername.Name = "lblHWSettingsRS232GSMUsername";
			this.lblHWSettingsRS232GSMUsername.Size = new Size(73, 22);
			this.lblHWSettingsRS232GSMUsername.TabIndex = 101;
			this.lblHWSettingsRS232GSMUsername.Tag = "";
			this.lblHWSettingsRS232GSMUsername.Text = "Username:";
			this.lblHWSettingsRS232GSMUsername.TextAlign = ContentAlignment.MiddleCenter;
			this.txtHWSettingsRS232GSMAPN.BackColor = Color.White;
			this.txtHWSettingsRS232GSMAPN.Location = new Point(116, 111);
			this.txtHWSettingsRS232GSMAPN.MaxLength = 18;
			this.txtHWSettingsRS232GSMAPN.Name = "txtHWSettingsRS232GSMAPN";
			this.txtHWSettingsRS232GSMAPN.Size = new Size(215, 20);
			this.txtHWSettingsRS232GSMAPN.TabIndex = 100;
			this.txtHWSettingsRS232GSMAPN.Tag = "";
			this.txtHWSettingsRS232GSMAPN.TextAlign = HorizontalAlignment.Center;
			this.txtHWSettingsRS232GSMAPN.TextChanged += new EventHandler(this.txtHWSettingsRS232GSMAPN_TextChanged);
			this.lblHWSettingsRS232GSMAPN.Location = new Point(37, 108);
			this.lblHWSettingsRS232GSMAPN.Name = "lblHWSettingsRS232GSMAPN";
			this.lblHWSettingsRS232GSMAPN.Size = new Size(73, 22);
			this.lblHWSettingsRS232GSMAPN.TabIndex = 99;
			this.lblHWSettingsRS232GSMAPN.Tag = "";
			this.lblHWSettingsRS232GSMAPN.Text = "APN:";
			this.lblHWSettingsRS232GSMAPN.TextAlign = ContentAlignment.MiddleCenter;
			this.pbHWSettingsRS232.BackgroundImage = (Image)componentResourceManager.GetObject("pbHWSettingsRS232.BackgroundImage");
			this.pbHWSettingsRS232.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbHWSettingsRS232.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbHWSettingsRS232.ImageListBack");
			this.pbHWSettingsRS232.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbHWSettingsRS232.ImageListFore");
			this.pbHWSettingsRS232.Location = new Point(334, 2);
			this.pbHWSettingsRS232.Name = "pbHWSettingsRS232";
			this.pbHWSettingsRS232.SelectedImageBack = -1;
			this.pbHWSettingsRS232.SelectedImageFore = -1;
			this.pbHWSettingsRS232.Size = new Size(65, 61);
			this.pbHWSettingsRS232.TabIndex = 98;
			this.pbHWSettingsRS232.TabStop = false;
			this.cmbHWSettingsRS232CommMode.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbHWSettingsRS232CommMode.FormattingEnabled = true;
			this.cmbHWSettingsRS232CommMode.Location = new Point(115, 39);
			this.cmbHWSettingsRS232CommMode.Name = "cmbHWSettingsRS232CommMode";
			this.cmbHWSettingsRS232CommMode.Size = new Size(215, 21);
			this.cmbHWSettingsRS232CommMode.TabIndex = 94;
			this.cmbHWSettingsRS232CommMode.SelectedIndexChanged += new EventHandler(this.cmbHWSettingsRS232CommMode_SelectedIndexChanged);
			this.lblHWSettingsRS232CommMode.Location = new Point(10, 38);
			this.lblHWSettingsRS232CommMode.Name = "lblHWSettingsRS232CommMode";
			this.lblHWSettingsRS232CommMode.Size = new Size(101, 22);
			this.lblHWSettingsRS232CommMode.TabIndex = 93;
			this.lblHWSettingsRS232CommMode.Tag = "";
			this.lblHWSettingsRS232CommMode.Text = "Comm Mode:";
			this.lblHWSettingsRS232CommMode.TextAlign = ContentAlignment.MiddleRight;
			this.lblHWSettingsRS232Header.BackColor = Color.Transparent;
			this.lblHWSettingsRS232Header.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsRS232Header.Location = new Point(15, 2);
			this.lblHWSettingsRS232Header.Name = "lblHWSettingsRS232Header";
			this.lblHWSettingsRS232Header.Size = new Size(369, 26);
			this.lblHWSettingsRS232Header.TabIndex = 7;
			this.lblHWSettingsRS232Header.Text = "RS 232 Settings";
			this.lblHWSettingsRS232Header.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsRS232Header.MouseDown += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseDown);
			this.lblHWSettingsRS232Header.MouseMove += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseMove);
			this.lblHWSettingsRS232Header.MouseUp += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseUp);
			this.pnlHWSettingsServer.BackColor = Color.Transparent;
			this.pnlHWSettingsServer.Controls.Add(this.txtHWSettingsServerPort);
			this.pnlHWSettingsServer.Controls.Add(this.lblHWSettingsServerPort);
			this.pnlHWSettingsServer.Controls.Add(this.txtHWSettingsServerAddress);
			this.pnlHWSettingsServer.Controls.Add(this.lblHWSettingsServerAddress);
			this.pnlHWSettingsServer.Controls.Add(this.pbHWSettingsServer);
			this.pnlHWSettingsServer.Controls.Add(this.cmbHWSettingsServerVia);
			this.pnlHWSettingsServer.Controls.Add(this.lblHWSettingsServerVia);
			this.pnlHWSettingsServer.Controls.Add(this.lblHWSettingsServerHeader);
			this.pnlHWSettingsServer.Location = new Point(354, 166);
			this.pnlHWSettingsServer.Name = "pnlHWSettingsServer";
			this.pnlHWSettingsServer.Size = new Size(402, 191);
			this.pnlHWSettingsServer.TabIndex = 14;
			this.txtHWSettingsServerPort.BackColor = Color.White;
			this.txtHWSettingsServerPort.Location = new Point(113, 92);
			this.txtHWSettingsServerPort.Name = "txtHWSettingsServerPort";
			this.txtHWSettingsServerPort.Size = new Size(215, 20);
			this.txtHWSettingsServerPort.TabIndex = 102;
			this.txtHWSettingsServerPort.Tag = "";
			this.txtHWSettingsServerPort.Text = "22222";
			this.txtHWSettingsServerPort.TextAlign = HorizontalAlignment.Center;
			this.txtHWSettingsServerPort.TextChanged += new EventHandler(this.txtHWSettingsServerPort_TextChanged);
			this.lblHWSettingsServerPort.Location = new Point(34, 91);
			this.lblHWSettingsServerPort.Name = "lblHWSettingsServerPort";
			this.lblHWSettingsServerPort.Size = new Size(73, 22);
			this.lblHWSettingsServerPort.TabIndex = 101;
			this.lblHWSettingsServerPort.Tag = "";
			this.lblHWSettingsServerPort.Text = "Port:";
			this.lblHWSettingsServerPort.TextAlign = ContentAlignment.MiddleRight;
			this.txtHWSettingsServerAddress.BackColor = Color.White;
			this.txtHWSettingsServerAddress.Location = new Point(113, 67);
			this.txtHWSettingsServerAddress.Name = "txtHWSettingsServerAddress";
			this.txtHWSettingsServerAddress.Size = new Size(215, 20);
			this.txtHWSettingsServerAddress.TabIndex = 100;
			this.txtHWSettingsServerAddress.Tag = "";
			this.txtHWSettingsServerAddress.Text = "test.solaredge.com";
			this.txtHWSettingsServerAddress.TextAlign = HorizontalAlignment.Center;
			this.txtHWSettingsServerAddress.TextChanged += new EventHandler(this.txtHWSettingsServerAddress_TextChanged);
			this.lblHWSettingsServerAddress.Location = new Point(34, 64);
			this.lblHWSettingsServerAddress.Name = "lblHWSettingsServerAddress";
			this.lblHWSettingsServerAddress.Size = new Size(73, 22);
			this.lblHWSettingsServerAddress.TabIndex = 99;
			this.lblHWSettingsServerAddress.Tag = "";
			this.lblHWSettingsServerAddress.Text = "Address:";
			this.lblHWSettingsServerAddress.TextAlign = ContentAlignment.MiddleRight;
			this.pbHWSettingsServer.BackgroundImage = Resources.Server;
			this.pbHWSettingsServer.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbHWSettingsServer.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbHWSettingsServer.ImageListBack");
			this.pbHWSettingsServer.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbHWSettingsServer.ImageListFore");
			this.pbHWSettingsServer.Location = new Point(350, 2);
			this.pbHWSettingsServer.Name = "pbHWSettingsServer";
			this.pbHWSettingsServer.SelectedImageBack = -1;
			this.pbHWSettingsServer.SelectedImageFore = -1;
			this.pbHWSettingsServer.Size = new Size(45, 61);
			this.pbHWSettingsServer.TabIndex = 98;
			this.pbHWSettingsServer.TabStop = false;
			this.cmbHWSettingsServerVia.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbHWSettingsServerVia.FormattingEnabled = true;
			this.cmbHWSettingsServerVia.Location = new Point(113, 118);
			this.cmbHWSettingsServerVia.Name = "cmbHWSettingsServerVia";
			this.cmbHWSettingsServerVia.Size = new Size(215, 21);
			this.cmbHWSettingsServerVia.TabIndex = 94;
			this.cmbHWSettingsServerVia.SelectedIndexChanged += new EventHandler(this.cmbHWSettingsServerVia_SelectedIndexChanged);
			this.lblHWSettingsServerVia.Location = new Point(6, 115);
			this.lblHWSettingsServerVia.Name = "lblHWSettingsServerVia";
			this.lblHWSettingsServerVia.Size = new Size(101, 22);
			this.lblHWSettingsServerVia.TabIndex = 93;
			this.lblHWSettingsServerVia.Tag = "";
			this.lblHWSettingsServerVia.Text = "Connection Via:";
			this.lblHWSettingsServerVia.TextAlign = ContentAlignment.MiddleRight;
			this.lblHWSettingsServerHeader.BackColor = Color.Transparent;
			this.lblHWSettingsServerHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsServerHeader.Location = new Point(9, 1);
			this.lblHWSettingsServerHeader.Name = "lblHWSettingsServerHeader";
			this.lblHWSettingsServerHeader.Size = new Size(386, 28);
			this.lblHWSettingsServerHeader.TabIndex = 7;
			this.lblHWSettingsServerHeader.Text = "Server Connection Settings";
			this.lblHWSettingsServerHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsServerHeader.MouseDown += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseDown);
			this.lblHWSettingsServerHeader.MouseMove += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseMove);
			this.lblHWSettingsServerHeader.MouseUp += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseUp);
			this.pnlHWSettingsIIC.BackColor = Color.Transparent;
			this.pnlHWSettingsIIC.Controls.Add(this.cmbHWSettingsIICCommMode);
			this.pnlHWSettingsIIC.Controls.Add(this.lblHWSettingsIICCommMode);
			this.pnlHWSettingsIIC.Controls.Add(this.cmbHWSettingsIICCommType);
			this.pnlHWSettingsIIC.Controls.Add(this.lblHWSettingsIICCommType);
			this.pnlHWSettingsIIC.Controls.Add(this.pbHWSettingsIIC);
			this.pnlHWSettingsIIC.Controls.Add(this.lblHWSettingsIICHeader);
			this.pnlHWSettingsIIC.Location = new Point(386, 65);
			this.pnlHWSettingsIIC.Name = "pnlHWSettingsIIC";
			this.pnlHWSettingsIIC.Size = new Size(402, 191);
			this.pnlHWSettingsIIC.TabIndex = 15;
			this.cmbHWSettingsIICCommMode.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbHWSettingsIICCommMode.FormattingEnabled = true;
			this.cmbHWSettingsIICCommMode.Location = new Point(124, 122);
			this.cmbHWSettingsIICCommMode.Name = "cmbHWSettingsIICCommMode";
			this.cmbHWSettingsIICCommMode.Size = new Size(205, 21);
			this.cmbHWSettingsIICCommMode.TabIndex = 120;
			this.cmbHWSettingsIICCommMode.SelectedIndexChanged += new EventHandler(this.cmbHWSettingsIICCommMode_SelectedIndexChanged);
			this.lblHWSettingsIICCommMode.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsIICCommMode.Location = new Point(17, 121);
			this.lblHWSettingsIICCommMode.Name = "lblHWSettingsIICCommMode";
			this.lblHWSettingsIICCommMode.Size = new Size(101, 22);
			this.lblHWSettingsIICCommMode.TabIndex = 119;
			this.lblHWSettingsIICCommMode.Tag = "";
			this.lblHWSettingsIICCommMode.Text = "Comm Mode:";
			this.lblHWSettingsIICCommMode.TextAlign = ContentAlignment.MiddleRight;
			this.cmbHWSettingsIICCommType.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbHWSettingsIICCommType.FormattingEnabled = true;
			this.cmbHWSettingsIICCommType.Location = new Point(124, 91);
			this.cmbHWSettingsIICCommType.Name = "cmbHWSettingsIICCommType";
			this.cmbHWSettingsIICCommType.Size = new Size(205, 21);
			this.cmbHWSettingsIICCommType.TabIndex = 118;
			this.cmbHWSettingsIICCommType.SelectedIndexChanged += new EventHandler(this.cmbHWSettingsIICCommType_SelectedIndexChanged);
			this.lblHWSettingsIICCommType.BackColor = Color.Transparent;
			this.lblHWSettingsIICCommType.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsIICCommType.Location = new Point(12, 92);
			this.lblHWSettingsIICCommType.Name = "lblHWSettingsIICCommType";
			this.lblHWSettingsIICCommType.Size = new Size(106, 16);
			this.lblHWSettingsIICCommType.TabIndex = 117;
			this.lblHWSettingsIICCommType.Text = "Comm Type:";
			this.lblHWSettingsIICCommType.TextAlign = ContentAlignment.MiddleRight;
			this.pbHWSettingsIIC.BackgroundImage = (Image)componentResourceManager.GetObject("pbHWSettingsIIC.BackgroundImage");
			this.pbHWSettingsIIC.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbHWSettingsIIC.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbHWSettingsIIC.ImageListBack");
			this.pbHWSettingsIIC.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbHWSettingsIIC.ImageListFore");
			this.pbHWSettingsIIC.Location = new Point(335, 0);
			this.pbHWSettingsIIC.Name = "pbHWSettingsIIC";
			this.pbHWSettingsIIC.SelectedImageBack = -1;
			this.pbHWSettingsIIC.SelectedImageFore = -1;
			this.pbHWSettingsIIC.Size = new Size(65, 61);
			this.pbHWSettingsIIC.TabIndex = 97;
			this.pbHWSettingsIIC.TabStop = false;
			this.lblHWSettingsIICHeader.BackColor = Color.Transparent;
			this.lblHWSettingsIICHeader.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsIICHeader.Location = new Point(3, 3);
			this.lblHWSettingsIICHeader.Name = "lblHWSettingsIICHeader";
			this.lblHWSettingsIICHeader.Size = new Size(399, 24);
			this.lblHWSettingsIICHeader.TabIndex = 7;
			this.lblHWSettingsIICHeader.Text = "Inter-Inverter Communication Settings";
			this.lblHWSettingsIICHeader.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsIICHeader.MouseDown += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseDown);
			this.lblHWSettingsIICHeader.MouseMove += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseMove);
			this.lblHWSettingsIICHeader.MouseUp += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseUp);
			this.pnlHWSettingsRS485.BackColor = Color.Transparent;
			this.pnlHWSettingsRS485.Controls.Add(this.cmbHWSettingsRS485Configuration);
			this.pnlHWSettingsRS485.Controls.Add(this.lblHWSettingsRS485Configuration);
			this.pnlHWSettingsRS485.Controls.Add(this.pbHWSettingsRS485);
			this.pnlHWSettingsRS485.Controls.Add(this.lblHWSettingsRS485Header);
			this.pnlHWSettingsRS485.Location = new Point(366, 114);
			this.pnlHWSettingsRS485.Name = "pnlHWSettingsRS485";
			this.pnlHWSettingsRS485.Size = new Size(402, 191);
			this.pnlHWSettingsRS485.TabIndex = 16;
			this.cmbHWSettingsRS485Configuration.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbHWSettingsRS485Configuration.FormattingEnabled = true;
			this.cmbHWSettingsRS485Configuration.Location = new Point(124, 90);
			this.cmbHWSettingsRS485Configuration.Name = "cmbHWSettingsRS485Configuration";
			this.cmbHWSettingsRS485Configuration.Size = new Size(205, 21);
			this.cmbHWSettingsRS485Configuration.TabIndex = 118;
			this.cmbHWSettingsRS485Configuration.SelectedIndexChanged += new EventHandler(this.cmbHWSettingsRS485Configuration_SelectedIndexChanged);
			this.lblHWSettingsRS485Configuration.BackColor = Color.Transparent;
			this.lblHWSettingsRS485Configuration.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsRS485Configuration.Location = new Point(12, 92);
			this.lblHWSettingsRS485Configuration.Name = "lblHWSettingsRS485Configuration";
			this.lblHWSettingsRS485Configuration.Size = new Size(106, 16);
			this.lblHWSettingsRS485Configuration.TabIndex = 117;
			this.lblHWSettingsRS485Configuration.Text = "Configuration:";
			this.lblHWSettingsRS485Configuration.TextAlign = ContentAlignment.MiddleRight;
			this.pbHWSettingsRS485.BackgroundImage = (Image)componentResourceManager.GetObject("pbHWSettingsRS485.BackgroundImage");
			this.pbHWSettingsRS485.BackgroundImageLayout = ImageLayout.Zoom;
			this.pbHWSettingsRS485.ImageListBack = (List<Image>)componentResourceManager.GetObject("pbHWSettingsRS485.ImageListBack");
			this.pbHWSettingsRS485.ImageListFore = (List<Image>)componentResourceManager.GetObject("pbHWSettingsRS485.ImageListFore");
			this.pbHWSettingsRS485.Location = new Point(335, 0);
			this.pbHWSettingsRS485.Name = "pbHWSettingsRS485";
			this.pbHWSettingsRS485.SelectedImageBack = -1;
			this.pbHWSettingsRS485.SelectedImageFore = -1;
			this.pbHWSettingsRS485.Size = new Size(65, 61);
			this.pbHWSettingsRS485.TabIndex = 97;
			this.pbHWSettingsRS485.TabStop = false;
			this.lblHWSettingsRS485Header.BackColor = Color.Transparent;
			this.lblHWSettingsRS485Header.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lblHWSettingsRS485Header.Location = new Point(2, 2);
			this.lblHWSettingsRS485Header.Name = "lblHWSettingsRS485Header";
			this.lblHWSettingsRS485Header.Size = new Size(399, 27);
			this.lblHWSettingsRS485Header.TabIndex = 7;
			this.lblHWSettingsRS485Header.Text = "RS 485 Settings";
			this.lblHWSettingsRS485Header.TextAlign = ContentAlignment.MiddleCenter;
			this.lblHWSettingsRS485Header.MouseDown += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseDown);
			this.lblHWSettingsRS485Header.MouseMove += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseMove);
			this.lblHWSettingsRS485Header.MouseUp += new MouseEventHandler(this.lblHWSettingsATCommandsHeader_MouseUp);
			this.sePictureBox1.BackColor = Color.Transparent;
			this.sePictureBox1.BackgroundImage = Resources.SolarEdgeLogo;
			this.sePictureBox1.BackgroundImageLayout = ImageLayout.Zoom;
			this.sePictureBox1.ImageListBack = (List<Image>)componentResourceManager.GetObject("sePictureBox1.ImageListBack");
			this.sePictureBox1.ImageListFore = (List<Image>)componentResourceManager.GetObject("sePictureBox1.ImageListFore");
			this.sePictureBox1.Location = new Point(19, 203);
			this.sePictureBox1.Name = "sePictureBox1";
			this.sePictureBox1.SelectedImageBack = -1;
			this.sePictureBox1.SelectedImageFore = -1;
			this.sePictureBox1.Size = new Size(122, 29);
			this.sePictureBox1.TabIndex = 17;
			this.sePictureBox1.TabStop = false;
			this.lblHeader.BackColor = Color.Transparent;
			this.lblHeader.Location = new Point(26, 3);
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Size = new Size(361, 32);
			this.lblHeader.TabIndex = 18;
			this.lblHeader.Visible = false;
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.DimGray;
			this.BackgroundImage = Resources.BackLandNew;
			this.BackgroundImageLayout = ImageLayout.Stretch;
			base.ClientSize = new Size(414, 244);
			base.ControlBox = false;
			base.Controls.Add(this.pnlHWSettingsPingTest);
			base.Controls.Add(this.pnlHWSettingsRS485);
			base.Controls.Add(this.pnlHWSettingsServer);
			base.Controls.Add(this.pnlHWSettingsRS232);
			base.Controls.Add(this.pnlHWSettingsLAN);
			base.Controls.Add(this.pnlHWSettingsIIC);
			base.Controls.Add(this.btnSettingsCancel);
			base.Controls.Add(this.btnSettingsApply);
			base.Controls.Add(this.sePictureBox1);
			base.Controls.Add(this.pnlHWSettingsZB);
			base.Controls.Add(this.lblHeader);
			this.DoubleBuffered = true;
			base.FormBorderStyle = FormBorderStyle.None;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "HWSettings";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			base.TransparencyKey = Color.DimGray;
			this.pnlHWSettingsZB.ResumeLayout(false);
			this.pnlHWSettingsZB.PerformLayout();
			this.grbHWSettingsZBScanChannel.ResumeLayout(false);
			((ISupportInitialize)this.pbHWSettingsZB).EndInit();
			this.pnlHWSettingsLAN.ResumeLayout(false);
			this.pnlHWSettingsLAN.PerformLayout();
			((ISupportInitialize)this.pbHWSettingsLAN).EndInit();
			this.pnlHWSettingsPingTest.ResumeLayout(false);
			this.pnlHWSettingsPingTest.PerformLayout();
			((ISupportInitialize)this.pbHWSettingsPingTest).EndInit();
			this.pnlHWSettingsRS232.ResumeLayout(false);
			this.pnlHWSettingsRS232.PerformLayout();
			((ISupportInitialize)this.pbHWSettingsRS232GSM).EndInit();
			((ISupportInitialize)this.pbHWSettingsRS232).EndInit();
			this.pnlHWSettingsServer.ResumeLayout(false);
			this.pnlHWSettingsServer.PerformLayout();
			((ISupportInitialize)this.pbHWSettingsServer).EndInit();
			this.pnlHWSettingsIIC.ResumeLayout(false);
			((ISupportInitialize)this.pbHWSettingsIIC).EndInit();
			this.pnlHWSettingsRS485.ResumeLayout(false);
			((ISupportInitialize)this.pbHWSettingsRS485).EndInit();
			((ISupportInitialize)this.sePictureBox1).EndInit();
			base.ResumeLayout(false);
		}

		private HWSettings()
		{
			bool[] scanChannelMaskUse = new bool[16];
			this._scanChannelMaskUse = scanChannelMaskUse;
			this._inverter = null;
			this._wasRS232GSMStringErrorShown = false;
			this._shouldCheckApplyBeActive = true;
			this._shouldChkBxEventRun = true;
			base..ctor();
			this.InitializeComponent();
			this.SetupLabels();
			this.UpdateUI(this);
			this.OrganizeUI();
			this.HookUIEvents();
		}

		private void SetupLabels()
		{
			this.lblHWSettingsZBHeader.SetUI(false, true);
			this.lblHWSettingsZBConfiguration.SetUI(false, false);
			this.lblHWSettingsZBPanID.SetUI(true, true);
			this.grbHWSettingsZBScanChannel.SetUI(false, true);
			this.lblHWSettingsZBScanChannelAll.SetUI(false, true);
			this.lblHWSettingsZBScanChannel0.SetUI(false, false);
			this.lblHWSettingsZBScanChannel1.SetUI(false, false);
			this.lblHWSettingsZBScanChannel2.SetUI(false, false);
			this.lblHWSettingsZBScanChannel3.SetUI(false, false);
			this.lblHWSettingsZBScanChannel4.SetUI(false, false);
			this.lblHWSettingsZBScanChannel5.SetUI(false, false);
			this.lblHWSettingsZBScanChannel6.SetUI(false, false);
			this.lblHWSettingsZBScanChannel7.SetUI(false, false);
			this.lblHWSettingsZBScanChannel8.SetUI(false, false);
			this.lblHWSettingsZBScanChannel9.SetUI(false, false);
			this.lblHWSettingsZBScanChannel10.SetUI(false, false);
			this.lblHWSettingsZBScanChannel11.SetUI(false, false);
			this.lblHWSettingsZBScanChannel12.SetUI(false, false);
			this.lblHWSettingsZBScanChannel13.SetUI(false, false);
			this.lblHWSettingsZBScanChannel14.SetUI(false, false);
			this.lblHWSettingsZBScanChannel15.SetUI(false, false);
			this.lblHWSettingsLANHeader.SetUI(false, true);
			this.lbHWlSettingsLANCurrentIP.SetUI(true, true);
			this.lblHWSettingsLANSubnetMask.SetUI(true, true);
			this.lblHWSettingsLANGateway.SetUI(true, true);
			this.lblHWSettingsLANDNS.SetUI(true, true);
			this.lblHWSettingsLANHeaderCurrent.SetUI(true, true);
			this.lblHWSettingsLANHeaderDetected.SetUI(true, true);
			this.lblHWSettingsLANCurrentIPDetected.SetUI(false, false);
			this.lblHWSettingsLANSubnetMaskDetected.SetUI(false, false);
			this.lblHWSettingsLANGatewayDetected.SetUI(false, false);
			this.lblHWSettingsLANDNSDetected.SetUI(false, false);
			this.lblHWSettingsRS232Header.SetUI(false, true);
			this.lblHWSettingsRS232CommMode.SetUI(true, true);
			this.lblHWSettingsRS232GSMModemType.SetUI(true, true);
			this.lblHWSettingsRS232GSMAPN.SetUI(true, true);
			this.lblHWSettingsRS232GSMUsername.SetUI(true, true);
			this.lblHWSettingsRS232GSMPassword.SetUI(true, true);
			this.lblHWSettingsServerHeader.SetUI(false, true);
			this.lblHWSettingsServerAddress.SetUI(true, true);
			this.lblHWSettingsServerPort.SetUI(true, true);
			this.lblHWSettingsServerVia.SetUI(true, true);
			this.lblHWSettingsLANPingTestHeader.SetUI(false, true);
			this.lblHWSettingsIICHeader.SetUI(false, true);
			this.lblHWSettingsIICCommType.SetUI(true, true);
			this.lblHWSettingsIICCommMode.SetUI(true, true);
			this.lblHWSettingsRS485Header.SetUI(false, true);
			this.lblHWSettingsRS485Configuration.SetUI(true, true);
		}

		private void UpdateUI(Control control)
		{
			try
			{
				if (control != null)
				{
					control.Text = this.GetTextForControl(control);
					if (control.Controls != null && control.Controls.Count > 0)
					{
						int count = control.Controls.Count;
						for (int i = 0; i < count; i++)
						{
							Control control2 = control.Controls[i];
							this.UpdateUI(control2);
						}
					}
				}
			}
			catch (Exception e)
			{
				ExceptionForm.Show(e);
			}
		}

		private string GetTextForControl(Control control)
		{
			string result;
			if (control == null)
			{
				result = null;
			}
			else
			{
				string text = "";
				text = DataManager.Instance.Dictionary[control.Name];
				try
				{
					ISEUI iSEUI = (ISEUI)control;
					if (((ISEUI)control).IsAddColon())
					{
						text += ":";
					}
					else if (!((ISEUI)control).IsUseDictionary())
					{
						text = control.Text;
					}
				}
				catch (InvalidCastException var_3_7E)
				{
				}
				string text2 = text;
				result = text2;
			}
			return result;
		}

		private void OrganizeUI()
		{
			this.pnlHWSettingsZB.Location = new Point(6, 6);
			this.pnlHWSettingsLAN.Location = new Point(6, 6);
			this.pnlHWSettingsRS232.Location = new Point(6, 6);
			this.pnlHWSettingsServer.Location = new Point(6, 6);
			this.pnlHWSettingsPingTest.Location = new Point(6, 6);
			this.pnlHWSettingsIIC.Location = new Point(6, 6);
			this.pnlHWSettingsRS485.Location = new Point(6, 6);
		}

		private void HookUIEvents()
		{
			this.chkBxHWSettingsZBScanChannel0.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel1.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel2.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel3.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel4.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel5.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel6.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel7.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel8.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel9.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel10.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel11.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel12.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel13.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel14.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannel15.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannel_CheckedChanged);
			this.chkBxHWSettingsZBScanChannelAll.CheckedChanged += new EventHandler(this.chkBxHWSettingsZBScanChannelAll_CheckedChanged);
		}

		private string GetZBConfigText(bool isEnabled, bool isConnected, bool isMaster)
		{
			SEDictionary dictionary = DataManager.Instance.Dictionary;
			string result;
			if (isEnabled)
			{
				if (isConnected)
				{
					result = (isMaster ? dictionary["statusMaster"] : dictionary["statusSlave"]);
				}
				else
				{
					result = dictionary["statusDisconnected"];
				}
			}
			else
			{
				result = dictionary["statusDisabled"];
			}
			return result;
		}

		private void HideAll()
		{
			this.pnlHWSettingsZB.Visible = false;
			this.pnlHWSettingsLAN.Visible = false;
			this.pnlHWSettingsRS232.Visible = false;
			this.pnlHWSettingsServer.Visible = false;
			this.pnlHWSettingsPingTest.Visible = false;
			this.pnlHWSettingsIIC.Visible = false;
			this.pnlHWSettingsRS485.Visible = false;
		}

		private void SetButtonsStatus(bool isApply, bool isCancel)
		{
			this.btnSettingsApply.TSAccess.Enabled = isApply;
			this.btnSettingsCancel.TSAccess.Enabled = isCancel;
		}

		private void SetButtonsVisibility(bool isApply, bool isCancel)
		{
			this.btnSettingsApply.TSAccess.Visible = isApply;
			this.btnSettingsCancel.TSAccess.Visible = isCancel;
		}

		public static DialogResult ShowHWSettings(HWType type, Form owner)
		{
			if (HWSettings._instance == null)
			{
				HWSettings._instance = new HWSettings();
			}
			HWSettings._instance.Inverter = DataManager.Instance.GetSelectedInverter();
			SEPanel sEPanel = null;
			switch (type)
			{
			case HWType.ZB:
				sEPanel = HWSettings._instance.pnlHWSettingsZB;
				break;
			case HWType.LAN:
				sEPanel = HWSettings._instance.pnlHWSettingsLAN;
				break;
			case HWType.RS232:
				sEPanel = HWSettings._instance.pnlHWSettingsRS232;
				break;
			case HWType.Server:
				sEPanel = HWSettings._instance.pnlHWSettingsServer;
				break;
			case HWType.PingTest:
				sEPanel = HWSettings._instance.pnlHWSettingsPingTest;
				break;
			case HWType.IIC:
				sEPanel = HWSettings._instance.pnlHWSettingsIIC;
				break;
			case HWType.RS485:
				sEPanel = HWSettings._instance.pnlHWSettingsRS485;
				break;
			}
			HWSettings._instance.HideAll();
			DialogResult result;
			if (sEPanel != null)
			{
				HWSettings._instance.UpdatePanelByType(type);
				sEPanel.Visible = true;
				sEPanel.BringToFront();
				HWSettings._instance.DialogResult = HWSettings._instance.ShowDialog(owner);
				result = HWSettings._instance.DialogResult;
			}
			else
			{
				result = DialogResult.Cancel;
			}
			return result;
		}

		private void UpdatePanelByType(HWType type)
		{
			this._shouldCheckApplyBeActive = false;
			bool isApply = true;
			bool isCancel = true;
			switch (type)
			{
			case HWType.ZB:
				this.UpdatePanelZB();
				break;
			case HWType.LAN:
				this.UpdatePanelLAN();
				break;
			case HWType.RS232:
				this.UpdatePanelRS232();
				break;
			case HWType.Server:
				this.UpdatePanelServer();
				break;
			case HWType.PingTest:
				this.UpdatePanelPingTest();
				isApply = false;
				break;
			case HWType.IIC:
				this.UpdatePanelIIC();
				break;
			case HWType.RS485:
				this.UpdatePanelRS485();
				break;
			default:
				this.HideAll();
				break;
			}
			this._shouldCheckApplyBeActive = true;
			this.SetButtonsVisibility(isApply, isCancel);
		}

		private void PopulateComboBoxByEnum(SEComboBox cmbo, InnerEnumTypes type)
		{
			if (cmbo != null && type != InnerEnumTypes.COUNT)
			{
				DataManager instance = DataManager.Instance;
				cmbo.Items.Clear();
				List<string> enumListByType = DBParameterData.Instance.GetEnumListByType(type, this.Inverter.Portia.DeviceSWVersion, DeviceType.PORTIA);
				int num = (enumListByType != null) ? enumListByType.Count : 0;
				for (int i = 0; i < num; i++)
				{
					string text = enumListByType[i];
					if (!string.IsNullOrEmpty(text) && !cmbo.Items.Contains(text))
					{
						cmbo.Items.Add(instance.Dictionary[text]);
					}
				}
			}
		}

		private void UpdatePanelRS232()
		{
			DataManager instance = DataManager.Instance;
			this.Inverter.UpdateSettingsCommRS232();
			this.PopulateComboBoxByEnum(this.cmbHWSettingsRS232CommMode, InnerEnumTypes.RS_232_TO_SERVER_MODE);
			bool flag = this.Inverter.DataComm.RS232ToServerConnection == 1;
			this.cmbHWSettingsRS232CommMode.SelectedIndex = (flag ? 1 : 0);
			this.SetGSMByType(flag);
			this.txtHWSettingsRS232GSMAPN.TSAccess.Text = this.Inverter.DataComm.RS232GSMAPN;
			this.txtHWSettingsRS232GSMUsername.TSAccess.Text = this.Inverter.DataComm.RS232GSMUsername;
			this.txtHWSettingsRS232GSMPassword.TSAccess.Text = this.Inverter.DataComm.RS232GSMPassword;
			try
			{
				this.txtHWSettingsRS232GSMAPN.MaxLength = ((HWSettings._instance.Inverter.Portia.DeviceSWVersion <= new SWVersion(2u, 69u)) ? 18 : 28);
			}
			catch (Exception)
			{
			}
			this.ValidateRS232GSM();
		}

		private bool ValidateRS232GSM()
		{
			string text = this.txtHWSettingsRS232GSMAPN.TSAccess.Text;
			this.txtHWSettingsRS232GSMAPN.TSAccess.BackColor = ((text.Length > 18) ? HWSettings.COLOR_BAD : HWSettings.COLOR_GOOD);
			bool flag = this.txtHWSettingsRS232GSMAPN.TSAccess.BackColor == HWSettings.COLOR_BAD;
			string text2 = this.txtHWSettingsRS232GSMUsername.TSAccess.Text;
			string text3 = this.txtHWSettingsRS232GSMPassword.TSAccess.Text;
			int num = text2.Length + text3.Length;
			this.txtHWSettingsRS232GSMUsername.TSAccess.BackColor = ((num > 29) ? HWSettings.COLOR_BAD : HWSettings.COLOR_GOOD);
			this.txtHWSettingsRS232GSMPassword.TSAccess.BackColor = ((num > 29) ? HWSettings.COLOR_BAD : HWSettings.COLOR_GOOD);
			flag |= (this.txtHWSettingsRS232GSMUsername.TSAccess.BackColor == HWSettings.COLOR_BAD);
			if (flag && !this._wasRS232GSMStringErrorShown)
			{
				this._wasRS232GSMStringErrorShown = true;
				MessageBox.Show(DataManager.Instance.Dictionary["MsgErrorATCommands"]);
			}
			return !flag;
		}

		private void UpdatePanelRS485()
		{
			this.Inverter.UpdateSettingsCommRS485();
			this.cmbHWSettingsRS485Configuration.Items.Clear();
			this.cmbHWSettingsRS485Configuration.Items.Add("Slave");
			this.cmbHWSettingsRS485Configuration.Items.Add("Master");
			this.cmbHWSettingsRS485Configuration.SelectedIndex = (this.Inverter.DataComm.RS485Master ? 1 : 0);
		}

		private void UpdatePanelZB()
		{
			if (this.Inverter != null)
			{
				this.Inverter.UpdateSettingsCommZigBee();
				string text;
				if (this.Inverter.DataComm.ZigBeePANID <= 0u)
				{
					uint pANIDByInvID = this.GetPANIDByInvID(this.Inverter);
					text = Convert.ToString((long)((ulong)pANIDByInvID), 16).ToUpper();
				}
				else
				{
					text = Convert.ToString((long)((ulong)this.Inverter.DataComm.ZigBeePANID), 16).ToUpper();
				}
				this.txtHWSettingsZBPanID.TSAccess.Text = text;
				string scanChannelChecks = Convert.ToString((int)this.Inverter.DataComm.ZigBeeScanChannel, 16).ToUpper();
				this.SetScanChannelChecks(scanChannelChecks);
				this.chkHWSettingsZBConfigurationEnabled.TSAccess.Checked = this.Inverter.DataComm.ZigBeeEnabled;
				this.lblHWSettingsZBConfiguration.TSAccess.Text = this.GetZBConfigText(this.Inverter.DataComm.ZigBeeEnabled, this.Inverter.DataComm.ZigBeeConnected, this.Inverter.DataComm.ZigBeeMaster);
				this.txtHWSettingsZBPanID.TSAccess.Enabled = (this.Inverter.DataComm.ZigBeeEnabled && this.Inverter.DataComm.ZigBeeConnected);
				this.grbHWSettingsZBScanChannel.TSAccess.Enabled = (this.Inverter.DataComm.ZigBeeEnabled && this.Inverter.DataComm.ZigBeeConnected);
				this.btnSettingsApply.TSAccess.Enabled = (this.Inverter.DataComm.ZigBeeEnabled && this.Inverter.DataComm.ZigBeeConnected && this.IsZBChanged());
			}
		}

		private void UpdatePanelLAN()
		{
			this.Inverter.UpdateSettingsCommLAN();
			this.txtHWSettingsLANCurrentIP.TSAccess.Text = this.Inverter.DataComm.LANStatusParametered.IPStr;
			this.txtHWSettingsLANSubnetMask.TSAccess.Text = this.Inverter.DataComm.LANStatusParametered.SubnetMaskStr;
			this.txtHWSettingsLANGateway.TSAccess.Text = this.Inverter.DataComm.LANStatusParametered.DefaultGatewayStr;
			this.txtHWSettingsLANDNS.TSAccess.Text = this.Inverter.DataComm.LANStatusParametered.DNSIPStr;
			this.chkBxHWSettingsLANDHCP.TSAccess.Checked = this.Inverter.DataComm.LANDHCPEnabled;
			this.txtHWSettingsLANCurrentIP.TSAccess.Enabled = !this.chkBxHWSettingsLANDHCP.Checked;
			this.txtHWSettingsLANSubnetMask.TSAccess.Enabled = !this.chkBxHWSettingsLANDHCP.Checked;
			this.txtHWSettingsLANGateway.TSAccess.Enabled = !this.chkBxHWSettingsLANDHCP.Checked;
			this.txtHWSettingsLANDNS.TSAccess.Enabled = !this.chkBxHWSettingsLANDHCP.Checked;
			this.txtHWSettingsLANCurrentIP.TSAccess.BackColor = (this.chkBxHWSettingsLANDHCP.Checked ? Color.LightGray : Color.White);
			this.txtHWSettingsLANSubnetMask.TSAccess.BackColor = (this.chkBxHWSettingsLANDHCP.Checked ? Color.LightGray : Color.White);
			this.txtHWSettingsLANGateway.TSAccess.BackColor = (this.chkBxHWSettingsLANDHCP.Checked ? Color.LightGray : Color.White);
			this.txtHWSettingsLANDNS.TSAccess.BackColor = (this.chkBxHWSettingsLANDHCP.Checked ? Color.LightGray : Color.White);
			this.lblHWSettingsLANCurrentIPDetected.TSAccess.Text = this.Inverter.DataComm.LANStatusDetected.IPStr;
			this.lblHWSettingsLANSubnetMaskDetected.TSAccess.Text = this.Inverter.DataComm.LANStatusDetected.SubnetMaskStr;
			this.lblHWSettingsLANGatewayDetected.TSAccess.Text = this.Inverter.DataComm.LANStatusDetected.DefaultGatewayStr;
			this.lblHWSettingsLANDNSDetected.TSAccess.Text = this.Inverter.DataComm.LANStatusDetected.DNSIPStr;
		}

		private void UpdatePanelServer()
		{
			if (this.Inverter != null)
			{
				this.Inverter.UpdateSettingsCommServer();
				this.txtHWSettingsServerAddress.TSAccess.Text = this.Inverter.DataComm.ServerAddress;
				this.txtHWSettingsServerPort.TSAccess.Text = this.Inverter.DataComm.ServerPort.ToString();
				this.PopulateComboBoxByEnum(this.cmbHWSettingsServerVia, InnerEnumTypes.STREAM_SERVER);
				uint enumPositionByRealIndex = DBParameterData.Instance.GetEnumPositionByRealIndex(InnerEnumTypes.STREAM_SERVER, this.Inverter.DataComm.ServerCommunicationType);
				this.cmbHWSettingsServerVia.SelectedIndex = (int)enumPositionByRealIndex;
			}
		}

		private void UpdatePanelPingTest()
		{
			this.btnHWSettingsPingTestPing.Enabled = !string.IsNullOrEmpty(this.txtHWSettingsPingTestURL.Text);
			this.btnHWSettingsPingTestClear.Enabled = !string.IsNullOrEmpty(this.txtHWSettingsPingResult.Text);
		}

		private void UpdatePanelIIC()
		{
			this.pbHWSettingsIIC.BackgroundImage = ((this.Inverter is Inverter1Phase) ? Resources.Inverters : Resources.Inverters3Phase);
			DBParameterData instance = DBParameterData.Instance;
			this.Inverter.UpdateSettingsCommIIC();
			this.cmbHWSettingsIICCommType.TSAccess.Visible = true;
			this.PopulateComboBoxByEnum(this.cmbHWSettingsIICCommType, InnerEnumTypes.STREAM_POLESTARS);
			int iICConnectionType = this.Inverter.DataComm.IICConnectionType;
			uint enumPositionByRealIndex = instance.GetEnumPositionByRealIndex(InnerEnumTypes.STREAM_POLESTARS, iICConnectionType);
			int selectedIndex = 0;
			switch (enumPositionByRealIndex)
			{
			case 0u:
				selectedIndex = 0;
				break;
			case 1u:
			case 2u:
				selectedIndex = 1;
				break;
			case 3u:
				selectedIndex = 2;
				break;
			}
			this.cmbHWSettingsIICCommType.SelectedIndex = selectedIndex;
			this.PopulateComboBoxByEnum(this.cmbHWSettingsIICCommMode, InnerEnumTypes.SERVER_COM_MODE);
			int iICCommunicationType = this.Inverter.DataComm.IICCommunicationType;
			uint enumPositionByRealIndex2 = instance.GetEnumPositionByRealIndex(InnerEnumTypes.SERVER_COM_MODE, (iICConnectionType >= 0 && iICConnectionType != 2) ? iICCommunicationType : 2);
			this.cmbHWSettingsIICCommMode.SelectedIndex = (int)enumPositionByRealIndex2;
			this.IsIICChanged();
		}

		private void SetGSMByType(bool isGSM)
		{
			if (isGSM)
			{
				this.PopulateComboBoxByEnum(this.cmbHWSettingsRS232GSMModemType, InnerEnumTypes.GSM_MODEM_TYPE);
				uint enumPositionByRealIndex = DBParameterData.Instance.GetEnumPositionByRealIndex(InnerEnumTypes.GSM_MODEM_TYPE, this.Inverter.DataComm.RS232GSMModemType);
				this.cmbHWSettingsRS232GSMModemType.SelectedIndex = (int)enumPositionByRealIndex;
				this.lblHWSettingsRS232GSMModemType.TSAccess.Visible = true;
				this.cmbHWSettingsRS232GSMModemType.TSAccess.Visible = true;
				this.SetGSMByModemType(enumPositionByRealIndex == 0u);
			}
			else
			{
				this.lblHWSettingsRS232GSMModemType.TSAccess.Visible = false;
				this.cmbHWSettingsRS232GSMModemType.TSAccess.Visible = false;
				this.lblHWSettingsRS232GSMAPN.TSAccess.Visible = false;
				this.txtHWSettingsRS232GSMAPN.TSAccess.Visible = false;
				this.lblHWSettingsRS232GSMUsername.TSAccess.Visible = false;
				this.txtHWSettingsRS232GSMUsername.TSAccess.Visible = false;
				this.lblHWSettingsRS232GSMPassword.TSAccess.Visible = false;
				this.txtHWSettingsRS232GSMPassword.TSAccess.Visible = false;
				this.btnHWSettingsRS232ShowATCommands.TSAccess.Visible = false;
			}
		}

		private void SetGSMByModemType(bool isManual)
		{
			this.lblHWSettingsRS232GSMAPN.TSAccess.Visible = !isManual;
			this.txtHWSettingsRS232GSMAPN.TSAccess.Visible = !isManual;
			this.txtHWSettingsRS232GSMUsername.TSAccess.Visible = !isManual;
			this.lblHWSettingsRS232GSMUsername.TSAccess.Visible = !isManual;
			this.txtHWSettingsRS232GSMPassword.TSAccess.Visible = !isManual;
			this.lblHWSettingsRS232GSMPassword.TSAccess.Visible = !isManual;
			this.pbHWSettingsRS232GSM.TSAccess.Visible = !isManual;
			this.btnHWSettingsRS232ShowATCommands.TSAccess.Visible = isManual;
		}

		private void CheckApplyChanged(HWType type)
		{
			if (this._shouldCheckApplyBeActive)
			{
				bool isApply = false;
				switch (type)
				{
				case HWType.ZB:
					isApply = this.IsZBChanged();
					break;
				case HWType.LAN:
					isApply = this.IsLANChanged();
					break;
				case HWType.RS232:
					isApply = this.IsRS232Changed();
					break;
				case HWType.Server:
					isApply = this.IsServerChanged();
					break;
				case HWType.IIC:
					isApply = this.IsIICChanged();
					break;
				case HWType.RS485:
					isApply = this.IsRS485Changed();
					break;
				}
				this.SetButtonsStatus(isApply, true);
			}
		}

		private bool IsRS232Changed()
		{
			bool flag = false;
			int rS232ToServerConnection = this.Inverter.DataComm.RS232ToServerConnection;
			int selectedIndex = this.cmbHWSettingsRS232CommMode.SelectedIndex;
			flag |= (rS232ToServerConnection != selectedIndex);
			if (selectedIndex == 1)
			{
				int rS232GSMModemType = this.Inverter.DataComm.RS232GSMModemType;
				int selectedIndex2 = this.cmbHWSettingsRS232GSMModemType.SelectedIndex;
				flag |= (rS232GSMModemType != selectedIndex2);
				string rS232GSMAPN = this.Inverter.DataComm.RS232GSMAPN;
				string text = this.txtHWSettingsRS232GSMAPN.TSAccess.Text;
				flag |= !rS232GSMAPN.Equals(text);
				string rS232GSMUsername = this.Inverter.DataComm.RS232GSMUsername;
				string text2 = this.txtHWSettingsRS232GSMUsername.TSAccess.Text;
				flag |= !rS232GSMUsername.Equals(text2);
				string rS232GSMPassword = this.Inverter.DataComm.RS232GSMPassword;
				string text3 = this.txtHWSettingsRS232GSMPassword.TSAccess.Text;
				flag |= !rS232GSMPassword.Equals(text3);
				if (this.ValidateRS232GSM())
				{
					this._wasRS232GSMStringErrorShown = false;
					Dictionary<int, string> changeList = HWSettingsATCommands.GetChangeList(true, true);
					Dictionary<int, string> changeList2 = HWSettingsATCommands.GetChangeList(false, true);
					if ((changeList != null && changeList.Count > 0) || (changeList2 != null && changeList2.Count > 0))
					{
						flag = true;
					}
				}
				else
				{
					flag = false;
				}
			}
			return flag;
		}

		private void RS232Apply()
		{
			bool flag = false;
			bool flag2 = false;
			bool flag3 = false;
			bool flag4 = false;
			bool flag5 = false;
			string text = "";
			string text2 = "";
			string text3 = "";
			int rS232ToServerConnection = this.Inverter.DataComm.RS232ToServerConnection;
			int selectedIndex = this.cmbHWSettingsRS232CommMode.SelectedIndex;
			int selectedIndex2 = this.cmbHWSettingsRS232GSMModemType.SelectedIndex;
			List<PortiaParams> list = new List<PortiaParams>(0);
			List<object> list2 = new List<object>(0);
			if (rS232ToServerConnection != selectedIndex)
			{
				list.Add(PortiaParams.ENABLE_GSM);
				list2.Add(selectedIndex);
				flag = true;
			}
			int rS232GSMModemType = this.Inverter.DataComm.RS232GSMModemType;
			int selectedIndex3 = this.cmbHWSettingsRS232GSMModemType.SelectedIndex;
			if (rS232GSMModemType != selectedIndex3)
			{
				list.Add(PortiaParams.GSM_MODEM_TYPE);
				list2.Add(selectedIndex3);
				flag2 = true;
			}
			if (selectedIndex == 1)
			{
				if (selectedIndex2 == 0)
				{
					int num = 0;
					int num2 = 0;
					this.ATCommandsApply(true, ref num, list, list2);
					list.Add(PortiaParams.GSM_NUM_OF_CONN_CMDS);
					list2.Add(num);
					this.ATCommandsApply(false, ref num2, list, list2);
					list.Add(PortiaParams.GSM_NUM_OF_DISCONN_CMDS);
					list2.Add(num2);
				}
				else
				{
					string rS232GSMAPN = this.Inverter.DataComm.RS232GSMAPN;
					text = this.txtHWSettingsRS232GSMAPN.TSAccess.Text;
					if (!rS232GSMAPN.Equals(text))
					{
						list.Add(PortiaParams.GSM_ACCESS_POINT_NAME);
						list2.Add(text);
						flag3 = true;
					}
					string rS232GSMUsername = this.Inverter.DataComm.RS232GSMUsername;
					text2 = this.txtHWSettingsRS232GSMUsername.TSAccess.Text;
					if (!rS232GSMUsername.Equals(text2))
					{
						list.Add(PortiaParams.GSM_USER_NAME);
						list2.Add(text2);
						flag4 = true;
					}
					string rS232GSMPassword = this.Inverter.DataComm.RS232GSMPassword;
					text3 = this.txtHWSettingsRS232GSMPassword.TSAccess.Text;
					if (!rS232GSMPassword.Equals(text3))
					{
						list.Add(PortiaParams.GSM_PASSWORD);
						list2.Add(text3);
						flag5 = true;
					}
				}
			}
			try
			{
				if (list != null && list.Count > 0)
				{
					this.Inverter.SetParamPortia(list.ToArray(), list2.ToArray());
					if (flag)
					{
						this.Inverter.DataComm.RS232ToServerConnection = selectedIndex;
					}
					if (flag2)
					{
						this.Inverter.DataComm.RS232GSMModemType = selectedIndex3;
					}
					if (flag3)
					{
						this.Inverter.DataComm.RS232GSMAPN = text;
					}
					if (flag4)
					{
						this.Inverter.DataComm.RS232GSMUsername = text2;
					}
					if (flag5)
					{
						this.Inverter.DataComm.RS232GSMPassword = text3;
					}
				}
			}
			catch (Exception var_20_312)
			{
			}
			if (selectedIndex == 1)
			{
				string caption = DataManager.Instance.Dictionary["MsgGSMSelectedCaption"];
				string text4 = DataManager.Instance.Dictionary["MsgGSMSelectedMessage"];
				MessageBox.Show(text4, caption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
			}
		}

		private void ATCommandsApply(bool isConnect, ref int count, List<PortiaParams> paramsList, List<object> dataList)
		{
			if (paramsList != null && dataList != null)
			{
				Dictionary<int, string> changeList = HWSettingsATCommands.GetChangeList(isConnect, true);
				Dictionary<int, string> changeList2 = HWSettingsATCommands.GetChangeList(isConnect, false);
				string format = isConnect ? "GSM_CONN_CMD_{0}" : "GSM_DISCONN_CMD_{0}";
				string format2 = isConnect ? "GSM_CONN_RESP_{0}" : "GSM_DISCONN_RESP_{0}";
				if (changeList != null && changeList.Count > 0 && changeList2 != null && changeList2.Count > 0)
				{
					int count2 = changeList.Count;
					count += count2;
					for (int i = 0; i < count2; i++)
					{
						string value = string.Format(format, i + 1);
						PortiaParams item = (PortiaParams)Enum.Parse(typeof(PortiaParams), value);
						object item2 = changeList[i];
						paramsList.Add(item);
						dataList.Add(item2);
					}
					int count3 = changeList2.Count;
					for (int i = 0; i < count3; i++)
					{
						string value2 = string.Format(format2, i + 1);
						PortiaParams item3 = (PortiaParams)Enum.Parse(typeof(PortiaParams), value2);
						object item4 = changeList2[i];
						paramsList.Add(item3);
						dataList.Add(item4);
					}
				}
			}
		}

		private bool IsRS485Changed()
		{
			int num = this.Inverter.DataComm.RS485Master ? 1 : 0;
			int selectedIndex = this.cmbHWSettingsRS485Configuration.SelectedIndex;
			return num != selectedIndex;
		}

		private void RS485Apply()
		{
			int num = this.Inverter.DataComm.RS485Master ? 1 : 0;
			int selectedIndex = this.cmbHWSettingsRS485Configuration.SelectedIndex;
			bool flag = selectedIndex == 0;
			List<PortiaParams> list = new List<PortiaParams>(0);
			List<object> list2 = new List<object>(0);
			if (num != selectedIndex)
			{
				list.Add(PortiaParams.RS485_MASTER);
				list2.Add((uint)selectedIndex);
			}
			try
			{
				this.Inverter.SetParamPortia(list.ToArray(), list2.ToArray());
				this.Inverter.DataComm.RS485Master = !flag;
			}
			catch (Exception var_5_8D)
			{
			}
			bool flag2 = this.Inverter.Portia.DeviceSWVersion >= DataManager.PORTIA_SW_VERSION_2_0023;
			if (flag)
			{
				bool flag3 = this.Inverter.Slaves != null && this.Inverter.Slaves.Count > 0;
				bool flag4 = !flag2;
				int num2 = -1;
				if (!flag4)
				{
					string paramPortia = this.Inverter.GetParamPortia(PortiaParams.STREAM_POLESTARS);
					num2 = ((paramPortia != null) ? int.Parse(paramPortia) : -1);
				}
				bool flag5 = !flag4 && num2 == 2;
				if (flag3 && (flag4 || flag5))
				{
					DataManager.Instance.ClearDevices(true);
					this.Inverter.FreeSlaves();
				}
			}
			else
			{
				this.Inverter.UpdateSlaves();
				DataManager.Instance.DetectSlavesForDevice(this.Inverter);
			}
			MainForm.Instance.UpdateInverterList = true;
		}

		private bool IsZBChanged()
		{
			bool flag = false;
			bool zigBeeEnabled = this.Inverter.DataComm.ZigBeeEnabled;
			bool @checked = this.chkHWSettingsZBConfigurationEnabled.Checked;
			flag |= (zigBeeEnabled != @checked);
			uint zigBeePANID = this.Inverter.DataComm.ZigBeePANID;
			uint num = (!string.IsNullOrEmpty(this.txtHWSettingsZBPanID.TSAccess.Text)) ? uint.Parse(this.txtHWSettingsZBPanID.TSAccess.Text, NumberStyles.HexNumber) : 0u;
			flag |= (zigBeePANID != num);
			uint zigBeeScanChannel = this.Inverter.DataComm.ZigBeeScanChannel;
			uint scanChannelHex = (uint)this.GetScanChannelHex();
			return flag | zigBeeScanChannel != scanChannelHex;
		}

		private bool ZBApply()
		{
			bool flag = false;
			bool flag2 = false;
			bool flag3 = false;
			bool flag4 = false;
			List<PortiaParams> list = new List<PortiaParams>(0);
			List<object> list2 = new List<object>(0);
			bool zigBeeEnabled = this.Inverter.DataComm.ZigBeeEnabled;
			bool @checked = this.chkHWSettingsZBConfigurationEnabled.Checked;
			if (zigBeeEnabled != @checked)
			{
				list.Add(PortiaParams.ENABLE_ZB);
				list2.Add(@checked ? 1 : 0);
				flag2 = true;
			}
			uint zigBeePANID = this.Inverter.DataComm.ZigBeePANID;
			uint num = (!string.IsNullOrEmpty(this.txtHWSettingsZBPanID.TSAccess.Text)) ? uint.Parse(this.txtHWSettingsZBPanID.TSAccess.Text, NumberStyles.HexNumber) : 0u;
			if (zigBeePANID != num)
			{
				list.Add(PortiaParams.ZIGBEE_PAN_ID);
				list2.Add(num);
				flag3 = true;
			}
			bool result;
			if (num <= 0u)
			{
				SEDictionary dictionary = DataManager.Instance.Dictionary;
				MessageBox.Show(dictionary["MsgZBNoPANIDErrorMessage"], dictionary["MsgZBNoPANIDErrorHeader"], MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
				result = true;
			}
			else
			{
				uint zigBeeScanChannel = this.Inverter.DataComm.ZigBeeScanChannel;
				uint scanChannelHex = (uint)this.GetScanChannelHex();
				if (zigBeeScanChannel != scanChannelHex)
				{
					list.Add(PortiaParams.ZIGBEE_SCAN_CHANNELS_MASK);
					list2.Add(scanChannelHex);
					flag4 = true;
				}
				if (zigBeeScanChannel <= 0u || scanChannelHex <= 0u)
				{
					SEDictionary dictionary = DataManager.Instance.Dictionary;
					MessageBox.Show(dictionary["MsgZBNoChannelErrorMessage"], dictionary["MsgZBNoChannelErrorHeader"], MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
					result = true;
				}
				else
				{
					try
					{
						this.Inverter.SetParamPortia(list.ToArray(), list2.ToArray());
						if (flag2)
						{
							this.Inverter.DataComm.ZigBeeEnabled = @checked;
						}
						if (flag3)
						{
							this.Inverter.DataComm.ZigBeePANID = num;
						}
						if (flag4)
						{
							this.Inverter.DataComm.ZigBeeScanChannel = scanChannelHex;
						}
					}
					catch (Exception var_13_21B)
					{
					}
					bool flag5 = this.Inverter.Portia.DeviceSWVersion >= DataManager.PORTIA_SW_VERSION_2_0023;
					this.Inverter.UpdateSlaves();
					DataManager.Instance.DetectSlavesForDevice(this.Inverter);
					MainForm.Instance.UpdateInverterList = true;
					result = flag;
				}
			}
			return result;
		}

		private bool IsLANChanged()
		{
			bool flag = false;
			bool lANDHCPEnabled = this.Inverter.DataComm.LANDHCPEnabled;
			bool @checked = this.chkBxHWSettingsLANDHCP.TSAccess.Checked;
			flag |= (lANDHCPEnabled != @checked);
			if (!@checked)
			{
				bool enabled = this.txtHWSettingsLANCurrentIP.TSAccess.Enabled;
				string iPStr = this.Inverter.DataComm.LANStatusParametered.IPStr;
				string text = this.txtHWSettingsLANCurrentIP.TSAccess.Text;
				flag |= (enabled && !iPStr.Equals(text));
				bool enabled2 = this.txtHWSettingsLANSubnetMask.TSAccess.Enabled;
				string subnetMaskStr = this.Inverter.DataComm.LANStatusParametered.SubnetMaskStr;
				string text2 = this.txtHWSettingsLANSubnetMask.TSAccess.Text;
				flag |= (enabled2 && !subnetMaskStr.Equals(text2));
				bool enabled3 = this.txtHWSettingsLANGateway.TSAccess.Enabled;
				string defaultGatewayStr = this.Inverter.DataComm.LANStatusParametered.DefaultGatewayStr;
				string text3 = this.txtHWSettingsLANGateway.TSAccess.Text;
				flag |= (enabled3 && !defaultGatewayStr.Equals(text3));
				bool enabled4 = this.txtHWSettingsLANDNS.TSAccess.Enabled;
				string dNSIPStr = this.Inverter.DataComm.LANStatusParametered.DNSIPStr;
				string text4 = this.txtHWSettingsLANDNS.TSAccess.Text;
				flag |= (enabled4 && !dNSIPStr.Equals(text4));
			}
			return flag;
		}

		private void LANApply()
		{
			List<PortiaParams> list = new List<PortiaParams>(0);
			List<object> list2 = new List<object>(0);
			bool flag = false;
			bool flag2 = false;
			bool flag3 = false;
			bool flag4 = false;
			bool flag5 = false;
			bool lANDHCPEnabled = this.Inverter.DataComm.LANDHCPEnabled;
			bool @checked = this.chkBxHWSettingsLANDHCP.TSAccess.Checked;
			if (lANDHCPEnabled != @checked)
			{
				list.Add(PortiaParams.ENABLE_DHCP);
				list2.Add(@checked ? 1 : 0);
				flag = true;
			}
			string iPStr = this.Inverter.DataComm.LANStatusParametered.IPStr;
			string text = this.txtHWSettingsLANCurrentIP.TSAccess.Text;
			if (!iPStr.Equals(text))
			{
				list.Add(PortiaParams.MY_IP);
				list2.Add(text);
				flag2 = true;
			}
			string subnetMaskStr = this.Inverter.DataComm.LANStatusParametered.SubnetMaskStr;
			string text2 = this.txtHWSettingsLANSubnetMask.TSAccess.Text;
			if (!subnetMaskStr.Equals(text2))
			{
				list.Add(PortiaParams.MY_SUBNETMASK_IP);
				list2.Add(text2);
				flag3 = true;
			}
			string defaultGatewayStr = this.Inverter.DataComm.LANStatusParametered.DefaultGatewayStr;
			string text3 = this.txtHWSettingsLANGateway.TSAccess.Text;
			if (!defaultGatewayStr.Equals(text3))
			{
				list.Add(PortiaParams.MY_GATEWAY_IP);
				list2.Add(text3);
				flag4 = true;
			}
			string dNSIPStr = this.Inverter.DataComm.LANStatusParametered.DNSIPStr;
			string text4 = this.txtHWSettingsLANDNS.TSAccess.Text;
			if (!dNSIPStr.Equals(text4))
			{
				list.Add(PortiaParams.MY_DNS_IP);
				list2.Add(text4);
				flag5 = true;
			}
			try
			{
				this.Inverter.SetParamPortia(list.ToArray(), list2.ToArray());
				if (flag)
				{
					this.Inverter.DataComm.LANDHCPEnabled = @checked;
				}
				if (flag2)
				{
					this.Inverter.DataComm.LANStatusParametered.IP = Utils.ConvertToIP(text);
				}
				if (flag3)
				{
					this.Inverter.DataComm.LANStatusParametered.SubnetMask = Utils.ConvertToIP(text2);
				}
				if (flag4)
				{
					this.Inverter.DataComm.LANStatusParametered.DefaultGateway = Utils.ConvertToIP(text3);
				}
				if (flag5)
				{
					this.Inverter.DataComm.LANStatusParametered.DNSIP = Utils.ConvertToIP(text4);
				}
			}
			catch (Exception var_17_280)
			{
			}
		}

		private bool IsServerChanged()
		{
			bool flag = false;
			string serverAddress = this.Inverter.DataComm.ServerAddress;
			string text = this.txtHWSettingsServerAddress.TSAccess.Text;
			flag |= !serverAddress.Equals(text);
			int serverPort = this.Inverter.DataComm.ServerPort;
			int num = (!string.IsNullOrEmpty(this.txtHWSettingsServerPort.TSAccess.Text)) ? int.Parse(this.txtHWSettingsServerPort.TSAccess.Text) : -1;
			flag |= (serverPort != num);
			int serverCommunicationType = this.Inverter.DataComm.ServerCommunicationType;
			uint selectedIndex = (uint)this.cmbHWSettingsServerVia.SelectedIndex;
			int? enumRealIndexByPosition = DBParameterData.Instance.GetEnumRealIndexByPosition(InnerEnumTypes.STREAM_SERVER, selectedIndex);
			int num2 = enumRealIndexByPosition.HasValue ? enumRealIndexByPosition.Value : -1;
			return flag | serverCommunicationType != num2;
		}

		private void ServerApply()
		{
			List<PortiaParams> list = new List<PortiaParams>(0);
			List<object> list2 = new List<object>(0);
			bool flag = false;
			bool flag2 = false;
			bool flag3 = false;
			string serverAddress = this.Inverter.DataComm.ServerAddress;
			string text = this.txtHWSettingsServerAddress.TSAccess.Text;
			if (!serverAddress.Equals(text))
			{
				list.Add(PortiaParams.SERVER_IP1);
				list2.Add(text);
				list.Add(PortiaParams.SERVER_IP2);
				list2.Add(text);
				list.Add(PortiaParams.SERVER_IP3);
				list2.Add(text);
				flag = true;
			}
			int serverPort = this.Inverter.DataComm.ServerPort;
			int num = (!string.IsNullOrEmpty(this.txtHWSettingsServerPort.TSAccess.Text)) ? int.Parse(this.txtHWSettingsServerPort.TSAccess.Text) : -1;
			if (serverPort != num)
			{
				list.Add(PortiaParams.TCP_SERVER_PORT);
				list2.Add(num);
				flag2 = true;
			}
			bool flag4 = false;
			int serverCommunicationType = this.Inverter.DataComm.ServerCommunicationType;
			int selectedIndex = this.cmbHWSettingsServerVia.SelectedIndex;
			int num2 = (selectedIndex >= 0) ? DBParameterData.Instance.GetEnumRealIndexByPosition(InnerEnumTypes.STREAM_SERVER, (uint)selectedIndex).Value : -2;
			if (num2 > -2 && serverCommunicationType != num2)
			{
				list.Add(PortiaParams.STREAM_SERVER);
				list2.Add(num2);
				if (num2 >= 0 && num2 <= 1)
				{
					list.Add(PortiaParams.RS485_MASTER);
					list2.Add(0);
					flag3 = true;
					flag4 = true;
				}
				else if (num2 == 2)
				{
					list.Add(PortiaParams.RS485_MASTER);
					list2.Add(0);
					flag3 = true;
					flag4 = true;
				}
			}
			try
			{
				this.Inverter.SetParamPortia(list.ToArray(), list2.ToArray());
				if (flag)
				{
					this.Inverter.DataComm.ServerAddress = text;
				}
				if (flag2)
				{
					this.Inverter.DataComm.ServerPort = num;
				}
				if (flag3)
				{
					this.Inverter.DataComm.ServerCommunicationType = num2;
				}
			}
			catch (Exception var_13_232)
			{
			}
			if (flag4 && this.Inverter.IsMaster && this.Inverter.CanSustainSlaves(false))
			{
				DataManager.Instance.ClearDevices(true);
				this.Inverter.FreeSlaves();
				MainForm.Instance.UpdateInverterList = true;
			}
		}

		private bool IsIICChanged()
		{
			bool flag = false;
			DBParameterData instance = DBParameterData.Instance;
			int iICConnectionType = this.Inverter.DataComm.IICConnectionType;
			int selectedIndex = this.cmbHWSettingsIICCommType.SelectedIndex;
			int position = 0;
			switch (selectedIndex)
			{
			case 0:
				position = 0;
				break;
			case 1:
				position = 1;
				break;
			case 2:
				position = 3;
				break;
			}
			int value = instance.GetEnumRealIndexByPosition(InnerEnumTypes.STREAM_POLESTARS, (uint)position).Value;
			flag |= (iICConnectionType != value);
			int iICCommunicationType = this.Inverter.DataComm.IICCommunicationType;
			int selectedIndex2 = this.cmbHWSettingsIICCommMode.SelectedIndex;
			bool result;
			if (selectedIndex2 < 0)
			{
				result = flag;
			}
			else
			{
				int value2 = instance.GetEnumRealIndexByPosition(InnerEnumTypes.SERVER_COM_MODE, (uint)selectedIndex2).Value;
				if (value == 2 || value == -1)
				{
					this.cmbHWSettingsIICCommMode.SelectedIndex = 0;
					this.cmbHWSettingsIICCommMode.TSAccess.Enabled = false;
				}
				else if (value == -1)
				{
					this.cmbHWSettingsIICCommMode.TSAccess.Enabled = false;
				}
				else
				{
					uint enumPositionByRealIndex = instance.GetEnumPositionByRealIndex(InnerEnumTypes.SERVER_COM_MODE, value2);
					this.cmbHWSettingsIICCommMode.SelectedIndex = (int)enumPositionByRealIndex;
					this.cmbHWSettingsIICCommMode.TSAccess.Enabled = true;
				}
				flag |= (iICCommunicationType != value2);
				result = flag;
			}
			return result;
		}

		private void IICApply()
		{
			List<PortiaParams> list = new List<PortiaParams>(0);
			List<object> list2 = new List<object>(0);
			DBParameterData instance = DBParameterData.Instance;
			bool flag = false;
			bool flag2 = false;
			int iICConnectionType = this.Inverter.DataComm.IICConnectionType;
			int selectedIndex = this.cmbHWSettingsIICCommType.SelectedIndex;
			int num = iICConnectionType;
			switch (selectedIndex)
			{
			case 0:
				num = 0;
				break;
			case 1:
				num = 1;
				break;
			case 2:
				num = 3;
				break;
			}
			int num2 = (num >= 0) ? instance.GetEnumRealIndexByPosition(InnerEnumTypes.STREAM_POLESTARS, (uint)num).Value : -2;
			if (num2 > -2 && iICConnectionType != num)
			{
				list.Add(PortiaParams.STREAM_POLESTARS);
				list2.Add(num2);
				flag = true;
			}
			int iICCommunicationType = this.Inverter.DataComm.IICCommunicationType;
			int selectedIndex2 = this.cmbHWSettingsIICCommMode.SelectedIndex;
			int num3 = (selectedIndex2 >= 0) ? instance.GetEnumRealIndexByPosition(InnerEnumTypes.SERVER_COM_MODE, (uint)selectedIndex2).Value : -2;
			if (num3 > -2 && iICCommunicationType != selectedIndex2)
			{
				list.Add(PortiaParams.SERVER_COM_MODE);
				list2.Add(num3);
				flag2 = true;
			}
			try
			{
				this.Inverter.SetParamPortia(list.ToArray(), list2.ToArray());
				if (flag)
				{
					this.Inverter.DataComm.IICConnectionType = num2;
				}
				if (flag2)
				{
					this.Inverter.DataComm.IICCommunicationType = num3;
				}
			}
			catch (Exception var_12_17D)
			{
			}
			bool flag3 = this.Inverter.Portia.DeviceSWVersion >= DataManager.PORTIA_SW_VERSION_2_0023;
			bool flag4 = this.Inverter.Slaves != null && this.Inverter.Slaves.Count > 0;
			string paramPortia = this.Inverter.GetParamPortia(PortiaParams.STREAM_POLESTARS);
			int num4 = (paramPortia != null) ? int.Parse(paramPortia) : -1;
			if (flag3)
			{
				if (flag4 && num4 < 0)
				{
					DataManager.Instance.ClearDevices(true);
					this.Inverter.FreeSlaves();
				}
				else
				{
					Inverter selectedInverter = DataManager.Instance.GetSelectedInverter();
					if (selectedInverter != null)
					{
						DataManager.Instance.ClearDevices(true);
						selectedInverter.FreeSlaves();
						if (selectedInverter.IsMaster)
						{
							selectedInverter.Action_DetectSlaves();
						}
					}
				}
				MainForm.Instance.UpdateInverterList = true;
			}
		}

		private uint GetPANIDByInvID(Inverter selInv)
		{
			uint num = 0u;
			uint result;
			if (selInv == null)
			{
				result = num;
			}
			else
			{
				uint address = DataManager.Instance.GetSelectedInverter().Portia.Address;
				if (address > 255u)
				{
					string text = Convert.ToString((long)((ulong)address), 2);
					text = Utils.Reverse(text);
					if (text.Length > 16)
					{
						text = text.Substring(0, 16);
					}
					text = Utils.Reverse(text);
					num = Convert.ToUInt32(text, 2);
				}
				result = num;
			}
			return result;
		}

		private void SetScanChannelData(int channel, bool isEnabled)
		{
			if (channel >= 0 && channel < this._scanChannelMaskUse.Length)
			{
				this._scanChannelMaskUse[channel] = isEnabled;
			}
		}

		private void SetScanChannelUI(int channel, bool isEnabled)
		{
			if (channel >= 0 && channel < this._scanChannelMaskUse.Length)
			{
				string format = "chkBxHWSettingsZBScanChannel{0}";
				try
				{
					CheckBox checkBox = (CheckBox)this.grbHWSettingsZBScanChannel.Controls[string.Format(format, channel)];
					this._shouldChkBxEventRun = false;
					checkBox.Checked = isEnabled;
					this._shouldChkBxEventRun = true;
				}
				catch (Exception var_2_5B)
				{
				}
				this._scanChannelMaskUse[channel] = isEnabled;
			}
		}

		private void SetScanChannelChecks(string hexValue)
		{
			this._shouldChkBxEventRun = false;
			int value = Convert.ToInt32(hexValue, 16);
			string text = Convert.ToString(value, 2);
			int length = text.Length;
			int num = 16 - length;
			for (int i = 0; i < num; i++)
			{
				text = "0" + text;
			}
			text = Utils.Reverse(text);
			int length2 = text.Length;
			if (length2 == this._scanChannelMaskUse.Length)
			{
				for (int i = 0; i < length2; i++)
				{
					bool isEnabled = text[i].ToString().Equals("1");
					this.SetScanChannelData(i, isEnabled);
					this.SetScanChannelUI(i, isEnabled);
				}
				this._shouldChkBxEventRun = true;
			}
		}

		private int GetScanChannelHex()
		{
			int num = 0;
			for (int i = 0; i < 15; i++)
			{
				if (this._scanChannelMaskUse[i])
				{
					if (i == 0)
					{
						num++;
					}
					else
					{
						num += 1 << i;
					}
				}
			}
			return num;
		}

		protected override void OnShown(EventArgs e)
		{
			this.UpdateUI(this);
			base.OnShown(e);
		}

		protected override void OnClosed(EventArgs e)
		{
			this.Inverter = null;
			this.HideAll();
			base.OnClosed(e);
		}

		private void chkBxHWSettingsZBScanChannel_CheckedChanged(object sender, EventArgs e)
		{
			if (this._shouldChkBxEventRun)
			{
				try
				{
					CheckBox checkBox = (CheckBox)sender;
					string name = checkBox.Name;
					string s = name.Substring(name.Length - 1, 1);
					int channel = int.Parse(s);
					this.SetScanChannelData(channel, checkBox.Checked);
				}
				catch (Exception var_4_46)
				{
				}
				this.CheckApplyChanged(HWType.ZB);
			}
		}

		private void chkBxHWSettingsZBScanChannelAll_CheckedChanged(object sender, EventArgs e)
		{
			SECheckBox sECheckBox = (SECheckBox)sender;
			if (sECheckBox != null)
			{
				bool @checked = sECheckBox.Checked;
				for (int i = 0; i < 16; i++)
				{
					this.SetScanChannelData(i, @checked);
					this.SetScanChannelUI(i, @checked);
				}
				this.CheckApplyChanged(HWType.ZB);
			}
		}

		private void txtHWSettingsZBPanID_KeyDown(object sender, KeyEventArgs e)
		{
			SETextBox sETextBox = (SETextBox)sender;
			uint num = (!string.IsNullOrEmpty(sETextBox.Text)) ? Convert.ToUInt32(sETextBox.Text, 16) : 0u;
			if (num == 0u)
			{
				num = this.GetPANIDByInvID(DataManager.Instance.GetSelectedInverter());
				this.txtHWSettingsZBPanID.Text = num.ToString();
			}
			if (num > 0u && !sETextBox.IsDataValid(DataValidationType.HEX, e.KeyValue))
			{
				e.SuppressKeyPress = true;
			}
		}

		private void btnHWSettingsPingTestPing_Click(object sender, EventArgs e)
		{
			DataManager instance = DataManager.Instance;
			SEDictionary dictionary = instance.Dictionary;
			Inverter selectedInverter = instance.GetSelectedInverter();
			PING_RESULT pING_RESULT = PING_RESULT.PR_NO_REPLY;
			if (selectedInverter != null)
			{
				this.txtHWSettingsPingResult.TSAccess.Enabled = true;
				try
				{
					pING_RESULT = selectedInverter.Portia.Command_Portia_PingTest(this.txtHWSettingsPingTestURL.TSAccess.Text, 3);
				}
				catch (TimeoutException var_4_58)
				{
				}
				bool flag = pING_RESULT == PING_RESULT.PR_OK;
				DateTime now = DateTime.Now;
				SETextBoxThreadSafeAccess expr_77 = this.txtHWSettingsPingResult.TSAccess;
				expr_77.Text += string.Format(dictionary["MsgPingMessage"], new object[]
				{
					now.Hour,
					now.Minute,
					now.Second,
					dictionary[pING_RESULT.ToString()] + "\r\n"
				});
				this.txtHWSettingsPingResult.SelectionStart = this.txtHWSettingsPingResult.Text.Length - 1;
				this.txtHWSettingsPingResult.ScrollToCaret();
			}
		}

		private void btnHWSettingsPingTestClear_Click(object sender, EventArgs e)
		{
			this.txtHWSettingsPingResult.Clear();
		}

		private void txtHWSettingsPingResult_TextChanged(object sender, EventArgs e)
		{
			SETextBox sETextBox = (SETextBox)sender;
			this.btnHWSettingsPingTestClear.TSAccess.Enabled = !string.IsNullOrEmpty(sETextBox.TSAccess.Text);
		}

		private void btnSettingsApply_Click(object sender, EventArgs e)
		{
			Cursor cursor = this.Cursor;
			this.Cursor = Cursors.WaitCursor;
			bool flag = false;
			if (this.pnlHWSettingsRS232.Visible)
			{
				this.RS232Apply();
				this.Inverter.UpdateSettingsCommRS232();
			}
			if (this.pnlHWSettingsRS485.Visible)
			{
				this.RS485Apply();
				this.Inverter.UpdateSettingsCommRS485();
				this.Inverter.UpdateSettingsCommServer();
			}
			if (this.pnlHWSettingsZB.Visible)
			{
				flag = this.ZBApply();
				this.Inverter.UpdateSettingsCommZigBee();
				this.Inverter.UpdateSettingsCommServer();
			}
			if (this.pnlHWSettingsLAN.Visible)
			{
				this.LANApply();
				this.Inverter.UpdateSettingsCommLAN();
			}
			if (this.pnlHWSettingsServer.Visible)
			{
				this.ServerApply();
				this.Inverter.UpdateSettingsCommServer();
				this.Inverter.UpdateSettingsCommRS485();
				this.Inverter.UpdateSettingsCommZigBee();
			}
			if (this.pnlHWSettingsIIC.Visible)
			{
				this.IICApply();
				this.Inverter.UpdateSettingsCommIIC();
			}
			this.Cursor = cursor;
			if (!flag)
			{
				base.DialogResult = DialogResult.OK;
				base.Close();
			}
		}

		private void btnSettingsCancel_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void txtHWSettingsPingResult_EnabledChanged(object sender, EventArgs e)
		{
			SETextBox sETextBox = (SETextBox)sender;
			sETextBox.TSAccess.BackColor = (sETextBox.TSAccess.Enabled ? Color.White : Color.Gray);
		}

		private void txtHWSettingsPingTestURL_TextChanged(object sender, EventArgs e)
		{
			SETextBox sETextBox = (SETextBox)sender;
			this.btnHWSettingsPingTestPing.TSAccess.Enabled = !string.IsNullOrEmpty(sETextBox.TSAccess.Text);
		}

		private void cmbHWSettingsIICCommType_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.IIC);
		}

		private void cmbHWSettingsIICCommMode_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.IIC);
		}

		private void txtHWSettingsServerAddress_TextChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.Server);
		}

		private void txtHWSettingsServerPort_TextChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.Server);
		}

		private void cmbHWSettingsServerVia_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.Server);
		}

		private void txtHWSettingsLANCurrentIP_TextChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.LAN);
		}

		private void txtHWSettingsLANSubnetMask_TextChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.LAN);
		}

		private void txtHWSettingsLANGateway_TextChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.LAN);
		}

		private void txtHWSettingsLANDNS_TextChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.LAN);
		}

		private void chkBxHWSettingsLANDHCP_CheckedChanged(object sender, EventArgs e)
		{
			SECheckBox sECheckBox = (SECheckBox)sender;
			if (sECheckBox != null)
			{
				this.CheckApplyChanged(HWType.LAN);
				this.txtHWSettingsLANCurrentIP.TSAccess.Enabled = !sECheckBox.Checked;
				this.txtHWSettingsLANSubnetMask.TSAccess.Enabled = !sECheckBox.Checked;
				this.txtHWSettingsLANGateway.TSAccess.Enabled = !sECheckBox.Checked;
				this.txtHWSettingsLANDNS.TSAccess.Enabled = !sECheckBox.Checked;
				this.txtHWSettingsLANCurrentIP.TSAccess.BackColor = (sECheckBox.Checked ? Color.LightGray : Color.White);
				this.txtHWSettingsLANSubnetMask.TSAccess.BackColor = (sECheckBox.Checked ? Color.LightGray : Color.White);
				this.txtHWSettingsLANGateway.TSAccess.BackColor = (sECheckBox.Checked ? Color.LightGray : Color.White);
				this.txtHWSettingsLANDNS.TSAccess.BackColor = (sECheckBox.Checked ? Color.LightGray : Color.White);
			}
		}

		private void txtHWSettingsZBPanID_TextChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.ZB);
		}

		private void txtHWSettingsZBScanChannel_TextChanged_1(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.ZB);
		}

		private void cmbHWSettingsRS232CommMode_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.RS232);
			int selectedIndex = this.cmbHWSettingsRS232CommMode.SelectedIndex;
			this.SetGSMByType(selectedIndex > 0);
		}

		private void cmbHWSettingsRS232GSMModemType_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.RS232);
			int selectedIndex = this.cmbHWSettingsRS232GSMModemType.SelectedIndex;
			this.SetGSMByModemType(selectedIndex == 0);
		}

		private void txtHWSettingsRS232GSMAPN_TextChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.RS232);
		}

		private void txtHWSettingsRS232GSMUsername_TextChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.RS232);
		}

		private void txtHWSettingsRS232GSMPassword_TextChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.RS232);
		}

		private void txtHWSettingsRS232GSMATCommands_TextChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.RS232);
		}

		private void cmbHWSettingsRS485Configuration_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.CheckApplyChanged(HWType.RS485);
		}

		private void btnHWSettingsRS232ShowATCommands_Click(object sender, EventArgs e)
		{
			HWSettingsATCommands.ShowHWSettingsATCommands(this, true);
		}

		private void lblHWSettingsATCommandsHeader_MouseDown(object sender, MouseEventArgs e)
		{
			SELabel sELabel = (SELabel)sender;
			base.HandleMouseDown(e.X, e.Y, sELabel.Size.Height);
		}

		private void lblHWSettingsATCommandsHeader_MouseMove(object sender, MouseEventArgs e)
		{
			base.HandleMouseMove(e.X, e.Y);
		}

		private void lblHWSettingsATCommandsHeader_MouseUp(object sender, MouseEventArgs e)
		{
			base.HandleMouseUp(e.X, e.Y);
		}

		private void chkHWSettingsZBConfigurationEnabled_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox checkBox = (CheckBox)sender;
			this.lblHWSettingsZBConfiguration.TSAccess.Text = this.GetZBConfigText(checkBox.Checked, this.Inverter.DataComm.ZigBeeConnected, this.Inverter.DataComm.ZigBeeMaster);
			this.txtHWSettingsZBPanID.TSAccess.Enabled = (checkBox.Checked && this.Inverter.DataComm.ZigBeeConnected);
			this.grbHWSettingsZBScanChannel.TSAccess.Enabled = (checkBox.Checked && this.Inverter.DataComm.ZigBeeConnected);
			this.btnSettingsApply.TSAccess.Enabled = (checkBox.Checked && this.Inverter.DataComm.ZigBeeConnected);
			this.CheckApplyChanged(HWType.ZB);
		}
	}
}
