using System;

namespace InverterConfigurationTool.Src.Ui.DataEntry
{
	public enum ConnectionType
	{
		NONE = -1,
		SERIAL,
		TCP,
		PORT_REPLICATOR,
		COUNT
	}
}
