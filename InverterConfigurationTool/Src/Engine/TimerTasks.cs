using System;

namespace InverterConfigurationTool.Src.Engine
{
	public enum TimerTasks
	{
		COMM_START,
		COMM_CLOSE,
		WAIT_500,
		WAIT_1000,
		WAIT_2000,
		COUNT
	}
}
