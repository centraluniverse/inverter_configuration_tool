using System;

namespace InverterConfigurationTool.Src.Model.Data
{
	public enum InverterTelemetrySpeedType
	{
		SLOW,
		NORMAL,
		FAST
	}
}
