using InverterConfigurationTool.Src.Engine;
using SEStorage;
using System;
using System.Collections.Generic;
using System.Data;

namespace InverterConfigurationTool.Src.Model.Data
{
	public class DBPowerBoxData : DBExcel
	{
		public const string WS_TELEM_NAME = "Panels Telemetries";

		public DBPowerBoxData() : base(null, FileTypes.XLS)
		{
		}

		private void setupWorksheet(DataTable powerBoxTable)
		{
			int count = powerBoxTable.Columns.Count;
			if (base.AddWorkSheet("Panels Telemetries"))
			{
				for (int i = 0; i < count - 1; i++)
				{
					DataColumn dataColumn = powerBoxTable.Columns[i];
					string data = DataManager.Instance.Dictionary[dataColumn.ColumnName];
					base.SetCell("Panels Telemetries", 0, i, data);
				}
				int count2 = powerBoxTable.Rows.Count;
				for (int i = 0; i < count2; i++)
				{
					DataRow dataRow = powerBoxTable.Rows[i];
					int num = dataRow.ItemArray.Length - 1;
					for (int j = 0; j < num; j++)
					{
						object data2 = dataRow.ItemArray[j];
						base.SetCell("Panels Telemetries", i + 1, j, data2);
					}
				}
			}
		}

		public void ExportDataToWorksheet(DataTable powerBoxTable)
		{
			base.ResetWorkbook();
			this.setupWorksheet(powerBoxTable);
		}

		public void SaveWorksheet(string fileName)
		{
			if (!string.IsNullOrEmpty(fileName))
			{
				base.SaveXLSDatabase(fileName);
			}
		}

		public DataTable LoadWorksheet(string path)
		{
			DataTable result = null;
			try
			{
				base.LoadXLSDatabase(path);
				DataTable dataTable = new DataTable();
				dataTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_POWER_BOX_PANEL_SERIAL_NUMBER.ToString(), typeof(string)));
				dataTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_VOLTAGE.ToString(), typeof(float)));
				dataTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_PB_VOLTAGE.ToString(), typeof(float)));
				dataTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_CURRENT.ToString(), typeof(float)));
				dataTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_POWER.ToString(), typeof(float)));
				dataTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_TELEM_TIME.ToString(), typeof(string)));
				dataTable.Columns.Add(new DataColumn(PowerBoxColumns.COL_PANEL_VER.ToString(), typeof(string)));
				int num = 0;
				object[] row;
				while ((row = base.GetRow("Panels Telemetries", ++num)) != null)
				{
					List<object> list = new List<object>(0);
					int num2 = row.Length;
					for (int i = 0; i < num2; i++)
					{
						list.Add(row[i]);
					}
					list.Add("0");
					dataTable.Rows.Add(list.ToArray());
				}
				result = dataTable;
			}
			catch (Exception ex)
			{
				base.ResetWorkbook();
				throw ex;
			}
			return result;
		}
	}
}
