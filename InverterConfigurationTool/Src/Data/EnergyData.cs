using InverterConfigurationTool.Src.Model.Data.Base;
using InverterConfigurationTool.Src.Model.Devices.Base;
using System;

namespace InverterConfigurationTool.Src.Model.Data
{
	public class EnergyData : DeviceData
	{
		private float _energyDay = 0f;

		private float _energyMonth = 0f;

		private float _energyYear = 0f;

		private float _energyTotal = 0f;

		public float EnergyDay
		{
			get
			{
				float energyDay;
				lock (this)
				{
					energyDay = this._energyDay;
				}
				return energyDay;
			}
			set
			{
				lock (this)
				{
					this._energyDay = value;
					base.DataChanged = true;
				}
			}
		}

		public float EnergyMonth
		{
			get
			{
				float energyMonth;
				lock (this)
				{
					energyMonth = this._energyMonth;
				}
				return energyMonth;
			}
			set
			{
				lock (this)
				{
					this._energyMonth = value;
					base.DataChanged = true;
				}
			}
		}

		public float EnergyYear
		{
			get
			{
				float energyYear;
				lock (this)
				{
					energyYear = this._energyYear;
				}
				return energyYear;
			}
			set
			{
				lock (this)
				{
					this._energyYear = value;
					base.DataChanged = true;
				}
			}
		}

		public float EnergyTotal
		{
			get
			{
				float energyTotal;
				lock (this)
				{
					energyTotal = this._energyTotal;
				}
				return energyTotal;
			}
			set
			{
				lock (this)
				{
					this._energyTotal = value;
					base.DataChanged = true;
				}
			}
		}

		public EnergyData(Inverter inverter) : base(inverter)
		{
		}
	}
}
