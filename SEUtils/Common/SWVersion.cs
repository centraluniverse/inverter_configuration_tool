using System;
using System.Xml.Serialization;

namespace SEUtils.Common
{
	public class SWVersion : ICloneable, IComparable, IComparable<SWVersion>, IEquatable<SWVersion>
	{
		private const string HEX_PREFIX = "0x";

		private const string BETA_KEY = "(Beta)";

		private const string SPACE = " ";

		private const string PAD_ZERO = "0";

		public const bool WITH_DECIMAL_CHECKSUM = true;

		public const bool WITH_HEX_CHECKSUM = false;

		public const int DEF_PADDING = 0;

		public const int[] NO_PADDING = null;

		private uint _major;

		private uint _minor;

		private uint _build;

		private uint _revision;

		private uint? _checkSum;

		private bool _isBeta;

		[XmlElement("Major")]
		public uint Major
		{
			get
			{
				return this._major;
			}
			set
			{
				this._major = value;
			}
		}

		[XmlElement("Minor")]
		public uint Minor
		{
			get
			{
				return this._minor;
			}
			set
			{
				this._minor = value;
			}
		}

		[XmlElement("Build")]
		public uint Build
		{
			get
			{
				return this._build;
			}
			set
			{
				this._build = value;
			}
		}

		[XmlElement("Revision")]
		public uint Revision
		{
			get
			{
				return this._revision;
			}
			set
			{
				this._revision = value;
			}
		}

		[XmlElement("Checksum")]
		public uint? CheckSum
		{
			get
			{
				return this._checkSum;
			}
			set
			{
				this._checkSum = value;
			}
		}

		public string CheckSumHEX
		{
			get
			{
				string result = null;
				if (this._checkSum.HasValue)
				{
					uint value = this._checkSum.Value;
					result = Convert.ToString((long)((ulong)value), 16).ToUpper();
				}
				return result;
			}
		}

		[XmlElement("Beta")]
		public bool Beta
		{
			get
			{
				return this._isBeta;
			}
			set
			{
				this._isBeta = value;
			}
		}

		public SWVersion() : this(0u, 0u, 0u, 0u, null, false)
		{
		}

		public SWVersion(uint major) : this(major, 0u, 0u, 0u, null, false)
		{
		}

		public SWVersion(uint major, uint minor) : this(major, minor, 0u, 0u, null, false)
		{
		}

		public SWVersion(uint major, uint minor, uint build) : this(major, minor, build, 0u, null, false)
		{
		}

		public SWVersion(uint major, uint minor, uint build, uint revision) : this(major, minor, build, revision, null, false)
		{
		}

		public SWVersion(uint major, uint minor, uint build, uint revision, uint? checkSum) : this(major, minor, build, revision, checkSum, false)
		{
		}

		public SWVersion(uint major, uint minor, uint build, uint revision, uint? checkSum, bool isBeta)
		{
			this._major = 0u;
			this._minor = 0u;
			this._build = 0u;
			this._revision = 0u;
			this._checkSum = null;
			this._isBeta = false;
			base..ctor();
			this.Major = major;
			this.Minor = minor;
			this.Build = build;
			this.Revision = revision;
			this.CheckSum = checkSum;
			this.Beta = isBeta;
		}

		public SWVersion(SWVersion version)
		{
			this._major = 0u;
			this._minor = 0u;
			this._build = 0u;
			this._revision = 0u;
			this._checkSum = null;
			this._isBeta = false;
			base..ctor();
			if (version == null)
			{
				throw new NullReferenceException("No version to copy from");
			}
			this.Major = version.Major;
			this.Minor = version.Minor;
			this.Build = version.Build;
			this.Revision = version.Revision;
			this.CheckSum = version.CheckSum;
			this.Beta = version.Beta;
		}

		public SWVersion(string version)
		{
			this._major = 0u;
			this._minor = 0u;
			this._build = 0u;
			this._revision = 0u;
			this._checkSum = null;
			this._isBeta = false;
			base..ctor();
			if (version == null)
			{
				throw new NullReferenceException("version string is null!");
			}
			if (!version.Equals(""))
			{
				string[] array = version.Split(new string[]
				{
					" "
				}, StringSplitOptions.None);
				int num = (array != null) ? array.Length : 0;
				bool flag = false;
				bool flag2 = false;
				bool flag3 = false;
				for (int i = 0; i < num; i++)
				{
					string text = array[i];
					if (!flag && !flag2 && !flag3 && text.Contains("."))
					{
						flag = true;
						string[] array2 = text.Split(new string[]
						{
							"."
						}, StringSplitOptions.None);
						int num2 = (array2 != null) ? array2.Length : 0;
						for (int j = 0; j < num2; j++)
						{
							try
							{
								string s = array2[j];
								uint num3 = uint.Parse(s);
								switch (j)
								{
								case 0:
									this.Major = num3;
									break;
								case 1:
									this.Minor = num3;
									break;
								case 2:
									this.Build = num3;
									break;
								case 3:
									this.Revision = num3;
									break;
								}
							}
							catch (Exception var_12_158)
							{
							}
						}
					}
					else if (flag && !flag2 && !flag3 && !text.Equals("(Beta)", StringComparison.CurrentCultureIgnoreCase))
					{
						try
						{
							text = text.Substring(1, text.Length - 2);
							bool flag4 = text.StartsWith("0x", StringComparison.CurrentCultureIgnoreCase);
							uint num3;
							if (flag4)
							{
								string value = text.Substring(2);
								num3 = Convert.ToUInt32(value, 16);
							}
							else
							{
								num3 = Convert.ToUInt32(text);
							}
							this.CheckSum = new uint?(num3);
							flag2 = true;
						}
						catch (Exception var_12_158)
						{
						}
					}
					else if (flag && text.Equals("(Beta)", StringComparison.CurrentCultureIgnoreCase))
					{
						flag3 = true;
						this.Beta = true;
					}
				}
			}
		}

		public override string ToString()
		{
			return this.ToString(FieldEnums.COUNT_ENUM);
		}

		public string ToString(FieldEnums upToVersionPart)
		{
			return this.ToString(upToVersionPart, null);
		}

		public string ToString(FieldEnums upToVersionPart, int[] padding)
		{
			return this.ToString(upToVersionPart, null, (VersionOptions)12);
		}

		public string ToString(FieldEnums upToVersionPart, int[] padding, VersionOptions options)
		{
			return this.GetSWVersionAsString(upToVersionPart, padding, options);
		}

		public static bool operator !=(SWVersion v1, SWVersion v2)
		{
			return SWVersion.CheckVersions(v1, v2, FieldEnums.MAJOR_ENUM) != 0;
		}

		public static bool operator <(SWVersion v1, SWVersion v2)
		{
			return SWVersion.CheckVersions(v1, v2, FieldEnums.MAJOR_ENUM) == -1;
		}

		public static bool operator <=(SWVersion v1, SWVersion v2)
		{
			int num = SWVersion.CheckVersions(v1, v2, FieldEnums.MAJOR_ENUM);
			return num == -1 || num == 0;
		}

		public static bool operator ==(SWVersion v1, SWVersion v2)
		{
			return SWVersion.CheckVersions(v1, v2, FieldEnums.MAJOR_ENUM) == 0;
		}

		public static bool operator >(SWVersion v1, SWVersion v2)
		{
			return SWVersion.CheckVersions(v1, v2, FieldEnums.MAJOR_ENUM) == 1;
		}

		public static bool operator >=(SWVersion v1, SWVersion v2)
		{
			int num = SWVersion.CheckVersions(v1, v2, FieldEnums.MAJOR_ENUM);
			return num == 1 || num == 0;
		}

		public object Clone()
		{
			return new SWVersion
			{
				Major = this.Major,
				Minor = this.Minor,
				Build = this.Build,
				Revision = this.Revision,
				CheckSum = this.CheckSum,
				Beta = this.Beta
			};
		}

		public int CompareTo(object version)
		{
			SWVersion ver = (SWVersion)version;
			return SWVersion.CheckVersions(this, ver, FieldEnums.MAJOR_ENUM);
		}

		public int CompareTo(SWVersion value)
		{
			return SWVersion.CheckVersions(this, value, FieldEnums.MAJOR_ENUM);
		}

		public bool Equals(SWVersion obj)
		{
			return SWVersion.CheckVersions(this, obj, FieldEnums.MAJOR_ENUM) == 0;
		}

		public override bool Equals(object obj)
		{
			SWVersion ver = (SWVersion)obj;
			return SWVersion.CheckVersions(this, ver, FieldEnums.MAJOR_ENUM) == 0;
		}

		private static int CheckVersions(SWVersion ver1, SWVersion ver2, FieldEnums state)
		{
			uint[] array = null;
			uint[] array2 = null;
			int result;
			try
			{
				array = ver1.GetArray();
				try
				{
					array2 = ver2.GetArray();
				}
				catch (NullReferenceException var_3_1A)
				{
					result = 1;
					return result;
				}
			}
			catch (NullReferenceException var_4_25)
			{
				try
				{
					array2 = ver2.GetArray();
					result = -1;
					return result;
				}
				catch (NullReferenceException var_5_35)
				{
					result = 0;
					return result;
				}
			}
			uint num = array[(int)state];
			uint num2 = array2[(int)state];
			int num3;
			if (num > num2)
			{
				num3 = 1;
			}
			else if (num < num2)
			{
				num3 = -1;
			}
			else if (state < FieldEnums.BETA_ENUM)
			{
				num3 = SWVersion.CheckVersions(ver1, ver2, state + 1);
			}
			else
			{
				num3 = 0;
			}
			result = num3;
			return result;
		}

		private uint[] GetArray()
		{
			return new uint[]
			{
				this.Major,
				this.Minor,
				this.Build,
				this.Revision,
				(this.CheckSum.HasValue ? this.CheckSum : new uint?(0u)).Value,
				this.Beta ? 0u : 1u
			};
		}

		private string GetTrimmedVersion(string verStr)
		{
			string text = "";
			if (verStr == null)
			{
				throw new NullReferenceException("No string to trim!");
			}
			string[] array = verStr.Split(new string[]
			{
				"."
			}, StringSplitOptions.None);
			int num = array.Length;
			for (int i = num - 1; i >= 0; i--)
			{
				try
				{
					string text2 = array[i];
					int num2 = int.Parse(text2);
					if (num2 != 0)
					{
						break;
					}
					array[i] = null;
				}
				catch (Exception var_6_70)
				{
				}
			}
			for (int i = 0; i < num; i++)
			{
				string text2 = array[i];
				if (text2 == null)
				{
					break;
				}
				text = text + ((i > 0) ? "." : "") + array[i];
			}
			return text;
		}

		private string GetSWVersionAsString(FieldEnums upToVersionPart, int[] padding, VersionOptions options)
		{
			string text = "";
			uint[] array = this.GetArray();
			bool flag = (options & VersionOptions.ADD_CHECKSUM_DECIMAL) == VersionOptions.ADD_CHECKSUM_DECIMAL || (options & VersionOptions.ADD_CHECKSUM_HEX) == VersionOptions.ADD_CHECKSUM_HEX;
			FieldEnums fieldEnums = ((options & VersionOptions.ADD_BETA_INDICATION) == VersionOptions.ADD_BETA_INDICATION) ? FieldEnums.BETA_ENUM : (flag ? FieldEnums.CHECKSUM_ENUM : upToVersionPart);
			int num = (int)(fieldEnums + 1);
			int i = 0;
			while (i < num)
			{
				uint num2 = array[i];
				switch (i)
				{
				case 0:
				case 1:
				case 2:
				case 3:
					if (i <= (int)upToVersionPart)
					{
						string text2 = "";
						int num3 = (padding != null && i < padding.Length) ? padding[i] : 0;
						string text3 = num2.ToString();
						int num4 = num3 - text3.Length;
						for (int j = 0; j < num4; j++)
						{
							text2 += "0";
						}
						text2 += text3;
						text += ((i > 0 && i < 4) ? "." : "");
						text += text2;
						if (i == 3 && (options & VersionOptions.TRIM_TRAIL) == VersionOptions.TRIM_TRAIL)
						{
							text = this.GetTrimmedVersion(text);
						}
					}
					break;
				case 4:
					if ((options & VersionOptions.ADD_CHECKSUM_DECIMAL) == VersionOptions.ADD_CHECKSUM_DECIMAL || (options & VersionOptions.ADD_CHECKSUM_HEX) == VersionOptions.ADD_CHECKSUM_HEX)
					{
						text += " (";
						if ((options & VersionOptions.ADD_CHECKSUM_DECIMAL) == VersionOptions.ADD_CHECKSUM_DECIMAL)
						{
							text += num2.ToString();
						}
						else
						{
							string checkSumHEX = this.CheckSumHEX;
							text = text + "0x" + (string.IsNullOrEmpty(checkSumHEX) ? "0" : checkSumHEX);
						}
						text += ")";
					}
					break;
				case 5:
					if ((options & VersionOptions.ADD_BETA_INDICATION) == VersionOptions.ADD_BETA_INDICATION)
					{
						text += ((num2 == 0u) ? " (Beta)" : "");
					}
					break;
				}
				IL_1DA:
				i++;
				continue;
				goto IL_1DA;
			}
			return text;
		}
	}
}
