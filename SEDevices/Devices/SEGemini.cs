using SEDevices.Data;
using SEDevices.Records.Parameter;
using SEDevices.Records.Status;
using SEDevices.Upgrade.Data;
using SEDevices.Upgrade.DeviceVersions.Base;
using SEDevices.Upgrade.DeviceVersions.Compatibility;
using SEDevices.Upgrade.DeviceVersions.Info;
using SEProtocol.SDP;
using SEProtocol.SDP.Packets.Base;
using SEUtils.Common;
using System;
using System.Collections.Generic;
using System.Threading;

namespace SEDevices.Devices
{
	public class SEGemini : SECommDevice
	{
		protected SWVersion _deviceSWVersionPwr;

		private uint _dspIndex = 0u;

		private uint _combiCardIndex = 0u;

		public SWVersion DeviceSWVersionPower
		{
			get
			{
				return this._deviceSWVersionPwr;
			}
		}

		public override uint DeviceMaxDataLength
		{
			get
			{
				return 480u;
			}
		}

		public override uint DeviceResetDelay
		{
			get
			{
				return 200u;
			}
		}

		public uint DSPIndex
		{
			get
			{
				return this._dspIndex;
			}
		}

		public uint CombiCardIndex
		{
			get
			{
				return this._combiCardIndex;
			}
		}

		public SEGemini(uint address, uint dspIndex, uint combiCardIndex, CommManager commManager, object owner, SWVersion version) : base(commManager, owner, version)
		{
			this._address = address;
			this._combiCardIndex = combiCardIndex;
			this._dspIndex = dspIndex;
			base.DeviceDefaultTimeout = 10000u;
			if (version == null)
			{
				this._deviceSWVersion = this.Command_Device_Version();
			}
		}

		protected override Type GetParamType(ushort index)
		{
			return DBParameterVersion.Instance.GetTypeOfParamGemini(index);
		}

		public static SwitchStates GetSwitchStateByIndex(int switchStateIndex)
		{
			SwitchStates result = SwitchStates.CONNECTED_DONT_KNOW;
			if (switchStateIndex <= 20)
			{
				if (switchStateIndex != 0 && switchStateIndex != 8)
				{
					if (switchStateIndex != 20)
					{
						return result;
					}
					goto IL_3A;
				}
			}
			else if (switchStateIndex <= 32)
			{
				if (switchStateIndex == 23)
				{
					goto IL_3A;
				}
				if (switchStateIndex != 32)
				{
					return result;
				}
			}
			else
			{
				if (switchStateIndex == 40)
				{
					result = SwitchStates.DISCONNECTED_DONT_KNOW;
					return result;
				}
				if (switchStateIndex != 43)
				{
					return result;
				}
				result = SwitchStates.CONNECTED_DONT_KNOW;
				return result;
			}
			result = SwitchStates.DISCONNECTED;
			return result;
			IL_3A:
			result = SwitchStates.CONNECTED;
			return result;
		}

		public static bool IsTestActive(ActiveTests test, int activeTests)
		{
			return (activeTests & (int)test) > 0;
		}

		public static int[] GetActiveStringIndices(int activeStrings)
		{
			List<int> list = null;
			int[] result;
			if (activeStrings <= 0)
			{
				result = null;
			}
			else
			{
				string text = Convert.ToString(activeStrings, 2);
				int length = text.Length;
				for (int i = 0; i < length; i++)
				{
					int num = (int)Math.Pow(2.0, (double)i);
					bool flag = (num & activeStrings) > 0;
					if (flag)
					{
						if (list == null)
						{
							list = new List<int>(0);
						}
						list.Add(i);
					}
				}
				result = ((list != null) ? list.ToArray() : null);
			}
			return result;
		}

		public static int[] GetNonActiveStringIndices(int activeStrings)
		{
			List<int> list = null;
			int[] result;
			if (activeStrings < 0)
			{
				result = null;
			}
			else
			{
				int num = 16;
				for (int i = 0; i < num; i++)
				{
					int num2 = 1 << i;
					if ((num2 & activeStrings) <= 0)
					{
						if (list == null)
						{
							list = new List<int>(0);
						}
						list.Add(i);
					}
				}
				if (activeStrings == 65535 && list == null)
				{
					list = new List<int>(0);
				}
				result = ((list != null) ? list.ToArray() : null);
			}
			return result;
		}

		public static int[] GetAssembledStringIndices(int assembledStrings)
		{
			List<int> list = null;
			int[] result;
			if (assembledStrings <= 0)
			{
				result = null;
			}
			else
			{
				string text = Convert.ToString(assembledStrings, 2);
				int length = text.Length;
				for (int i = 0; i < length; i++)
				{
					int num = (int)Math.Pow(2.0, (double)i);
					bool flag = (num & assembledStrings) > 0;
					if (flag)
					{
						if (list == null)
						{
							list = new List<int>(0);
						}
						list.Add(i);
					}
				}
				result = ((list != null) ? list.ToArray() : null);
			}
			return result;
		}

		public static bool IsStringActive(StringIndex index, int activeStrings)
		{
			string text = Convert.ToString(activeStrings, 2);
			int length = text.Length;
			bool result;
			if (index == StringIndex.COUNT || index >= (StringIndex)length)
			{
				result = false;
			}
			else
			{
				int num = (int)Math.Pow(2.0, (double)index);
				result = ((num & activeStrings) > 0);
			}
			return result;
		}

		public static bool IsStringAssembled(StringIndex index, int assembledStrings)
		{
			string text = Convert.ToString(assembledStrings, 2);
			int length = text.Length;
			bool result;
			if (index == StringIndex.COUNT || index >= (StringIndex)length)
			{
				result = false;
			}
			else
			{
				int num = (int)Math.Pow(2.0, (double)index);
				result = ((num & assembledStrings) > 0);
			}
			return result;
		}

		public static uint GetDSPAddressOffset(int combiIndex, int dspIndex)
		{
			string str = Convert.ToString(combiIndex, 2);
			string str2 = Convert.ToString(dspIndex, 2);
			string value = str + str2;
			uint num = Convert.ToUInt32(value, 2);
			return num << 20;
		}

		public static int GetCombiIndexFromDSPAddress(uint dspAddressOffset)
		{
			int value = (int)dspAddressOffset >> 20;
			string text = Convert.ToString(value, 2);
			int length = text.Length;
			string value2 = text.Substring(0, length - 1);
			return Convert.ToInt32(value2, 2);
		}

		public static int GetDSPIndexFromDSPAddress(uint dspAddressOffset)
		{
			int value = (int)dspAddressOffset >> 20;
			string text = Convert.ToString(value, 2);
			int length = text.Length;
			string value2 = text.Substring(length - 1, 1);
			return Convert.ToInt32(value2, 2);
		}

		public override SEParam Command_Params_Get(ushort index)
		{
			return this.Command_Params_Get((GeminiParams)index);
		}

		public SEParam Command_Params_Get(GeminiParams param)
		{
			SEParam sEParam;
			if (param == GeminiParams.ID)
			{
				sEParam = base.Command_Params_Get_Real((ushort)param);
			}
			else
			{
				int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.GEMINI, base.DeviceSWVersion);
				sEParam = base.Command_Params_Get_Real((ushort)paramRealIndex);
				if (sEParam != null)
				{
					sEParam.Index = (ushort)param;
				}
			}
			return sEParam;
		}

		public override SEParam[] Command_Params_Get(ushort[] indices)
		{
			if (indices == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (indices.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			List<GeminiParams> list = new List<GeminiParams>(0);
			int num = indices.Length;
			for (int i = 0; i < num; i++)
			{
				list.Add((GeminiParams)indices[i]);
			}
			return this.Command_Params_Get(list.ToArray());
		}

		public SEParam[] Command_Params_Get(GeminiParams[] paramList)
		{
			if (paramList == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (paramList.Length <= 0)
			{
				throw new Exception("Parameter List to set is empty!");
			}
			int num = paramList.Length;
			List<ushort> list = new List<ushort>(0);
			for (int i = 0; i < num; i++)
			{
				GeminiParams geminiParams = paramList[i];
				int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)geminiParams, DeviceType.GEMINI, base.DeviceSWVersion);
				list.Add((ushort)paramRealIndex);
			}
			SEParam[] array = base.Command_Params_Get_Real(list.ToArray());
			int num2 = array.Length;
			for (int i = 0; i < num2; i++)
			{
				SEParam sEParam = array[i];
				sEParam.Index = (ushort)paramList[i];
			}
			return array;
		}

		public override string Command_Params_Get_Name(ushort index)
		{
			return this.Command_Params_Get_Name((GeminiParams)index);
		}

		public string Command_Params_Get_Name(GeminiParams param)
		{
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.GEMINI, base.DeviceSWVersion);
			return base.Command_Params_Get_Name_Real((ushort)paramRealIndex);
		}

		public override SEParamInfo Command_Params_Get_Info(ushort index)
		{
			return this.Command_Params_Get_Info((GeminiParams)index);
		}

		public SEParamInfo Command_Params_Get_Info(GeminiParams param)
		{
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex((ushort)param, DeviceType.GEMINI, base.DeviceSWVersion);
			return base.Command_Params_Get_Info_Real((ushort)paramRealIndex);
		}

		public override int Command_Params_Get_Count()
		{
			return base.Command_Params_Get_Count_Real();
		}

		public override void Command_Params_Set(SEParam param)
		{
			if (param == null)
			{
				throw new NullReferenceException("No Parameter to send!");
			}
			int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex(param.Index, DeviceType.GEMINI, base.DeviceSWVersion);
			if (paramRealIndex > -1)
			{
				bool flag = DBParameterVersion.Instance.IsResetRequiredForParam(param.Index, DeviceType.GEMINI, base.DeviceSWVersion);
				param.Index = (ushort)paramRealIndex;
				base.Command_Params_Set_Real(param);
				if (flag)
				{
					this.Command_Device_Reset(0u);
					Thread.Sleep((int)(1000u + this.DeviceResetDelay));
				}
			}
		}

		public override void Command_Params_Set(SEParam[] paramList)
		{
			if (paramList == null)
			{
				throw new NullReferenceException("Parameter List to set is null!");
			}
			if (paramList.Length > 0)
			{
				int num = paramList.Length;
				for (int i = 0; i < num; i++)
				{
					SEParam sEParam = paramList[i];
					if (sEParam != null)
					{
						int paramRealIndex = DBParameterVersion.Instance.GetParamRealIndex(sEParam.Index, DeviceType.GEMINI, base.DeviceSWVersion);
						sEParam.Index = (ushort)paramRealIndex;
					}
				}
				base.Command_Params_Set_Real(paramList);
				this.Command_Device_Reset(0u);
				Thread.Sleep((int)(1000u + this.DeviceResetDelay));
			}
		}

		public void Command_Gemini_SetCountry(uint countryIndex)
		{
			Packet command = base.BuildCommand(527, new List<object>(0)
			{
				countryIndex
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public override SWVersion Command_Device_Version()
		{
			SWVersion sWVersion = new SWVersion();
			SEParam sEParam = base.Command_Params_Get_Real(1);
			SEParam sEParam2 = base.Command_Params_Get_Real(2);
			SEParam sEParam3 = base.Command_Params_Get_Real(3);
			string text = (sEParam != null) ? sEParam.GetParamAsString() : null;
			string text2 = (sEParam2 != null) ? sEParam2.GetParamAsString() : null;
			string text3 = (sEParam3 != null) ? sEParam3.GetParamAsString() : null;
			if (text != null)
			{
				sWVersion.Major = uint.Parse(text);
			}
			if (text2 != null)
			{
				sWVersion.Minor = uint.Parse(text2);
			}
			if (text3 != null)
			{
				sWVersion.CheckSum = new uint?(uint.Parse(text3));
			}
			return sWVersion;
		}

		public override void Command_Device_Reset(uint delay)
		{
			base.Command_Device_Reset(0u);
		}

		public void Command_Gemini_Force_Telemetry(ForceTelemetryType type)
		{
			this.Command_Gemini_Force_Telemetry((ushort)type);
		}

		public void Command_Gemini_Relay_Connect(StringIndex stringIndex, RelaySide side)
		{
			this.Command_Gemini_Relay_Connect((ushort)stringIndex, (ushort)side);
		}

		public void Command_Gemini_Relay_Disconnect(StringIndex stringIndex, RelaySide side)
		{
			this.Command_Gemini_Relay_Disconnect((ushort)stringIndex, (ushort)side);
		}

		public void Command_Gemini_Relay_Connect(StringIndex[] indices)
		{
			if (indices != null && indices.Length > 0)
			{
				int num = indices.Length;
				List<ushort> list = new List<ushort>(0);
				for (int i = 0; i < num; i++)
				{
					list.Add((ushort)indices[i]);
				}
				this.Command_Gemini_Relay_Connect(list.ToArray());
			}
		}

		public void Command_Gemini_Relay_Disconnect(StringIndex[] indices)
		{
			if (indices != null && indices.Length > 0)
			{
				int num = indices.Length;
				List<ushort> list = new List<ushort>(0);
				for (int i = 0; i < num; i++)
				{
					list.Add((ushort)indices[i]);
				}
				this.Command_Gemini_Relay_Disconnect(list.ToArray());
			}
		}

		public void Command_Gemini_Monitoring_Resume()
		{
			this.Command_Gemini_Pause_Monitoring(1, 0u);
		}

		public void Command_Gemini_Monitoring_Pause(uint timeoutToResume)
		{
			this.Command_Gemini_Pause_Monitoring(0, timeoutToResume);
		}

		public override void UpgradeState_VerifyCompliance(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, List<DeviceVersionInfo> deviceUpgradeVersionList, List<DeviceCompatbilityItem> deviceCompatabilityList)
		{
			if (stateData != null && deviceVersionList != null && deviceUpgradeVersionList != null && deviceCompatabilityList != null)
			{
				GeminiVersionInfo geminiVersionInfo = (GeminiVersionInfo)deviceVersionList[0];
				GeminiVersionInfo geminiVersion = (GeminiVersionInfo)deviceUpgradeVersionList[0];
				List<GeminiCompatibilityItem> list = new List<GeminiCompatibilityItem>(0);
				int count = deviceCompatabilityList.Count;
				for (int i = 0; i < count; i++)
				{
					list.Add((GeminiCompatibilityItem)deviceCompatabilityList[i]);
				}
				if (geminiVersionInfo.Equals(geminiVersion))
				{
					stateData.UpgradeState = UpgradeState.Finished;
				}
				else
				{
					GeminiCompatibilityItem geminiCompatibilityItem = null;
					foreach (GeminiCompatibilityItem current in list)
					{
						foreach (GeminiVersionInfo current2 in current.GeminiVersions)
						{
							if (current2.Equals(geminiVersionInfo))
							{
								geminiCompatibilityItem = current;
								break;
							}
						}
						if (geminiCompatibilityItem != null)
						{
							break;
						}
					}
					if (geminiCompatibilityItem == null)
					{
						throw new Exception(string.Format("Upgrade file is not compatible to Gemini version - ({0})", geminiVersionInfo.ToString()));
					}
					stateData.GeminiCompatibiltyItem = geminiCompatibilityItem;
					stateData.OriginalGeminiVersion = geminiVersionInfo;
					stateData.UpgradeState = UpgradeState.SaveParamaeters;
				}
			}
		}

		public override void UpgradeState_SaveParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, uint? sufVersion)
		{
			if (stateData != null && deviceVersionList != null)
			{
				GeminiVersionInfo geminiVersionInfo = (GeminiVersionInfo)deviceVersionList[0];
				if (!geminiVersionInfo.Equals(stateData.OriginalGeminiVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				stateData.SavedParameters = new List<ParamaeterPair>();
				foreach (int current in stateData.GeminiCompatibiltyItem.RestoreParams)
				{
					int num = current;
					if (!sufVersion.HasValue)
					{
						ushort? paramVirtualIndex = DBParameterVersion.Instance.GetParamVirtualIndex(num, DeviceType.GEMINI, base.DeviceSWVersion);
						ushort? num2 = paramVirtualIndex;
						if ((num2.HasValue ? new int?((int)num2.GetValueOrDefault()) : null).HasValue)
						{
							num = (int)paramVirtualIndex.Value;
						}
					}
					SEParam sEParam = this.Command_Params_Get((GeminiParams)num);
					string paramAsString = sEParam.GetParamAsString();
					ParamaeterPair paramaeterPair = new ParamaeterPair();
					paramaeterPair.Index = num;
					paramaeterPair.Value = paramAsString;
					stateData.SavedParameters.Add(paramaeterPair);
				}
				stateData.UpgradeState = UpgradeState.UpgradeSoftware;
			}
		}

		public override void UpgradeState_UpgradeSoftwareStatus(UpgradeStateData stateData, UpgradeXmlData upgradeXmlData)
		{
			if (stateData != null && upgradeXmlData != null)
			{
				uint checksum = 0u;
				if (upgradeXmlData.UpgradeData.DataChecksum.HasValue)
				{
					checksum = upgradeXmlData.UpgradeData.DataChecksum.Value;
				}
				base.DoUpgrade(upgradeXmlData.UpgradeData.Data, upgradeXmlData.UpgradeData.Data.Length, 0u, checksum, 0, base.UpgradeNotifier);
				stateData.UpgradeState = UpgradeState.VerifyVersionAfterUpgrade;
			}
		}

		public override void UpgradeState_VerifyVersionAfterUpgrade(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList, SECommDevice extraCommDevice)
		{
			if (stateData != null && deviceVersionList != null)
			{
				GeminiVersionInfo geminiVersionInfo = (GeminiVersionInfo)deviceVersionList[0];
				GeminiVersionInfo geminiVersion = (GeminiVersionInfo)deviceVersionList[1];
				SEPortia sEPortia = (SEPortia)extraCommDevice;
				sEPortia.Command_Device_Reset();
				int deviceDefaultRetries = (int)base.DeviceDefaultRetries;
				base.DeviceDefaultRetries = 50u;
				this._deviceSWVersion = this.Command_Device_Version();
				SWVersion deviceSWVersion = base.DeviceSWVersion;
				geminiVersionInfo.MajorVersion = deviceSWVersion.Major;
				geminiVersionInfo.MinorVersion = deviceSWVersion.Minor;
				if (!geminiVersionInfo.Equals(geminiVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				else
				{
					base.DeviceDefaultRetries = (uint)deviceDefaultRetries;
					stateData.UpgradeState = UpgradeState.ConfigureParamaeters;
				}
			}
		}

		public override void UpgradeState_ConfigureParameters(UpgradeStateData stateData, List<DeviceVersionInfo> deviceVersionList)
		{
			if (stateData != null && deviceVersionList != null)
			{
				GeminiVersionInfo geminiVersionInfo = (GeminiVersionInfo)deviceVersionList[0];
				GeminiVersionInfo geminiVersion = (GeminiVersionInfo)deviceVersionList[1];
				if (!geminiVersionInfo.Equals(geminiVersion))
				{
					stateData.UpgradeState = UpgradeState.VerifyCompliance;
				}
				else
				{
					List<SEParam> list = new List<SEParam>(0);
					foreach (ParamaeterPair current in stateData.SavedParameters)
					{
						ushort num = (ushort)current.Index;
						Type typeOfParamGemini = DBParameterVersion.Instance.GetTypeOfParamGemini(num);
						ParamType paramTypeByType = SEParam.GetParamTypeByType(typeOfParamGemini);
						SEParam item = null;
						switch (paramTypeByType)
						{
						case ParamType.UINT32:
							item = new SEParam(num, uint.Parse(current.Value));
							break;
						case ParamType.FLOAT:
							item = new SEParam(num, float.Parse(current.Value));
							break;
						case ParamType.INT:
							item = new SEParam(num, int.Parse(current.Value));
							break;
						case ParamType.STRING:
							item = new SEParam(num, current.Value);
							break;
						}
						list.Add(item);
					}
					if (stateData.GeminiCompatibiltyItem != null && stateData.GeminiCompatibiltyItem.ParamatersToSet != null)
					{
						foreach (ParamaeterPair current in stateData.GeminiCompatibiltyItem.ParamatersToSet)
						{
							ushort num = (ushort)current.Index;
							Type typeOfParamGemini = DBParameterVersion.Instance.GetTypeOfParamGemini(num);
							ParamType paramTypeByType = SEParam.GetParamTypeByType(typeOfParamGemini);
							SEParam item = null;
							switch (paramTypeByType)
							{
							case ParamType.UINT32:
								item = new SEParam(num, uint.Parse(current.Value));
								break;
							case ParamType.FLOAT:
								item = new SEParam(num, float.Parse(current.Value));
								break;
							case ParamType.INT:
								item = new SEParam(num, int.Parse(current.Value));
								break;
							case ParamType.STRING:
								item = new SEParam(num, current.Value);
								break;
							}
							list.Add(item);
						}
					}
					this.Command_Params_Set(list.ToArray());
					stateData.UpgradeState = UpgradeState.Finished;
				}
			}
		}

		public override void UpgradeState_Finished(UpgradeStateData stateData, DeviceVersionInfo deviceVersion, DeviceVersionInfo upgradeVersion)
		{
			GeminiVersionInfo geminiVersionInfo = (GeminiVersionInfo)deviceVersion;
			GeminiVersionInfo geminiVersion = (GeminiVersionInfo)upgradeVersion;
			if (!geminiVersionInfo.Equals(geminiVersion))
			{
				stateData.UpgradeState = UpgradeState.VerifyCompliance;
			}
		}

		private void Command_Gemini_Pause_Monitoring(ushort resume, uint timeout)
		{
			Packet command = base.BuildCommand(2816, new List<object>(0)
			{
				resume,
				timeout
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		private void Command_Gemini_Force_Telemetry(ushort type)
		{
			Packet command = base.BuildCommand(2820, new List<object>(0)
			{
				type
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		private void Command_Gemini_Relay_Connect(ushort stringIndex, ushort relaySide)
		{
			Packet command = base.BuildCommand(2840, new List<object>(0)
			{
				stringIndex,
				relaySide
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		private void Command_Gemini_Relay_Disconnect(ushort stringIndex, ushort relaySide)
		{
			Packet command = base.BuildCommand(2841, new List<object>(0)
			{
				stringIndex,
				relaySide
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		private void Command_Gemini_Relay_Connect(ushort[] stringIndices)
		{
			if (stringIndices != null && stringIndices.Length > 0)
			{
				List<object> list = new List<object>(0);
				int num = stringIndices.Length;
				for (int i = 0; i < num; i++)
				{
					list.Add(stringIndices[i]);
				}
				Packet command = base.BuildCommand(2821, list);
				base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			}
		}

		private void Command_Gemini_Relay_Disconnect(ushort[] stringIndices)
		{
			if (stringIndices != null && stringIndices.Length > 0)
			{
				List<object> list = new List<object>(0);
				int num = stringIndices.Length;
				for (int i = 0; i < num; i++)
				{
					list.Add(stringIndices[i]);
				}
				Packet command = base.BuildCommand(2822, list);
				base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			}
		}

		public void Command_Gemini_Relay_Connect_All()
		{
			Packet command = base.BuildCommand(2823, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public void Command_Gemini_Relay_Disconnect_All()
		{
			Packet command = base.BuildCommand(2824, null);
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public GeminiTestResult Command_Gemini_Execute_RCD_Test()
		{
			GeminiTestResult result = GeminiTestResult.NOT_PERFORMED;
			Packet command = base.BuildCommand(2825, null);
			Packet packet = base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				try
				{
					result = (GeminiTestResult)packet.RetrieveUInt16();
				}
				catch (Exception var_3_3F)
				{
				}
			}
			return result;
		}

		public GeminiTestResult Command_Gemini_Execute_Relays_Test(ushort stringIndex, ushort hs)
		{
			GeminiTestResult result = GeminiTestResult.NOT_PERFORMED;
			Packet command = base.BuildCommand(2826, new List<object>
			{
				stringIndex,
				hs
			});
			Packet packet = base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				try
				{
					result = (GeminiTestResult)packet.RetrieveUInt16();
				}
				catch (Exception var_4_5F)
				{
				}
			}
			return result;
		}

		public void Command_Gemini_Execute_RCD_Calibration(ushort stringIndex)
		{
			Packet command = base.BuildCommand(2818, new List<object>(0)
			{
				stringIndex
			});
			base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
		}

		public SETelemetryStatusCombiString Command_Gemini_Get_StringStatus(ushort stringIndex)
		{
			SETelemetryStatusCombiString[] array = this.Command_Gemini_Get_StringStatus(new ushort[]
			{
				stringIndex
			});
			return (array != null && array.Length > 0) ? array[0] : null;
		}

		public SETelemetryStatusCombiString[] Command_Gemini_Get_StringStatus(ushort[] stringIndices)
		{
			List<SETelemetryStatusCombiString> list = null;
			List<object> list2 = new List<object>(0);
			int num = (stringIndices != null) ? stringIndices.Length : 0;
			for (int i = 0; i < num; i++)
			{
				list2.Add(stringIndices[i]);
			}
			Packet command = base.BuildCommand(2832, list2);
			Packet packet = base.Transcieve(command, ProtocolGeneralResponses.PROT_RESP_ACK, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				list = new List<SETelemetryStatusCombiString>(0);
				for (int i = 0; i < num; i++)
				{
					list.Add(new SETelemetryStatusCombiString
					{
						StringID = (int)packet.RetrieveInt16(),
						GeminiStringId = (int)packet.RetrieveInt16(),
						SwitchesState = packet.RetrieveUInt16(),
						RCD_I = packet.RetrieveSingle(),
						String_I = packet.RetrieveSingle(),
						String_V = packet.RetrieveSingle(),
						HS_V = packet.RetrieveSingle(),
						LS_V = packet.RetrieveSingle()
					});
				}
			}
			return (list != null) ? list.ToArray() : null;
		}

		public SETelemetryStatusCombiString Command_Gemini_Get_StringStatus_Unified(ushort stringIndex)
		{
			SETelemetryStatusCombiString[] array = this.Command_Gemini_Get_StringStatus_Unified(new ushort[]
			{
				stringIndex
			});
			return (array != null && array.Length > 0) ? array[0] : null;
		}

		public SETelemetryStatusCombiString[] Command_Gemini_Get_StringStatus_Unified(ushort[] stringIndices)
		{
			List<SETelemetryStatusCombiString> list = null;
			List<object> list2 = new List<object>(0);
			int num = (stringIndices != null) ? stringIndices.Length : 0;
			for (int i = 0; i < num; i++)
			{
				list2.Add(stringIndices[i]);
			}
			Packet command = base.BuildCommand(2837, list2);
			Packet packet = base.Transcieve(command, 2948, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				list = new List<SETelemetryStatusCombiString>(0);
				for (int i = 0; i < num; i++)
				{
					list.Add(new SETelemetryStatusCombiString
					{
						StringID = (int)packet.RetrieveInt16(),
						GeminiStringId = (int)packet.RetrieveInt16(),
						SwitchesState = packet.RetrieveUInt16(),
						RCD_I = packet.RetrieveSingle(),
						String_I = packet.RetrieveSingle(),
						String_V = packet.RetrieveSingle(),
						HS_V = packet.RetrieveSingle(),
						LS_V = packet.RetrieveSingle(),
						TrippedTests = packet.RetrieveUInt16()
					});
				}
			}
			return (list != null) ? list.ToArray() : null;
		}

		public SETelemetryStatusCombi Command_Gemini_Get_CombiStatus()
		{
			SETelemetryStatusCombi sETelemetryStatusCombi = null;
			Packet command = base.BuildCommand(2833, null);
			Packet packet = base.Transcieve(command, 2946, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				sETelemetryStatusCombi = new SETelemetryStatusCombi();
				sETelemetryStatusCombi.ActiveStrings = packet.RetrieveUInt16();
				sETelemetryStatusCombi.ViInv = packet.RetrieveSingle();
				sETelemetryStatusCombi.Total_I = packet.RetrieveSingle();
				sETelemetryStatusCombi.Temperature = packet.RetrieveSingle();
				sETelemetryStatusCombi.VSupply = packet.RetrieveSingle();
				sETelemetryStatusCombi.VCommon = packet.RetrieveSingle();
				sETelemetryStatusCombi.VRef1 = packet.RetrieveSingle();
				sETelemetryStatusCombi.VRef2 = packet.RetrieveSingle();
			}
			return sETelemetryStatusCombi;
		}

		public SETelemetryStatusCombi Command_Gemini_Get_CombiStatus_Unified()
		{
			SETelemetryStatusCombi sETelemetryStatusCombi = null;
			Packet command = base.BuildCommand(2838, null);
			Packet packet = base.Transcieve(command, 2949, base.DeviceDefaultTimeout, base.DeviceDefaultRetries);
			if (packet != null)
			{
				sETelemetryStatusCombi = new SETelemetryStatusCombi();
				sETelemetryStatusCombi.ActiveStrings = packet.RetrieveUInt16();
				sETelemetryStatusCombi.ViInv = packet.RetrieveSingle();
				sETelemetryStatusCombi.Total_I = packet.RetrieveSingle();
				sETelemetryStatusCombi.Temperature = packet.RetrieveSingle();
				sETelemetryStatusCombi.VSupply = packet.RetrieveSingle();
				sETelemetryStatusCombi.VCommon = packet.RetrieveSingle();
				sETelemetryStatusCombi.VRef1 = packet.RetrieveSingle();
				sETelemetryStatusCombi.VRef2 = packet.RetrieveSingle();
			}
			return sETelemetryStatusCombi;
		}
	}
}
