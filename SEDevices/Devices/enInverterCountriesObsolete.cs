using System;

namespace SEDevices.Devices
{
	public enum enInverterCountriesObsolete
	{
		General,
		Australia,
		France,
		Germany,
		GreeceCont,
		GreeceIsls,
		Israel,
		Italy,
		Spain,
		UK,
		USAuto,
		US208V,
		US240V,
		US208VNoNeutral,
		US240VNoNeutral
	}
}
