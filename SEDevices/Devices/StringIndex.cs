using System;

namespace SEDevices.Devices
{
	public enum StringIndex
	{
		NONE = -1,
		STRING_0,
		STRING_1,
		STRING_2,
		STRING_3,
		STRING_4,
		STRING_5,
		STRING_6,
		STRING_7,
		STRING_8,
		STRING_9,
		STRING_10,
		STRING_11,
		STRING_12,
		STRING_13,
		STRING_14,
		STRING_15,
		COUNT
	}
}
