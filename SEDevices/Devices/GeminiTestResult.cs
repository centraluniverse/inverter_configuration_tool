using System;

namespace SEDevices.Devices
{
	public enum GeminiTestResult
	{
		NOT_PERFORMED,
		PASS,
		FAIL
	}
}
