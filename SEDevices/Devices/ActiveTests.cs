using System;

namespace SEDevices.Devices
{
	public enum ActiveTests
	{
		RCD_TEST,
		HS_RELAY_TEST,
		LS_RELAY_TEST,
		RCD_MON_RELAYS_CONNECTED,
		RCD_MON_RELAYS_DISCONNECTED,
		RELAYS_MON,
		HS_RELAY_MON,
		LS_RELAY_MON,
		COUNT
	}
}
