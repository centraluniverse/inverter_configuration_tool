using System;

namespace SEDevices.Devices
{
	public enum enVenusConversionMode
	{
		MODE_FIXED_VOLTAGE_CONTROL,
		MODE_DUTY_CYCLE_OPTIMIZED,
		MODE_U_I_MODE_MASTER,
		MODE_I_U_MODE_SLAVE,
		MODE_NO_POWER
	}
}
