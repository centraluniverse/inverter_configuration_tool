using SEDevices.Records.Status;
using System;

namespace SEDevices.Records.Extra
{
	public class SETelemetryPrivlidgedEvent
	{
		public SETelemetryHeader Header = new SETelemetryHeader();

		public uint TimeStamp;

		public EVenusEventCodes Event = EVenusEventCodes.VENUSLOG_NO_ERROR;

		public ushort NumOfEvents;
	}
}
