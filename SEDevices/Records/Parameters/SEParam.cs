using SEUtils.Common;
using System;

namespace SEDevices.Records.Parameter
{
	public class SEParam
	{
		public const int DATA_LENGTH_PARAM_NON_STRING = 6;

		private uint _value = 0u;

		private string _strValue = null;

		private float _rate = 1f;

		private float _a = 1f;

		private float _b = 0f;

		private short _paramType;

		private ushort _index;

		public uint ValueUInt32
		{
			get
			{
				return this._value;
			}
		}

		public float ValueFloat
		{
			get
			{
				return Utils.UInt32AsFloat(this._value);
			}
			set
			{
				this._value = Utils.FloatAsUInt32(value);
			}
		}

		public int ValueInt32
		{
			get
			{
				return Utils.UInt32AsInt32(this._value);
			}
			set
			{
				this._value = Utils.Int32AsUInt32(value);
			}
		}

		public string ValueString
		{
			get
			{
				return this._strValue;
			}
			set
			{
				this._strValue = value;
			}
		}

		public double ValueConverted
		{
			get
			{
				double num = 0.0;
				switch (this.ParameterType)
				{
				case 0:
					num = Convert.ToDouble(this.ValueUInt32);
					break;
				case 1:
					num = Convert.ToDouble(this.ValueFloat);
					break;
				case 2:
					num = Convert.ToDouble(this.ValueInt32);
					break;
				}
				if (num > 0.0)
				{
					num = (double)this._a * (num / (double)this._rate) + (double)this._b;
					string s = string.Format("{0:0.00}", num);
					num = double.Parse(s);
				}
				return num;
			}
		}

		public short ParameterType
		{
			get
			{
				return this._paramType;
			}
		}

		public ushort Index
		{
			get
			{
				return this._index;
			}
			set
			{
				this._index = value;
			}
		}

		public SEParam(ushort index, uint value)
		{
			this._index = index;
			this._value = value;
			this._strValue = null;
			this._paramType = 0;
		}

		public SEParam(ushort index, float value)
		{
			this._index = index;
			this._value = Utils.FloatAsUInt32(value);
			this._strValue = null;
			this._paramType = 1;
		}

		public SEParam(ushort index, int value)
		{
			this._index = index;
			this._value = Utils.Int32AsUInt32(value);
			this._strValue = null;
			this._paramType = 2;
		}

		public SEParam(ushort index, string value)
		{
			this._index = index;
			this._value = 0u;
			this._strValue = value;
			this._paramType = 3;
		}

		public SEParam(ushort index, object value, Type type)
		{
			this._index = index;
			this._strValue = null;
			if (type.Equals(typeof(uint)))
			{
				this._value = uint.Parse(value.ToString());
			}
			if (type.Equals(typeof(float)))
			{
				this._value = Utils.FloatAsUInt32(float.Parse(value.ToString()));
			}
			if (type.Equals(typeof(int)))
			{
				this._value = Utils.Int32AsUInt32(int.Parse(value.ToString()));
			}
			if (type.Equals(typeof(string)))
			{
				this._value = 0u;
				this._strValue = value.ToString();
			}
			this._paramType = (short)SEParam.GetParamTypeByType(type);
		}

		public SEParam Copy()
		{
			SEParam result = null;
			switch (this.ParameterType)
			{
			case 0:
				result = new SEParam(this.Index, this.ValueUInt32);
				break;
			case 1:
				result = new SEParam(this.Index, this.ValueFloat);
				break;
			case 2:
				result = new SEParam(this.Index, this.ValueInt32);
				break;
			case 3:
				result = new SEParam(this.Index, this.ValueString);
				break;
			}
			return result;
		}

		public void SetValue(uint value)
		{
			this._value = value;
			this._strValue = null;
		}

		public void SetValue(float value)
		{
			this._value = Utils.FloatAsUInt32(value);
			this._strValue = null;
		}

		public void SetValue(int value)
		{
			this._value = Utils.Int32AsUInt32(value);
			this._strValue = null;
		}

		public void SetValue(string value)
		{
			this._value = 0u;
			this._strValue = value;
		}

		public void SetValueConverted(double value)
		{
			switch (this.ParameterType)
			{
			case 0:
				this.SetValue((uint)value);
				break;
			case 1:
				this.SetValue((float)value);
				break;
			case 2:
				this.SetValue((int)value);
				break;
			}
		}

		public void SetConvertionRate(float rate, float a, float b)
		{
			this._rate = rate;
			this._a = a;
			this._b = b;
		}

		public static ParamType GetParamTypeByType(Type type)
		{
			ParamType paramType = ParamType.COUNT;
			ParamType result;
			if (type == null)
			{
				result = paramType;
			}
			else
			{
				if (type.Equals(typeof(uint)))
				{
					paramType = ParamType.UINT32;
				}
				else if (type.Equals(typeof(float)))
				{
					paramType = ParamType.FLOAT;
				}
				else if (type.Equals(typeof(int)))
				{
					paramType = ParamType.INT;
				}
				else if (type.Equals(typeof(string)))
				{
					paramType = ParamType.STRING;
				}
				result = paramType;
			}
			return result;
		}

		public virtual string GetParamAsString()
		{
			return this.GetParamAsString(false);
		}

		public virtual string GetParamAsString(bool isConverted)
		{
			string text = null;
			string result;
			if (this.ParameterType == 3)
			{
				result = this.ValueString;
			}
			else
			{
				if (isConverted)
				{
					text = this.ValueConverted.ToString();
				}
				else
				{
					switch (this.ParameterType)
					{
					case 0:
						text = this.ValueUInt32.ToString();
						break;
					case 1:
						text = this.ValueFloat.ToString();
						break;
					case 2:
						text = this.ValueInt32.ToString();
						break;
					case 3:
						text = this.ValueString;
						break;
					}
				}
				result = text;
			}
			return result;
		}
	}
}
