using SEDevices.Records.Jupiter;
using System;

namespace SEDevices.Records.Status
{
	public class SETelemetrySystemStatusJupiter
	{
		private SETelemetryHeader _header;

		private float _vin;

		private SEPhaseData _vout;

		private SEPhaseData _power;

		private SEPhaseData _freq;

		private float _temp;

		private uint _timer;

		private EInvProcessState _invState;

		private ushort _onOffSwitch;

		private EVenusEventCodes _lastErr;

		private EInvProtInverterMode _isEmode;

		private short _relayStat;

		public SETelemetryHeader Header
		{
			get
			{
				return this._header;
			}
			set
			{
				this._header = value;
			}
		}

		public float Vin
		{
			get
			{
				return this._vin;
			}
			set
			{
				this._vin = value;
			}
		}

		public SEPhaseData Vout
		{
			get
			{
				return this._vout;
			}
			set
			{
				this._vout = value;
			}
		}

		public SEPhaseData Power
		{
			get
			{
				return this._power;
			}
			set
			{
				this._power = value;
			}
		}

		public SEPhaseData Freq
		{
			get
			{
				return this._freq;
			}
			set
			{
				this._freq = value;
			}
		}

		public float Temp
		{
			get
			{
				return this._temp;
			}
			set
			{
				this._temp = value;
			}
		}

		public uint Timer
		{
			get
			{
				return this._timer;
			}
			set
			{
				this._timer = value;
			}
		}

		public EInvProcessState InvState
		{
			get
			{
				return this._invState;
			}
			set
			{
				this._invState = value;
			}
		}

		public ushort OnOffSwitch
		{
			get
			{
				return this._onOffSwitch;
			}
			set
			{
				this._onOffSwitch = value;
			}
		}

		public EVenusEventCodes LastErr
		{
			get
			{
				return this._lastErr;
			}
			set
			{
				this._lastErr = value;
			}
		}

		public EInvProtInverterMode ISEMode
		{
			get
			{
				return this._isEmode;
			}
			set
			{
				this._isEmode = value;
			}
		}

		public short RelayStat
		{
			get
			{
				return this._relayStat;
			}
			set
			{
				this._relayStat = value;
			}
		}

		public SETelemetrySystemStatusJupiter()
		{
			this._header = new SETelemetryHeader();
		}

		public bool Equals(SETelemetrySystemStatusJupiter objToCompare)
		{
			return objToCompare == null || (this._vin == objToCompare.Vin && this._vout == objToCompare.Vout && this._power == objToCompare.Power && this._freq == objToCompare.Freq && this._temp == objToCompare.Temp && this._invState == objToCompare.InvState && this._onOffSwitch == objToCompare.OnOffSwitch);
		}
	}
}
