using System;

namespace SEDevices.Records.Status
{
	public enum EVenusEventCodes
	{
		VENUSLOG_NO_ERROR,
		VENUSLOG_WAKEUP_INVCODE,
		VENUSLOG_USER_TURN_OFF_INVCODE,
		VENUSLOG_NIGHT_MODE_INVCODE,
		VENUSLOG_SYSTEM_RESET_INVCODE,
		VENUSLOG_TEST_MODE_INVCODE,
		VENUSLOG_RELAY1_OPEN,
		VENUSLOG_FIRST_RELAY_EVENT = 6,
		VENUSLOG_RELAY1_CLOSE,
		VENUSLOG_RELAY2_OPEN,
		VENUSLOG_RELAY2_CLOSE,
		VENUSLOG_LAST_RELAY_EVENT = 9,
		VENUSLOG_ERROR_COMMUNICATION_INVCODE = 4096,
		VENUSLOG_FIRST_ERROR_INVCODE = 4096,
		VENUSLOG_ERROR_UNEXPECTED_MODE_INVCODE,
		VENUSLOG_ERROR_SUPERVISE_INVCODE,
		VENUSLOG_ERROR_VIN_DECREASE_TIMEOUT_INVCODE,
		VENUSLOG_ERROR_ISE_ERR_NO_CODE_INVCODE,
		VENUSLOG_SE_ERROR_GRIDMON_VL1L2_MAX = 8192,
		VENUSLOG_FIRST_SUPERVISE_ERROR = 8192,
		VENUSLOG_SE_ERROR_GRIDMON_VL1L2_MIN,
		VENUSLOG_SE_ERROR_GRIDMON_VOUT_MAX,
		VENUSLOG_SE_ERROR_GRIDMON_VOUT_MIN,
		VENUSLOG_SE_ERROR_GRIDMON_VOUT_MAX2,
		VENUSLOG_SE_ERROR_GRIDMON_VOUT_MIN2,
		VENUSLOG_SE_ERROR_GRIDMON_FOUT_MAX,
		VENUSLOG_SE_ERROR_GRIDMON_FOUT_MIN,
		VENUSLOG_SE_ERROR_GRIDMON_IOUT_DC,
		VENUSLOG_SE_ERROR_GRIDMON_IRCD1,
		VENUSLOG_SE_ERROR_GRIDMON_IRCD2,
		VENUSLOG_SE_ERROR_GRIDMON_SYNC,
		VENUSLOG_SE_ERROR_GRIDMON_ISLANDING,
		VENUSLOG_SE_ERROR_GRIDMON_LAST = 8204,
		VENUSLOG_SE_ERROR_UNKNOWN = 12287,
		VENUSLOG_ISE_ERROR_OPER_DRV_TZ,
		VENUSLOG_FIRST_ISE_ERROR_INVCODE = 12288,
		VENUSLOG_ISE_ERROR_OPER_RCD_TZ,
		VENUSLOG_ISE_ERROR_OPER_AMC_TZ,
		VENUSLOG_ISE_ERROR_OPER_ITO_TZ,
		VENUSLOG_ISE_ERROR_OPER_IOUT_MAX,
		VENUSLOG_ISE_ERROR_OPER_VOUT_MAX,
		VENUSLOG_ISE_ERROR_OPER_VIN_MAX,
		VENUSLOG_ISE_ERROR_OPER_REFERENCE,
		VENUSLOG_ISE_ERROR_OPER_TEMP,
		VENUSLOG_ISE_ERROR_OPER_WATCHDOG,
		VENUSLOG_ISE_ERROR_OPER_WR_TMG,
		VENUSLOG_ISE_ERROR_OPER_WR_VBG,
		VENUSLOG_ISE_ERROR_OPER_WR_ATO,
		VENUSLOG_ISE_ERROR_OPER_UA_VBG,
		VENUSLOG_ISE_ERROR_OPER_UA_ATO,
		VENUSLOG_ISE_ERROR_INIT_TEMP_SENS,
		VENUSLOG_ISE_ERROR_INIT_SE_ISOL_CHK_FAULT,
		VENUSLOG_ISE_ERROR_INIT_RELAY_TEST_FAULT,
		VENUSLOG_ISE_ERROR_INIT_OFS_GAIN_CORRECTION,
		VENUSLOG_ISE_ERROR_INIT_RCD_TEST,
		VENUSLOG_ISE_ERROR_INIT_ITER_ISOL_TEST,
		VENUSLOG_ISE_ERROR_GRIDMON_VL1L2_MAX,
		VENUSLOG_ISE_ERROR_GRIDMON_VL1L2_MIN,
		VENUSLOG_ISE_ERROR_GRIDMON_VOUT_MAX,
		VENUSLOG_ISE_ERROR_GRIDMON_VOUT_MIN,
		VENUSLOG_ISE_ERROR_GRIDMON_VOUT_MAX2,
		VENUSLOG_ISE_ERROR_GRIDMON_FOUT_MAX,
		VENUSLOG_ISE_ERROR_GRIDMON_FOUT_MIN,
		VENUSLOG_ISE_ERROR_GRIDMON_IOUT_DC,
		VENUSLOG_ISE_ERROR_GRIDMON_IRCD1,
		VENUSLOG_ISE_ERROR_GRIDMON_IRCD2,
		VENUSLOG_ISE_ERROR_GRIDMON_SYNC,
		VENUSLOG_ISE_ERROR_GRIDMON_ISLANDING,
		VENUSLOG_ISE_ERROR_GRIDMON_VOUT_MIN2,
		VENUSLOG_LAST_ISE_ERROR_INVCODE = 12321,
		VENUSLOG_ISE_ERROR_UNKNOWN = 16383,
		VENUSLOG_LAST_ERROR_INVCODE = 16383,
		VENUSLOG_COUNTRY_GENERAL_PARAMS,
		VENUSLOG_FIRST_COUNTRY_INVCODE = 16384,
		VENUSLOG_COUNTRY_AUSTRALIA,
		VENUSLOG_COUNTRY_FRANCE,
		VENUSLOG_COUNTRY_GERMANY,
		VENUSLOG_COUNTRY_GREECE_CONTINENT,
		VENUSLOG_COUNTRY_GREECE_ISLANDS,
		VENUSLOG_COUNTRY_ISRAEL,
		VENUSLOG_COUNTRY_ITALY,
		VENUSLOG_COUNTRY_SPAIN,
		VENUSLOG_COUNTRY_UK,
		VENUSLOG_COUNTRY_US_AUTO,
		VENUSLOG_COUNTRY_US_208V,
		VENUSLOG_COUNTRY_US_240V,
		VENUSLOG_COUNTRY_US_208V_NO_NEUTRAL,
		VENUSLOG_COUNTRY_US_240V_NO_NEUTRAL,
		VENUSLOG_LAST_COUNTRY_INVCODE = 16398
	}
}
