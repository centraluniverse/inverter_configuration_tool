using System;

namespace SEDevices.Records.Telemetry
{
	public class SETelemetryString : SETelemetry
	{
		public new const int VI_Q = 4;

		public new const int VO_Q = 4;

		public new const int POWER_Q = 3;

		public new const int TEMP_Q = 2;

		public new const int IIN_Q = 8;

		private uint _receiveId;

		private uint _dstId;

		private uint _secs;

		private float _vIn;

		private float _vOut;

		private float _iIn;

		private float _accPower;

		private float _temp;

		public uint ReceiverId
		{
			get
			{
				return this._receiveId;
			}
			set
			{
				this._receiveId = value;
			}
		}

		public uint DstId
		{
			get
			{
				return this._dstId;
			}
			set
			{
				this._dstId = value;
			}
		}

		public uint Seconds
		{
			get
			{
				return this._secs;
			}
			set
			{
				this._secs = value;
			}
		}

		public float Vin
		{
			get
			{
				return this._vIn;
			}
			set
			{
				this._vIn = value;
			}
		}

		public float Vout
		{
			get
			{
				return this._vOut;
			}
			set
			{
				this._vOut = value;
			}
		}

		public float Iin
		{
			get
			{
				return this._iIn;
			}
			set
			{
				this._iIn = value;
			}
		}

		public float AccPower
		{
			get
			{
				return this._accPower;
			}
			set
			{
				this._accPower = value;
			}
		}

		public float Temperature
		{
			get
			{
				return this._temp;
			}
			set
			{
				this._temp = value;
			}
		}

		public override long ParseRecord(ref byte[] data, long position)
		{
			base.ParseRecord(ref data, position);
			this.ReceiverId = base.Reader.ReadUInt32();
			this.DstId = base.Reader.ReadUInt32();
			this.Seconds = base.Reader.ReadUInt32();
			this.Vin = base.Reader.ReadSingle();
			this.Vout = base.Reader.ReadSingle();
			this.Iin = base.Reader.ReadSingle();
			this.AccPower = base.Reader.ReadSingle();
			this.Temperature = base.Reader.ReadSingle();
			return base.MemStream.Position;
		}
	}
}
