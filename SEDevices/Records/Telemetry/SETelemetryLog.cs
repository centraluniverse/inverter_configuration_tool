using System;

namespace SEDevices.Records.Telemetry
{
	public class SETelemetryLog : SETelemetry
	{
		private uint _errCode;

		private float _param0;

		private float _param1;

		private float _param2;

		private float _param3;

		private float _param4;

		public uint ErrorCode
		{
			get
			{
				return this._errCode;
			}
			set
			{
				this._errCode = value;
			}
		}

		public float Param0
		{
			get
			{
				return this._param0;
			}
			set
			{
				this._param0 = value;
			}
		}

		public float Param1
		{
			get
			{
				return this._param1;
			}
			set
			{
				this._param1 = value;
			}
		}

		public float Param2
		{
			get
			{
				return this._param2;
			}
			set
			{
				this._param2 = value;
			}
		}

		public float Param3
		{
			get
			{
				return this._param3;
			}
			set
			{
				this._param3 = value;
			}
		}

		public float Param4
		{
			get
			{
				return this._param4;
			}
			set
			{
				this._param4 = value;
			}
		}

		public override long ParseRecord(ref byte[] data, long position)
		{
			base.ParseRecord(ref data, position);
			this._errCode = base.Reader.ReadUInt32();
			this._param0 = base.Reader.ReadSingle();
			this._param1 = base.Reader.ReadSingle();
			this._param2 = base.Reader.ReadSingle();
			this._param3 = base.Reader.ReadSingle();
			this._param4 = base.Reader.ReadSingle();
			return base.MemStream.Position;
		}
	}
}
