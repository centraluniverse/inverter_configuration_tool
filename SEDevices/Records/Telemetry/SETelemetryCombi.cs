using System;

namespace SEDevices.Records.Telemetry
{
	public class SETelemetryCombi : SETelemetry
	{
		private uint _secs;

		private uint _secsDelta;

		private uint _initiatorID;

		private ushort _trippedCode;

		private ushort _status;

		private ushort _errorCode;

		private ushort _activeStrings;

		private float _inverterVoltage;

		private float _totalSystemCurrent;

		private float _temperature;

		private float _vSupply;

		private float _5VCommon;

		private float _vRef1;

		private float _vRef2;

		private float _energyDelta;

		private float _energy;

		public uint Seconds
		{
			get
			{
				return this._secs;
			}
			set
			{
				this._secs = value;
			}
		}

		public uint SecondsDelta
		{
			get
			{
				return this._secsDelta;
			}
			set
			{
				this._secsDelta = value;
			}
		}

		public uint InitiatorID
		{
			get
			{
				return this._initiatorID;
			}
			set
			{
				this._initiatorID = value;
			}
		}

		public ushort TrippedCode
		{
			get
			{
				return this._trippedCode;
			}
			set
			{
				this._trippedCode = value;
			}
		}

		public ushort Status
		{
			get
			{
				return this._status;
			}
			set
			{
				this._status = value;
			}
		}

		public ushort ErrorCode
		{
			get
			{
				return this._errorCode;
			}
			set
			{
				this._errorCode = value;
			}
		}

		public ushort ActiveStrings
		{
			get
			{
				return this._activeStrings;
			}
			set
			{
				this._activeStrings = value;
			}
		}

		public float InverterVoltage
		{
			get
			{
				return this._inverterVoltage;
			}
			set
			{
				this._inverterVoltage = value;
			}
		}

		public float TotalSystemCurrent
		{
			get
			{
				return this._totalSystemCurrent;
			}
			set
			{
				this._totalSystemCurrent = value;
			}
		}

		public float Temperature
		{
			get
			{
				return this._temperature;
			}
			set
			{
				this._temperature = value;
			}
		}

		public float VSupply
		{
			get
			{
				return this._vSupply;
			}
			set
			{
				this._vSupply = value;
			}
		}

		public float Common_5V
		{
			get
			{
				return this._5VCommon;
			}
			set
			{
				this._5VCommon = value;
			}
		}

		public float V_Ref_1
		{
			get
			{
				return this._vRef1;
			}
			set
			{
				this._vRef1 = value;
			}
		}

		public float V_Ref_2
		{
			get
			{
				return this._vRef2;
			}
			set
			{
				this._vRef2 = value;
			}
		}

		public float EnergyDelta
		{
			get
			{
				return this._energyDelta;
			}
			set
			{
				this._energyDelta = value;
			}
		}

		public float Energy
		{
			get
			{
				return this._energy;
			}
			set
			{
				this._energy = value;
			}
		}

		public override long ParseRecord(ref byte[] data, long position)
		{
			base.ParseRecord(ref data, position);
			this.Seconds = base.Reader.ReadUInt32();
			this.SecondsDelta = base.Reader.ReadUInt32();
			this.InitiatorID = base.Reader.ReadUInt32();
			this.TrippedCode = base.Reader.ReadUInt16();
			this.Status = base.Reader.ReadUInt16();
			this.ErrorCode = base.Reader.ReadUInt16();
			this.ActiveStrings = base.Reader.ReadUInt16();
			this.InverterVoltage = base.Reader.ReadSingle();
			this.TotalSystemCurrent = base.Reader.ReadSingle();
			this.Temperature = base.Reader.ReadSingle();
			this.VSupply = base.Reader.ReadSingle();
			this.Common_5V = base.Reader.ReadSingle();
			this.V_Ref_1 = base.Reader.ReadSingle();
			this.V_Ref_2 = base.Reader.ReadSingle();
			this.EnergyDelta = base.Reader.ReadSingle();
			this.Energy = base.Reader.ReadSingle();
			return base.MemStream.Position;
		}
	}
}
