using System;

namespace SEDevices.Records.Telemetry
{
	public class SETelemetryInverter3Phase : SETelemetry
	{
		private uint _secs;

		private uint _secsDelta;

		private float _temperature;

		private float _accPowerAC;

		private float _accPowerDeltaAC;

		private float _voltageAC1;

		private float _voltageAC2;

		private float _voltageAC3;

		private float _currentAC1;

		private float _currentAC2;

		private float _currentAC3;

		private float _freqAC1;

		private float _freqAC2;

		private float _freqAC3;

		private float _accPowerDC;

		private float _accPowerDeltaDC;

		private float _voltageDC;

		private float _currentDC;

		public uint Seconds
		{
			get
			{
				return this._secs;
			}
			set
			{
				this._secs = value;
			}
		}

		public uint SecondsDelta
		{
			get
			{
				return this._secsDelta;
			}
			set
			{
				this._secsDelta = value;
			}
		}

		public float Temperature
		{
			get
			{
				return this._temperature;
			}
			set
			{
				this._temperature = value;
			}
		}

		public float AccumulatedPowerAC
		{
			get
			{
				return this._accPowerAC;
			}
			set
			{
				this._accPowerAC = value;
			}
		}

		public float AccumulatedDeltaPowerAC
		{
			get
			{
				return this._accPowerDeltaAC;
			}
			set
			{
				this._accPowerDeltaAC = value;
			}
		}

		public float VoltageAC1
		{
			get
			{
				return this._voltageAC1;
			}
			set
			{
				this._voltageAC1 = value;
			}
		}

		public float VoltageAC2
		{
			get
			{
				return this._voltageAC2;
			}
			set
			{
				this._voltageAC2 = value;
			}
		}

		public float VoltageAC3
		{
			get
			{
				return this._voltageAC3;
			}
			set
			{
				this._voltageAC3 = value;
			}
		}

		public float CurrentAC1
		{
			get
			{
				return this._currentAC1;
			}
			set
			{
				this._currentAC1 = value;
			}
		}

		public float CurrentAC2
		{
			get
			{
				return this._currentAC2;
			}
			set
			{
				this._currentAC2 = value;
			}
		}

		public float CurrentAC3
		{
			get
			{
				return this._currentAC3;
			}
			set
			{
				this._currentAC3 = value;
			}
		}

		public float FreqAC1
		{
			get
			{
				return this._freqAC1;
			}
			set
			{
				this._freqAC1 = value;
			}
		}

		public float FreqAC2
		{
			get
			{
				return this._freqAC2;
			}
			set
			{
				this._freqAC2 = value;
			}
		}

		public float FreqAC3
		{
			get
			{
				return this._freqAC3;
			}
			set
			{
				this._freqAC3 = value;
			}
		}

		public float AccumulatedPowerDC
		{
			get
			{
				return this._accPowerDC;
			}
			set
			{
				this._accPowerDC = value;
			}
		}

		public float AccumulatedDeltaPowerDC
		{
			get
			{
				return this._accPowerDeltaDC;
			}
			set
			{
				this._accPowerDeltaDC = value;
			}
		}

		public float VoltageDC
		{
			get
			{
				return this._voltageDC;
			}
			set
			{
				this._voltageDC = value;
			}
		}

		public float CurrentDC
		{
			get
			{
				return this._currentDC;
			}
			set
			{
				this._currentDC = value;
			}
		}

		public SETelemetryInverter3Phase()
		{
			base.Type = RecordType.INVERTER_1PHASE;
		}

		public override long ParseRecord(ref byte[] data, long position)
		{
			base.ParseRecord(ref data, position);
			this._secs = base.Reader.ReadUInt32();
			this._secsDelta = base.Reader.ReadUInt32();
			this._temperature = base.Reader.ReadSingle();
			this._accPowerAC = base.Reader.ReadSingle();
			this._accPowerDeltaAC = base.Reader.ReadSingle();
			this._voltageAC1 = base.Reader.ReadSingle();
			this._voltageAC2 = base.Reader.ReadSingle();
			this._voltageAC3 = base.Reader.ReadSingle();
			this._currentAC1 = base.Reader.ReadSingle();
			this._currentAC2 = base.Reader.ReadSingle();
			this._currentAC3 = base.Reader.ReadSingle();
			this._freqAC1 = base.Reader.ReadSingle();
			this._freqAC2 = base.Reader.ReadSingle();
			this._freqAC3 = base.Reader.ReadSingle();
			this._accPowerDC = base.Reader.ReadSingle();
			this._accPowerDeltaDC = base.Reader.ReadSingle();
			this._voltageDC = base.Reader.ReadSingle();
			this._currentDC = base.Reader.ReadSingle();
			return base.MemStream.Position;
		}
	}
}
