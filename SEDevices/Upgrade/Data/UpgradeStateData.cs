using SEDevices.Upgrade.DeviceVersions.Compatibility;
using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.Data
{
	[XmlRoot("upgradeState")]
	public class UpgradeStateData
	{
		private UpgradeState _upgradeState;

		private int? _upgradeBytesCompleted;

		private List<ParamaeterPair> _savedParameters;

		private PortiaCompatibilityItem _portiaCompatibiltyItem;

		private PortiaVersionInfo _originalPortiaVersion;

		private uint? _previousPortiaAddress;

		private VenusCompatibilityItem _venusCompatibiltyItem;

		private VenusVersionInfo _originalVenusVersion;

		private IseVersionInfo _originalIseVersion;

		private JupiterCompatibilityItem _jupiterCompatibiltyItem;

		private JupiterVersionInfo _originalJupiterVersion;

		private JupiterPowerCompatibilityItem _jupiterPowerCompatibiltyItem;

		private JupiterPowerVersionInfo _originalJupiterPowerVersion;

		private GeminiCompatibilityItem _geminiCompatibiltyItem;

		private GeminiVersionInfo _originalGeminiVersion;

		[XmlElement("state")]
		public UpgradeState UpgradeState
		{
			get
			{
				return this._upgradeState;
			}
			set
			{
				this._upgradeState = value;
			}
		}

		[XmlElement("bytesCompleted")]
		public int? UpgradeBytesCompleted
		{
			get
			{
				return this._upgradeBytesCompleted;
			}
			set
			{
				this._upgradeBytesCompleted = value;
			}
		}

		[XmlElement("savedParameters")]
		public List<ParamaeterPair> SavedParameters
		{
			get
			{
				return this._savedParameters;
			}
			set
			{
				this._savedParameters = value;
			}
		}

		[XmlElement("portiaCompatibilityItem")]
		public PortiaCompatibilityItem PortiaCompatibiltyItem
		{
			get
			{
				return this._portiaCompatibiltyItem;
			}
			set
			{
				this._portiaCompatibiltyItem = value;
			}
		}

		[XmlElement("originalPortiaVersion")]
		public PortiaVersionInfo OriginalPortiaVersion
		{
			get
			{
				return this._originalPortiaVersion;
			}
			set
			{
				this._originalPortiaVersion = value;
			}
		}

		[XmlElement("perviosPortiaAddress")]
		public uint? PreviousPortiaAddress
		{
			get
			{
				return this._previousPortiaAddress;
			}
			set
			{
				this._previousPortiaAddress = value;
			}
		}

		[XmlElement("venusCompatibilityItem")]
		public VenusCompatibilityItem VenusCompatibiltyItem
		{
			get
			{
				return this._venusCompatibiltyItem;
			}
			set
			{
				this._venusCompatibiltyItem = value;
			}
		}

		[XmlElement("originalVenusVersion")]
		public VenusVersionInfo OriginalVenusVersion
		{
			get
			{
				return this._originalVenusVersion;
			}
			set
			{
				this._originalVenusVersion = value;
			}
		}

		[XmlElement("originalIseVersion")]
		public IseVersionInfo OriginalIseVersion
		{
			get
			{
				return this._originalIseVersion;
			}
			set
			{
				this._originalIseVersion = value;
			}
		}

		[XmlElement("jupiterCompatibilityItem")]
		public JupiterCompatibilityItem JupiterCompatibiltyItem
		{
			get
			{
				return this._jupiterCompatibiltyItem;
			}
			set
			{
				this._jupiterCompatibiltyItem = value;
			}
		}

		[XmlElement("originalJupiterVersion")]
		public JupiterVersionInfo OriginalJupiterVersion
		{
			get
			{
				return this._originalJupiterVersion;
			}
			set
			{
				this._originalJupiterVersion = value;
			}
		}

		[XmlElement("jupiterPowerCompatibilityItem")]
		public JupiterPowerCompatibilityItem JupiterPowerCompatibiltyItem
		{
			get
			{
				return this._jupiterPowerCompatibiltyItem;
			}
			set
			{
				this._jupiterPowerCompatibiltyItem = value;
			}
		}

		[XmlElement("originalJupiterPowerVersion")]
		public JupiterPowerVersionInfo OriginalJupiterPowerVersion
		{
			get
			{
				return this._originalJupiterPowerVersion;
			}
			set
			{
				this._originalJupiterPowerVersion = value;
			}
		}

		[XmlElement("geminiCompatibilityItem")]
		public GeminiCompatibilityItem GeminiCompatibiltyItem
		{
			get
			{
				return this._geminiCompatibiltyItem;
			}
			set
			{
				this._geminiCompatibiltyItem = value;
			}
		}

		[XmlElement("originalGeminiVersion")]
		public GeminiVersionInfo OriginalGeminiVersion
		{
			get
			{
				return this._originalGeminiVersion;
			}
			set
			{
				this._originalGeminiVersion = value;
			}
		}
	}
}
