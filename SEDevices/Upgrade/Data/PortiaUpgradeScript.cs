using SEDevices.Upgrade.DeviceVersions.Compatibility;
using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.Data
{
	public class PortiaUpgradeScript
	{
		private PortiaVersionInfo m_PortiaVersion;

		private List<PortiaCompatibilityItem> m_PortiaCompatabilityList;

		[XmlElement("portiaVersion")]
		public PortiaVersionInfo PortiaVersion
		{
			get
			{
				return this.m_PortiaVersion;
			}
			set
			{
				this.m_PortiaVersion = value;
			}
		}

		[XmlElement("compatiblePortiaVersions")]
		public List<PortiaCompatibilityItem> PortiaCompatabilityList
		{
			get
			{
				return this.m_PortiaCompatabilityList;
			}
			set
			{
				this.m_PortiaCompatabilityList = value;
			}
		}
	}
}
