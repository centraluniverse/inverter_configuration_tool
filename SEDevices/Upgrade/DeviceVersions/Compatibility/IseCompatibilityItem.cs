using SEDevices.Upgrade.DeviceVersions.Base;
using SEDevices.Upgrade.DeviceVersions.Info;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.DeviceVersions.Compatibility
{
	public class IseCompatibilityItem : DeviceCompatbilityItem
	{
		private List<IseVersionInfo> _iseVersions;

		private List<VenusCompatibilityItem> _venusCompatabilityList;

		[XmlElement("iseVersions")]
		public List<IseVersionInfo> IseVersions
		{
			get
			{
				return this._iseVersions;
			}
			set
			{
				this._iseVersions = value;
			}
		}

		[XmlElement("venusCompatabilityList")]
		public List<VenusCompatibilityItem> VenusCompatabilityList
		{
			get
			{
				return this._venusCompatabilityList;
			}
			set
			{
				this._venusCompatabilityList = value;
			}
		}
	}
}
