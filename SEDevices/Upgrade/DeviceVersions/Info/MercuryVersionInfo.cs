using SEDevices.Upgrade.DeviceVersions.Base;
using System;
using System.Xml.Serialization;

namespace SEDevices.Upgrade.DeviceVersions.Info
{
	public class MercuryVersionInfo : DeviceVersionInfo
	{
		private uint _majorVersion;

		private uint _minorVersion;

		[XmlElement("majorVersion")]
		public uint MajorVersion
		{
			get
			{
				return this._majorVersion;
			}
			set
			{
				this._majorVersion = value;
			}
		}

		[XmlElement("minorVersion")]
		public uint MinorVersion
		{
			get
			{
				return this._minorVersion;
			}
			set
			{
				this._minorVersion = value;
			}
		}

		public bool Equals(MercuryVersionInfo mercuryVersion)
		{
			return mercuryVersion._majorVersion == this._majorVersion && mercuryVersion._minorVersion == this._minorVersion;
		}

		public override string ToString()
		{
			return this._majorVersion + "." + this._minorVersion;
		}
	}
}
