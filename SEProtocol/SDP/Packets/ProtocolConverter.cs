using System;

namespace SEProtocol.SDP.Packets
{
	public class ProtocolConverter
	{
		private const ushort PROT_CMD_VER1_OFFSET = 1;

		private const ushort PROT_RESP_VER1_OFFSET = 129;

		private const ushort PROT_CMD_SIZE = 34;

		private const ushort PROT_RESP_SIZE = 15;

		private static ushort[] CmdVer1ToVer2 = new ushort[]
		{
			ProtocolMercuryCommands.PROT_CMD_MERCURY_SET_OTP_BLOCK,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_PWM_ENABLE,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_A2D_SAMPLE,
			ProtocolParametersCommands.PROT_CMD_PARAMS_SET_SINGLE,
			ProtocolParametersCommands.PROT_CMD_PARAMS_SET_ALL,
			ProtocolParametersCommands.PROT_CMD_PARAMS_RESET,
			ProtocolParametersCommands.PROT_CMD_PARAMS_GET_ALL,
			ProtocolSuntracerCommands.PROT_CMD_SUNTRACER_READ_FLASH,
			ProtocolSuntracerCommands.PROT_CMD_SUNTRACER_START,
			ProtocolSuntracerCommands.PROT_CMD_SUNTRACER_SET_RTC,
			ProtocolSuntracerCommands.PROT_CMD_SUNTRACER_DEL_FLASH,
			ProtocolSuntracerCommands.PROT_CMD_SUNTRACER_DEL_FLASH_SECTOR,
			ProtocolUpgradeCommands.PROT_CMD_UPGRADE_START,
			ProtocolUpgradeCommands.PROT_CMD_UPGRADE_WRITE,
			ProtocolUpgradeCommands.PROT_CMD_UPGRADE_FINISH,
			ProtocolMiscCommands.PROT_CMD_MISC_RESET,
			ProtocolMiscCommands.PROT_CMD_MISC_STOP,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_KA,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_SET_VIREF,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_SET_VOMAXREF,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_READ_MEAS,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_CLOSED_LOOP_START,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_OPEN_LOOP_START,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_OPEN_LOOP_SET,
			ProtocolUpgradeCommands.PROT_CMD_UPGRADE_READ_DATA,
			ProtocolUpgradeCommands.PROT_CMD_UPGRADE_READ_SIZE,
			ProtocolParametersCommands.PROT_CMD_PARAMS_GET_SINGLE,
			ProtocolParametersCommands.PROT_CMD_PARAMS_GET_INFO,
			ProtocolParametersCommands.PROT_CMD_PARAMS_GET_NAME,
			ProtocolParametersCommands.PROT_CMD_PARAMS_GET_NUM,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_SET_12V_10V,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_SET_5V_35V,
			ProtocolMercuryCommands.PROT_CMD_MERCURY_SET_VO_RANGE,
			ProtocolMiscCommands.PROT_CMD_MISC_DUMMY,
		};

		private static ushort[] RespVer1ToVer2 = new ushort[]
		{
			ProtocolGeneralResponses.PROT_RESP_ACK,
			ProtocolMercuryResponses.PROT_RESP_MERCURY_SAMPLES,
			ProtocolParametersResponses.PROT_RESP_PARAMS_ALL,
			ProtocolSuntracerResponses.PROT_RESP_SUNTRACER_TRACE,
			ProtocolGeneralResponses.PROT_RESP_NACK,
			ProtocolSuntracerResponses.PROT_RESP_SUNTRACER_FLASH,
			ProtocolMercuryResponses.PROT_RESP_MERCURY_MON,
			ProtocolMercuryResponses.PROT_RESP_MERCURY_TELEM,
			ProtocolMercuryResponses.PROT_RESP_MERCURY_MEAS,
			ProtocolUpgradeResponses.PROT_RESP_UPGRADE_SIZE,
			ProtocolParametersResponses.PROT_RESP_PARAMS_SINGLE,
			ProtocolParametersResponses.PROT_RESP_PARAMS_INFO,
			ProtocolParametersResponses.PROT_RESP_PARAMS_NAME,
			ProtocolParametersResponses.PROT_RESP_PARAMS_NUM,
			ProtocolUpgradeResponses.PROT_RESP_UPGRADE_DATA,
		};

		public static ushort Ver2ToVer1Conv(ushort ProtocolCode)
		{
			ushort result;
			for (int i = 0; i < PROT_CMD_SIZE; i++)
			{
				if (ProtocolConverter.CmdVer1ToVer2[i] == ProtocolCode)
				{
					result = (ushort)(i + PROT_CMD_VER1_OFFSET);
					return result;
				}
			}
			for (int i = 0; i < PROT_RESP_SIZE; i++)
			{
				if (ProtocolConverter.RespVer1ToVer2[i] == ProtocolCode)
				{
					result = (ushort)(i + PROT_RESP_VER1_OFFSET);
					return result;
				}
			}
			result = 0;
			return result;
		}

		public static ushort Ver1ToVer2Conv(ushort ProtocolCode)
		{
			ushort result;
			if (ProtocolCode < PROT_RESP_VER1_OFFSET)
			{
				result = ProtocolConverter.CmdVer1ToVer2[(int)(ProtocolCode - PROT_CMD_VER1_OFFSET)];
			}
			else
			{
				result = ProtocolConverter.RespVer1ToVer2[(int)(ProtocolCode - PROT_RESP_VER1_OFFSET)];
			}
			return result;
		}
	}
}
