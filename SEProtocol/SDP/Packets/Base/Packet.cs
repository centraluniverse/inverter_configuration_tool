using System;
using System.IO;

namespace SEProtocol.SDP.Packets.Base
{
	public class Packet
	{
		protected enum ReceivePacketState
		{
			PACKET_READ_BARKER,
			PACKET_READ_LEN1,
			PACKET_READ_LEN2,
			PACKET_READ_NLEN1,
			PACKET_READ_NLEN2,
			PACKET_READ_ID1,
			PACKET_READ_ID2,
			PACKET_READ_SRC1,
			PACKET_READ_SRC2,
			PACKET_READ_SRC3,
			PACKET_READ_SRC4,
			PACKET_READ_DST1,
			PACKET_READ_DST2,
			PACKET_READ_DST3,
			PACKET_READ_DST4,
			PACKET_READ_CODE1,
			PACKET_READ_CODE2,
			PACKET_READ_DATA,
			PACKET_READ_CRC1,
			PACKET_READ_CRC2,
			COUNT
		}

		public const int BARKER_LENGTH_UNDEFINED = -1;

		public const uint LENGTH_DATA_BUFFER_DEFAULT = 512u;

		public const uint LENGTH_DATA_STRING_VALUE_MAX = 40u;

		private byte[] _barker;

		private PacketVersion _packetVersion;

		private uint _maxDataBufferLength = 0u;

		private ushort _id;

		private ushort _opCode;

		private uint _src;

		private uint _dst;

		private int _length = 0;

		private byte[] _data = null;

		private MemoryStream _memStream;

		private BinaryWriter _writer;

		private BinaryReader _reader;

		protected ushort _notLength = 0;

		protected int _expectedLength = 0;

		protected ushort _tempCRC = 0;

		private ushort _expectedResponseOpCode;

		private bool _shouldWaitForReceive = false;

		private Packet.ReceivePacketState _packetReceiveState = Packet.ReceivePacketState.PACKET_READ_BARKER;

		private int _internalBarkerState = 0;

		private bool _isParsingDone = false;

		public static int generalInt = 0;

		public byte[] Barker
		{
			get
			{
				return this._barker;
			}
			set
			{
				this._barker = value;
			}
		}

		public int BarkerLength
		{
			get
			{
				int result = BARKER_LENGTH_UNDEFINED;
				if (this._barker != null)
				{
					result = this._barker.Length;
				}
				return result;
			}
		}

		public PacketVersion PacketVersion
		{
			get
			{
				return this._packetVersion;
			}
		}

		public ushort ID
		{
			get
			{
				return this._id;
			}
			set
			{
				this._id = value;
			}
		}

		public ushort OpCode
		{
			get
			{
				return this._opCode;
			}
			set
			{
				this._opCode = value;
			}
		}

		public uint Source
		{
			get
			{
				return this._src;
			}
			set
			{
				this._src = value;
			}
		}

		public uint Destination
		{
			get
			{
				return this._dst;
			}
			set
			{
				this._dst = value;
			}
		}

		public int Length
		{
			get
			{
				return this._length;
			}
			set
			{
				this._length = value;
			}
		}

		public byte[] Data
		{
			get
			{
				return this._data;
			}
		}

		public ushort ExpectedOpCode
		{
			get
			{
				return this._expectedResponseOpCode;
			}
			set
			{
				this._expectedResponseOpCode = value;
			}
		}

		public bool WaitForReceive
		{
			get
			{
				return this._shouldWaitForReceive;
			}
			set
			{
				this._shouldWaitForReceive = value;
			}
		}

		protected Packet.ReceivePacketState ReceiveState
		{
			get
			{
				return this._packetReceiveState;
			}
		}

		protected int InternalBarkerState
		{
			get
			{
				return this._internalBarkerState;
			}
		}

		public bool ParsingDone
		{
			get
			{
				return this._isParsingDone;
			}
			set
			{
				this._isParsingDone = value;
			}
		}

		public static Packet GetNewPacket(PacketVersion version, uint maxDataBufferLength)
		{
			Packet result;
			switch (version)
			{
			case PacketVersion.A:
				result = new PacketA(maxDataBufferLength);
				break;
			case PacketVersion.B:
				result = new PacketB(maxDataBufferLength);
				break;
			default:
				result = null;
				break;
			}
			return result;
		}

		public static Packet GetNewPacket(Packet packet)
		{
			Packet result;
			switch (packet.PacketVersion)
			{
			case PacketVersion.A:
				result = new PacketA(packet);
				break;
			case PacketVersion.B:
				result = new PacketB(packet);
				break;
			default:
				result = null;
				break;
			}
			return result;
		}

		public Packet(PacketVersion version, byte[] barker, uint maxDataBufferLength)
		{
			this._barker = barker;
			this._packetVersion = version;
			this._maxDataBufferLength = maxDataBufferLength;
			this.SetupDataStream();
		}

		public virtual void Reset()
		{
			this._id = 0;
			this._opCode = 0;
			this._src = 0u;
			this._dst = 0u;
			this._shouldWaitForReceive = true;
			this.StateReset();
		}

		private void SetupDataStream()
		{
			this._data = new byte[this._maxDataBufferLength];
			this._memStream = new MemoryStream(this._data);
			this._writer = new BinaryWriter(this._memStream);
			this._reader = new BinaryReader(this._memStream);
		}

		private void ReleaseDataStream()
		{
			if (this._writer != null)
			{
				this._writer = null;
			}
			if (this._reader != null)
			{
				this._reader = null;
			}
			if (this._memStream != null)
			{
				this._memStream.Close();
				this._memStream.Dispose();
				this._memStream = null;
			}
			this._data = null;
		}

		public Packet(Packet packet) : this(packet.PacketVersion, packet.Barker, (uint)packet.Data.Length)
		{
			this._packetVersion = packet.PacketVersion;
			this._id = packet.ID;
			this._length = packet.Length;
			this._opCode = packet.OpCode;
			this._src = packet.Source;
			this._dst = packet.Destination;
			this._data = packet.Data;
			if (this._data != null)
			{
				this._memStream = new MemoryStream(this._data);
				this._writer = new BinaryWriter(this._memStream);
				this._reader = new BinaryReader(this._memStream);
			}
			this._packetReceiveState = packet._packetReceiveState;
			this._expectedResponseOpCode = packet.ExpectedOpCode;
			this._shouldWaitForReceive = packet.WaitForReceive;
			this._isParsingDone = packet.ParsingDone;
		}

		public void Append(object data)
		{
			Type type = data.GetType();
			if (type == typeof(string))
			{
				int length = ((string)data).Length;
				for (int i = 0; i < length; i++)
				{
					byte value = (byte)((string)data)[i];
					this._writer.Write(value);
					this._length++;
				}
				this._writer.Write('\0');
				this._length++;
			}
			else if (type == typeof(char))
			{
				this._writer.Write((char)data);
				this._length += sizeof(char);
			}
			else if (type == typeof(byte))
			{
				this._writer.Write((byte)data);
				this._length++;
			}
			else if (type == typeof(byte[]))
			{
				this._writer.Write((byte[])data);
				this._length += ((byte[])data).Length;
			}
			else if (type == typeof(short))
			{
				this._writer.Write((short)data);
				this._length += sizeof(short);
			}
			else if (type == typeof(int))
			{
				this._writer.Write((int)data);
				this._length += sizeof(int);
			}
			else if (type == typeof(long))
			{
				this._writer.Write((long)data);
				this._length += sizeof(long);
			}
			else if (type == typeof(ushort))
			{
				this._writer.Write((ushort)data);
				this._length += sizeof(ushort);
			}
			else if (type == typeof(uint))
			{
				this._writer.Write((uint)data);
				this._length += sizeof(uint);
			}
			else if (type == typeof(ulong))
			{
				this._writer.Write((ulong)data);
				this._length += sizeof(ulong);
			}
			else if (type == typeof(float))
			{
				this._writer.Write((float)data);
				this._length += sizeof(float);
			}
			else if (type == typeof(double))
			{
				this._writer.Write((double)data);
				this._length += sizeof(double);
			}
			else if (type == typeof(bool))
			{
				this._writer.Write((bool)data);
				this._length++;
			}
		}

		public void FlushData()
		{
			this._writer.Flush();
		}

		public string RetrieveString()
		{
			string text = "";
			char c;
			while ((c = this._reader.ReadChar()) != '\0')
			{
				text += c;
			}
			return text;
		}

		public ushort RetrieveUInt16()
		{
			return this._reader.ReadUInt16();
		}

		public short RetrieveInt16()
		{
			return this._reader.ReadInt16();
		}

		public uint RetrieveUInt32()
		{
			return this._reader.ReadUInt32();
		}

		public int RetrieveInt32()
		{
			return this._reader.ReadInt32();
		}

		public ulong RetrieveUInt64()
		{
			return this._reader.ReadUInt64();
		}

		public long RetrieveInt64()
		{
			return this._reader.ReadInt64();
		}

		public float RetrieveSingle()
		{
			return this._reader.ReadSingle();
		}

		public byte[] RetrieveBytes(int count)
		{
			return this._reader.ReadBytes(count);
		}

		protected void StateBarkerNext()
		{
			int newState = ++this._internalBarkerState;
			this.StateBarkerMoveTo(newState);
		}

		protected void StateBarkerMoveTo(int newState)
		{
			if (newState >= Packet.ReceivePacketState.PACKET_READ_BARKER && newState < this.BarkerLength)
			{
				this._internalBarkerState = newState;
			}
			else
			{
				this.StateBarkerReset();
			}
		}

		protected void StateBarkerReset()
		{
			this._internalBarkerState = Packet.ReceivePacketState.PACKET_READ_BARKER;
		}

		protected void StateNext()
		{
			int newState = (int)(this._packetReceiveState + 1);
			this.StateMoveTo(newState);
		}

		protected void StateReset()
		{
			this._isParsingDone = false;
			this._packetReceiveState = Packet.ReceivePacketState.PACKET_READ_BARKER;
			this._expectedLength = 0;
			this._tempCRC = 0;
			this.StateBarkerReset();
			this.ReleaseDataStream();
			this.SetupDataStream();
		}

		protected void StateMoveTo(int newState)
		{
			if (newState >= Packet.ReceivePacketState.PACKET_READ_BARKER && newState < Packet.ReceivePacketState.COUNT)
			{
				this._packetReceiveState = (Packet.ReceivePacketState)newState;
			}
			else if (newState >= ReceivePacketState.COUNT)
			{
				this.StateReset();
			}
		}

		protected void StateMoveTo(Packet.ReceivePacketState newState)
		{
			this.StateMoveTo((int)newState);
		}

		protected void StateSkip(int numOfStateToSkip)
		{
			int newState = (int)(this._packetReceiveState + numOfStateToSkip + 1);
			this.StateMoveTo(newState);
		}

		public virtual byte[] GetTransmition()
		{
			return null;
		}

		public virtual bool Parse(byte b)
		{
			switch (this.ReceiveState)
			{
			case Packet.ReceivePacketState.PACKET_READ_BARKER:
				if (this.BarkerLength == BARKER_LENGTH_UNDEFINED)
				{
					this.StateReset();
					this._isParsingDone = true;
				}
				else
				{
					if (this.InternalBarkerState == Packet.ReceivePacketState.PACKET_READ_BARKER)
					{
					}
					byte b2 = this.Barker[this.InternalBarkerState];
					if (b == b2)
					{
						if (this.InternalBarkerState == this.BarkerLength - 1)
						{
							this.StateNext();
						}
						else
						{
							this.StateBarkerNext();
						}
					}
					else
					{
						this.StateReset();
					}
				}
				break;
			case Packet.ReceivePacketState.PACKET_READ_LEN1:
				this.Length = (int)b;
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_LEN2:
				this.Length |= (int)((ushort)(b << 8));
				this._expectedLength = this.Length;
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_NLEN1:
				this._notLength = (ushort)b;
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_NLEN2:
				this._notLength |= (ushort)(b << 8);
				if (this.Length != (int)(~this._notLength) || this.Length > this.Data.Length)
				{
					this.StateReset();
				}
				else
				{
					this.StateNext();
				}
				break;
			case Packet.ReceivePacketState.PACKET_READ_ID1:
				this.ID = (ushort)b;
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_ID2:
				this.ID |= (ushort)(b << 8);
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_SRC1:
				this.Source = (uint)b;
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_SRC2:
				this.Source |= (uint)((uint)b << 8);
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_SRC3:
				this.Source |= (uint)((uint)b << 16);
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_DST1:
				this.Destination = (uint)b;
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_DST2:
				this.Destination |= (uint)((uint)b << 8);
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_DST3:
				this.Destination |= (uint)((uint)b << 16);
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_DST4:
				this.Destination |= (uint)((uint)b << 24);
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_CODE1:
				this.OpCode = (ushort)b;
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_CODE2:
				this.OpCode |= (ushort)(b << 8);
				if (this.Length > 0)
				{
					this.StateNext();
					this.Length = 0;
				}
				else
				{
					this.StateSkip(1);
				}
				break;
			case Packet.ReceivePacketState.PACKET_READ_DATA:
				this.Append(b);
				if (this.Length == this._expectedLength)
				{
					this.StateNext();
				}
				break;
			case Packet.ReceivePacketState.PACKET_READ_CRC1:
				this._tempCRC = (ushort)b;
				this.StateNext();
				break;
			case Packet.ReceivePacketState.PACKET_READ_CRC2:
				this._tempCRC |= (ushort)(b << 8);
				if (this._tempCRC == this.GetCheckSum())
				{
					this.ParsingDone = true;
				}
				else
				{
					this.StateReset();
				}
				break;
			}
			return this._isParsingDone;
		}

		public virtual ushort GetCheckSum()
		{
			return 0;
		}
	}
}
