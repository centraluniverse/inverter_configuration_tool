using System;

namespace SEProtocol.SDP.Packets
{
	public class ProtocolMain
	{
		public const uint PROT_BROADCAST_ADDR	= 0xffffffff;

		public const uint PROT_SERVER_ADDR	= 0xfffffffe;

		public const uint PROT_CONFTOOL_ADDR	= 0xfffffffd;

		public const uint PROT_SINGLECAST_ADDR	= 0xfffffffc;

		public const uint PROT_WEBSERVICE_ADDR	= 0xfffffffb;
	}
}
