using System;

namespace SEProtocol.SDP.Packets
{
	public enum ProtocolServerCommands
	{
		PROT_CMD_SERVER_POST_DATA	= 0x0500,
		PROT_CMD_SERVER_GET_GMT		= 0x0501,
		PROT_CMD_SERVER_GET_NAME	= 0x0502,
		COUNT				= 0x0503,
	}
}
