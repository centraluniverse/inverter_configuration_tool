using System;

namespace SEProtocol.AT.ConnectOne
{
	public enum InternetSessionTriggerMode
	{
		DISABLE,
		ENTER_INIT,
		ALWAYS_ONLINE
	}
}
