using System;

namespace SEProtocol.AT.ConnectOne
{
	public enum SN_Flow
	{
		SN_F_IGNORE = -1,
		SN_F_NONE,
		SN_F_ACTIVE
	}
}
