using System;

namespace SEProtocol.AT.ConnectOne.Data
{
	public class iChipHostServerEndPoint
	{
		private string _address = null;

		private int _port = 0;

		public string Address
		{
			get
			{
				return this._address;
			}
			set
			{
				this._address = value;
			}
		}

		public int Port
		{
			get
			{
				return this._port;
			}
			set
			{
				this._port = value;
			}
		}

		public iChipHostServerEndPoint()
		{
		}

		public iChipHostServerEndPoint(string address, int port)
		{
			this._address = address;
			this._port = port;
		}
	}
}
