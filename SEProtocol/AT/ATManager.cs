using SEComm;
using SEComm.Connections;
using SEProtocol.AT.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace SEProtocol.AT
{
	public class ATManager
	{
		private const int DEFAULT_TIMEOUT = 1000;

		private Connection _connection;

		private ConnectionAccess _connectionAccess;

		public bool IsConnected
		{
			get
			{
				return this._connection != null && this._connection.IsConnected;
			}
		}

		public ATManager(ConnectionParams cp)
		{
			ConstructorInfo constructor = cp.ConnectionType.GetConstructor(cp.ParameterTypes);
			this._connection = (Connection)constructor.Invoke(cp.Parameters);
		}

		public bool StartConnection()
		{
			bool result = false;
			if (this._connection != null)
			{
				this._connection.OpenConnection();
				if (this._connection.IsConnected)
				{
					this._connectionAccess = new ConnectionAccess(this._connection);
				}
			}
			return result;
		}

		public void CloseConnection()
		{
			if (this._connection != null)
			{
				try
				{
					this._connection.CloseConnection();
				}
				catch (Exception var_0_1F)
				{
				}
				this._connection = null;
			}
			if (this._connectionAccess != null)
			{
				this._connectionAccess = null;
			}
		}

		public string[] SendReceive(ATPacket command)
		{
			return this.SendReceive(command, null);
		}

		public string[] SendReceive(ATPacket command, int? timeOutInMiliSec)
		{
			List<string> list = null;
			int timeOutInMiliSec2 = timeOutInMiliSec.HasValue ? timeOutInMiliSec.Value : 1000;
			string[] result;
			try
			{
				if (command == null)
				{
					result = null;
					return result;
				}
				string transmition = command.GetTransmition();
				Trace.WriteLine("Sending   AT: " + command.GetTransmition());
				this._connectionAccess.Write(transmition, timeOutInMiliSec2);
				list = new List<string>(0);
				while (true)
				{
					try
					{
						if (list != null && list.Count > 0)
						{
							string text = list[list.Count - 1];
							if (text.Equals(command.ExpectedEndResponse))
							{
								break;
							}
						}
						bool flag = false;
						bool flag2 = false;
						List<byte> list2 = new List<byte>(0);
						while (!flag || !flag2)
						{
							byte b = this._connectionAccess.ReadByte(timeOutInMiliSec2);
							list2.Add(b);
							if (b == 13)
							{
								flag = true;
							}
							else if (b == 10)
							{
								flag2 = true;
							}
						}
						StringBuilder stringBuilder = new StringBuilder(0);
						int count = list2.Count;
						for (int i = 0; i < count; i++)
						{
							byte b2 = list2[i];
							char value = (char)b2;
							stringBuilder.Append(value);
						}
						string text2 = stringBuilder.ToString();
						text2 = text2.Replace("\b", "");
						text2 = text2.Replace("\r\n", "");
						text2 = text2.Trim();
						if (!text2.Contains(command.Body))
						{
							list.Add(text2);
						}
					}
					catch (TimeoutException var_14_1AF)
					{
						break;
					}
					catch (Exception var_15_1B4)
					{
					}
				}
			}
			catch (Exception var_15_1B4)
			{
			}
			result = ((list != null) ? list.ToArray() : null);
			return result;
		}
	}
}
