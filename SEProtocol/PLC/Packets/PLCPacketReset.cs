using SEProtocol.PLC.Packets.Base;
using System;
using System.IO;

namespace SEProtocol.PLC.Packets
{
	public class PLCPacketReset : PLCPacket
	{
		private uint _dst = 0u;

		protected uint Destination
		{
			get
			{
				return this._dst;
			}
		}

		public PLCPacketReset(uint maxDataBufferLength, uint dst) : base(2, maxDataBufferLength)
		{
			this._dst = dst;
		}

		protected override byte[] GetDataTransmition()
		{
			MemoryStream memoryStream = new MemoryStream(0);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			binaryWriter.Write(this.Destination);
			binaryWriter.Flush();
			return memoryStream.ToArray();
		}
	}
}
