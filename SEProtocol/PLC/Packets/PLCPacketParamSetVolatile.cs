using System;
using System.Collections.Generic;
using System.IO;

namespace SEProtocol.PLC.Packets
{
	public class PLCPacketParamSetVolatile : PLCPacketParamGet
	{
		private List<uint> _values = null;

		protected List<uint> Values
		{
			get
			{
				return this._values;
			}
		}

		public override byte? ExpectedResponse
		{
			get
			{
				return new byte?(16);
			}
		}

		public PLCPacketParamSetVolatile(uint maxDataBufferLength, uint src, uint dst, List<ushort> indices, List<uint> values) : base(26, maxDataBufferLength, indices, src, dst)
		{
			this._values = values;
		}

		protected override byte[] GetDataTransmition()
		{
			MemoryStream memoryStream = new MemoryStream(0);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			binaryWriter.Write(base.Source);
			binaryWriter.Write(base.Destination);
			int num = (base.Indices != null && this.Values != null) ? Math.Min(base.Indices.Count, this.Values.Count) : 0;
			byte availableDataLength = this.GetAvailableDataLength();
			int val = (int)(availableDataLength / 6);
			num = Math.Min(num, val);
			for (int i = 0; i < num; i++)
			{
				binaryWriter.Write(base.Indices[i]);
				binaryWriter.Write(this.Values[i]);
			}
			binaryWriter.Flush();
			return memoryStream.ToArray();
		}
	}
}
