using SEProtocol.PLC.Packets.Base;
using System;
using System.IO;

namespace SEProtocol.PLC.Packets
{
	public class PLCPacketForceTelem : PLCPacket
	{
		private uint _dst = 0u;

		protected uint Destination
		{
			get
			{
				return this._dst;
			}
		}

		public PLCPacketForceTelem(uint maxDataBufferLength, uint dst) : base(5, maxDataBufferLength)
		{
			this._dst = dst;
		}

		protected override byte[] GetDataTransmition()
		{
			MemoryStream memoryStream = new MemoryStream(0);
			BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
			binaryWriter.Write(this.Destination);
			return memoryStream.ToArray();
		}
	}
}
